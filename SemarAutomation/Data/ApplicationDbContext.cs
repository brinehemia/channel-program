﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SemarAutomation.Models;

namespace SemarAutomation.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<ApprovalComment> ApprovalComment { get; set; }
        public DbSet<ApprovalTask> ApprovalTask { get; set; }
        public DbSet<ApprovalTemplate> ApprovalTemplate { get; set; }
        public DbSet<StepBase> StepBase { get; set; }
        public DbSet<HistoryLog> HistoryLog { get; set; }
        public DbSet<UserLoginLog> UserLoginLog { get; set; }
        public DbSet<NotificationApplicationUser> NotificationApplicationUser { get; set; }
        public DbSet<SendEmailLog> SendEmailLog { get; set; }
        public DbSet<EmailSetting> EmailSetting { get; set; }

        // SFM Channel Program
        public DbSet<Proposal> Proposal { get; set; }
        public DbSet<SFMProgram> SFMProgram { get; set; }
        public DbSet<SFMEntitlement> SFMEntitlement { get; set; }
        public DbSet<ProgramSFMDistributor> ProgramSFMDistributor { get; set; }
        public DbSet<SFMFileUploadType> SFMFileUploadType { get; set; }
        public DbSet<SFMProposalType> SFMProposalType { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    }
}