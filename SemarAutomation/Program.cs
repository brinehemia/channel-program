using Hangfire;
using Hangfire.MySql;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SemarAutomation.Controllers.Services;
using SemarAutomation.Data;
using SemarAutomation.Hubs;
using SemarAutomation.Models;
using System.Transactions;

// Add services to the container.
var builder = WebApplication.CreateBuilder(args);

builder.Services.Configure<CookiePolicyOptions>(options =>
{
    options.CheckConsentNeeded = context => true;
    options.MinimumSameSitePolicy = SameSiteMode.None;
});

var env = builder.Configuration.GetConnectionString("DefaultConnection");
var connectionString = string.Empty;

if (env == "Local") connectionString = "server=localhost;user=root;password=;database=signify_semarautomation;Convert Zero Datetime=True";
else if (env == "Staging") connectionString = "server=localhost;user=semar;password=liHuqi7I15De5AsimizIb7sa3I1Iju;database=signify_semarautomation;Convert Zero Datetime=True";
else connectionString = "server=165.22.56.73;user=prof_project;password=USc7eWrJm8exzbiq;database=signify_channelprogram;Convert Zero Datetime=True";

builder.Services.AddDbContext<ApplicationDbContext>(options =>
    options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString)));
builder.Services.AddDatabaseDeveloperPageExceptionFilter();

var mySqlStorageOptions = new MySqlStorageOptions
{
    TransactionIsolationLevel = IsolationLevel.ReadCommitted,
    QueuePollInterval = TimeSpan.FromSeconds(15),
    JobExpirationCheckInterval = TimeSpan.FromHours(14),
    CountersAggregateInterval = TimeSpan.FromMinutes(5),
    PrepareSchemaIfNecessary = true,
    DashboardJobListLimit = 50000,
    TransactionTimeout = TimeSpan.FromMinutes(1),
    TablesPrefix = "HangFireTable"
};

builder.Services.AddHangfire(x => x.UseStorage(new MySqlStorage(connectionString, mySqlStorageOptions)));
builder.Services.AddHangfireServer();

builder.Services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

builder.Services.Configure<IdentityOptions>(options =>
{
    // Password settings.
    options.Password.RequireNonAlphanumeric = true;
    options.Password.RequireDigit = true;
    options.Password.RequireLowercase = true;
    options.Password.RequireUppercase = true;
    options.Password.RequiredLength = 6;
    options.Lockout.AllowedForNewUsers = false;

    // User settings.
    options.User.AllowedUserNameCharacters =
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
    options.User.RequireUniqueEmail = true;
});

builder.Services.AddMvc(option => option.EnableEndpointRouting = false)
                .SetCompatibilityVersion(Microsoft.AspNetCore.Mvc.CompatibilityVersion.Latest)
                .AddNewtonsoftJson(opt => opt.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);

builder.Services.AddSession(options =>
{
    options.Cookie.Name = ".Claim.Session";
    options.IdleTimeout = TimeSpan.FromSeconds(5);
    options.IOTimeout = TimeSpan.FromSeconds(5);
    options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
    options.Cookie.SameSite = SameSiteMode.Strict;
    options.Cookie.IsEssential = true;
    options.Cookie.HttpOnly = true;
});

builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
builder.Services.AddTransient<IEmailSender, EmailSender>();
//builder.Services.AddScoped<IScopedProcessingService, ScopedProcessingService>();
builder.Services.AddScoped<IGetResponseService, GetResponseService>();
builder.Services.AddScoped<IPostResponseService, PostResponseService>();
builder.Services.AddScoped<IPutResponseService, PutResponseService>();
builder.Services.AddScoped<IStepApprovalController, StepApprovalController>();
builder.Services.AddTransient<IUserConnectionManager, UserConnectionManager>();

builder.Services.ConfigureApplicationCookie(options =>
{
    //Cookie settings
    options.Cookie.Name = "ClaimCookies";
    options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
    options.Cookie.HttpOnly = false;
    options.ExpireTimeSpan = TimeSpan.FromDays(1);
    options.LoginPath = new PathString("/login");
    options.LogoutPath = new PathString("/Identity/Account/Logout");
    options.AccessDeniedPath = new PathString("/Identity/Account/AccessDenied");

    options.SlidingExpiration = false;
});

builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(options =>
{
    options.Cookie.HttpOnly = false;
    options.Cookie.Name = "SemarAutomationCookie";
    options.ExpireTimeSpan = TimeSpan.FromDays(1);
});

builder.Services.AddAuthorization(options =>
{
    options.AddPolicy("Distributor",
        policy => policy.RequireRole("Distributor"));
    options.AddPolicy("ASM",
        policy => policy.RequireRole("ASM"));
    options.AddPolicy("Channel Lead",
        policy => policy.RequireRole("Channel Lead"));
    options.AddPolicy("Marketing",
        policy => policy.RequireRole("Marketing"));
    options.AddPolicy("Finance",
        policy => policy.RequireRole("Finance"));
    options.AddPolicy("Legal",
        policy => policy.RequireRole("Legal"));
    options.AddPolicy("MarketingFinance",
        policy => policy.RequireRole("Finance", "Marketing"));
    options.AddPolicy("SuperAdmin",
        policy => policy.RequireRole("Super Admin"));
});

builder.Services.AddRazorPages();
builder.Services.AddSignalR();

builder.Logging.AddFile("Logs/Eclaim-Log-{Date}.txt");

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseMigrationsEndPoint();
}
else
{
    app.UseExceptionHandler("/Identity/Account/AccessDenied");
    app.UseHsts();
}

app.UseHttpContext();
app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseCookiePolicy();
app.UseSession();
app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.UseHangfireDashboard("/hangfire", new DashboardOptions { Authorization = new[] { new CustomAuthorizationFilter() } });

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
app.MapRazorPages();

app.Run();
