﻿namespace SemarAutomation.Controllers.Services
{
    public class SetNotif
    {
        private HttpContext _http;

        public SetNotif(HttpContext http)
        {
            _http = http;
        }

        public string GetAlertStatus()
        {
            return _http.Session.GetString("Alert");
        }

        public void SetWithStatus(bool status, string tittleSuccess, string msgSuccess, string titleFail, string msgFail)
        {
            SessionHelperModel _session = new SessionHelperModel();
            if (status)
            {
                this.SetAlert(tittleSuccess, "success", msgSuccess);
            }
            else
            {
                this.SetAlert(titleFail, "error", msgFail);
            }
        }

        public void SetFailed(string titleFail, string msgFail)
        {
            this.SetAlert(titleFail, "error", msgFail);
        }

        public void SetSucced(string tittleSuccess, string msgSuccess)
        {
            this.SetAlert(tittleSuccess, "success", msgSuccess);
        }

        public void SetAlert(string titleAlert, string typeAlert, string msgAlert)
        {
            SessionHelperModel _session = new SessionHelperModel();
            _session.SessionTitleAlert = titleAlert;
            _session.SessionTypeAlert = typeAlert;
            _session.SessionMessageAlert = msgAlert;

            _http.Session.SetString("AlertTitle", _session.SessionTitleAlert);
            _http.Session.SetString("AlertType", _session.SessionTypeAlert);
            _http.Session.SetString("Alert", _session.SessionMessageAlert);
        }

        public void ClearSession()
        {
            _http.Session.SetString("Alert", " ");
        }

    }

    public class SessionHelperModel
    {
        public string SessionTitle { get; set; }
        public string SessionType { get; set; }
        public string SessionMessage { get; set; }

        public string SessionTitleAlert { get; set; }
        public string SessionTypeAlert { get; set; }
        public string SessionMessageAlert { get; set; }
    }
}
