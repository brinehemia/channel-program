﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SemarAutomation.Data;
using SemarAutomation.Hubs;
using SemarAutomation.Models;
using SemarAutomation.Models.ViewModels;

namespace SemarAutomation.Controllers.Services
{
    public class SendNotification
    {
        protected readonly ApplicationDbContext _context;
        private readonly IHubContext<NotificationHub> _notificationUserHubContext;
        private readonly IUserConnectionManager _userConnectionManager;
        private readonly IGetResponseService _getResponse;
        public SendNotification(ApplicationDbContext context,
            IHubContext<NotificationHub> notificationUserHubContext, 
            IUserConnectionManager userConnectionManager, 
            IGetResponseService getResponse)
        {
            _context = context;
            _notificationUserHubContext = notificationUserHubContext;
            _userConnectionManager = userConnectionManager;
            _getResponse = getResponse;
        }

        public NotificationApplicationUser sendUploadSFMEntitlementNotif(string url, string account)
        {
            NotificationApplicationUser notifUser = new NotificationApplicationUser();
            var user = _context.Users.Where(x => x.RoleName == "Distributor" && x.AccountDistributor == account).FirstOrDefault();
            notifUser.IsRead = false;
            notifUser.Title = "Entitlement Diupload";
            notifUser.Message = "Entitlement baru telah diupload";
            notifUser.Date = DateTime.UtcNow.AddHours(7);
            notifUser.Url = url;
            notifUser.Icon = "mdi mdi-upload-outline";
            notifUser.Color = "#19d135";
            notifUser.UserId = user.Id;
            _context.NotificationApplicationUser.Add(notifUser);
            if (_context.SaveChanges() > 0)
            {
                SendNotif(notifUser.UserId);
            }
            return notifUser;
        }

        public NotificationApplicationUser sendApproveSFMNotif(ApprovalTask approvalTask, string url, string isDelegate)
        {
            NotificationApplicationUser notifUser = new NotificationApplicationUser();

            var user = _context.Users.Where(x => x.RoleName == "Distributor" && x.Id == approvalTask.SubmittedById).FirstOrDefault();
            var account = user.AccountDistributor;
            var getDataProposal = new SFMProposalDetail();
            var getDataClaim = new SFMClaimDTO();
            var nextRole = string.Empty;
            var currentPIC = _context.Users.FirstOrDefault(a => a.Id == approvalTask.CurrentPICId);

            if (approvalTask.Title == "SFM Proposal")
            {
                getDataProposal = JsonConvert.DeserializeObject<SFMProposalDetail>(approvalTask.Data);
                notifUser.Message = "Proposal Anda telah selesai diapprove oleh " + currentPIC.RoleName;
                notifUser.Title = currentPIC.RoleName + " Approve Proposal";

                if (currentPIC.RoleName == "ASM") nextRole = "Marketing";
                else if (currentPIC.RoleName == "Marketing" && getDataProposal.IsNeedLegal) nextRole = "Legal";
            }
            else if (approvalTask.Title == "SFM Claim")
            {
                getDataClaim = JsonConvert.DeserializeObject<SFMClaimDTO>(approvalTask.Data);
                notifUser.Message = "Claim Anda telah selesai diapprove oleh " + currentPIC.RoleName;
                notifUser.Title = currentPIC.RoleName + " Approve Claim";

                if (isDelegate == "Yes")
                {
                    notifUser.Message = "Claim Anda telah selesai didelegasikan oleh " + currentPIC.RoleName;
                    notifUser.Title = currentPIC.RoleName + " Delegate Claim";
                }

                if (currentPIC.RoleName == "ASM") nextRole = "Marketing";
                else if (currentPIC.RoleName == "Marketing" && isDelegate == "Yes") nextRole = "Channel Lead";
                else if (currentPIC.RoleName == "Marketing" && isDelegate == "No" && getDataClaim.IsNeedLegal) nextRole = "Legal";
                else if (currentPIC.RoleName == "Marketing" && isDelegate == "No" && !getDataClaim.IsNeedLegal) nextRole = "Finance";
                else if (currentPIC.RoleName == "Channel Lead" && getDataClaim.IsNeedLegal) nextRole = "Legal";
                else if (currentPIC.RoleName == "Channel Lead" && !getDataClaim.IsNeedLegal) nextRole = "Finance";
                else if (currentPIC.RoleName == "Legal") nextRole = "Finance";
            }

            notifUser.IsRead = false;

            notifUser.Date = DateTime.UtcNow.AddHours(7);
            notifUser.Url = url;
            notifUser.Icon = "mdi mdi-file-check-outline";
            notifUser.Color = "#80E1AB";
            notifUser.UserId = user.Id;

            _context.NotificationApplicationUser.Add(notifUser);
            if (_context.SaveChanges() > 0)
            {
                SendNotif(notifUser.UserId);
            }

            if (nextRole != "")
            {
                var userInRoles = _context.Users.Where(x => x.RoleName == nextRole).ToList();
                if (nextRole != "Finance") userInRoles = userInRoles.Where(a => a.IsSFMUser == true).ToList();
                if (nextRole == "ASM")
                {
                    var response = _getResponse.GetResponse("getasm_by_soldto?sold_to=" + account).Result;
                    var listSoldTo = JsonConvert.DeserializeObject<SFMListResponse>(response.Content).data;

                    userInRoles = userInRoles.Where(a => listSoldTo.Any(x => x.ToLower() == a.Email.ToLower())).ToList();
                }

                if (approvalTask.CurrentStepId == "20ffdf40-2712-49ad-9bb7-7b188af2b914")
                {
                    var userASM = _context.Users.Where(a => a.IsSFMUser == true && a.RoleName == "Finance").ToList();
                    userInRoles.AddRange(userASM);
                }

                foreach (var _user in userInRoles)
                {
                    notifUser = new NotificationApplicationUser();
                    notifUser.IsRead = false;

                    if (approvalTask.Title == "SFM Proposal")
                    {
                        notifUser.Title = currentPIC.RoleName + " Approve Proposal";
                        notifUser.Message = "Proposal distributor dengan nomor proposal " + approvalTask.ClaimNumber + " telah selesai diapprove oleh " + currentPIC.RoleName;
                    }
                    else if (approvalTask.Title == "SFM Claim")
                    {
                        notifUser.Title = currentPIC.RoleName + " Approve Claim";
                        notifUser.Message = "Claim distributor dengan nomor claim " + approvalTask.ClaimNumber + " telah selesai diapprove oleh " + currentPIC.RoleName;
                    }

                    notifUser.Date = DateTime.UtcNow.AddHours(7);
                    notifUser.Icon = "mdi mdi-file-check-outline";
                    notifUser.Color = "#80E1AB";
                    notifUser.Url = url;
                    notifUser.UserId = _user.Id;
                    _context.NotificationApplicationUser.Add(notifUser);
                    if (_context.SaveChanges() > 0)
                    {
                        SendNotif(notifUser.UserId);
                    }
                }
            }
            return notifUser;
        }

        public NotificationApplicationUser sendSendBackSFMNotif(ApprovalTask approvalTask, string url)
        {
            var allUser = _context.Users.Where(a => a.IsSFMUser == true).ToList();
            var notifResult = new List<NotificationApplicationUser>() { };

            var user = _context.Users.Where(x => x.RoleName == "Distributor" && x.Id == approvalTask.SubmittedById).FirstOrDefault();
            var step = _context.StepBase.AsNoTracking().Where(x => x.Id == approvalTask.CurrentStepId).FirstOrDefault();
            var currentPIC = _context.Users.Where(x => x.Id == approvalTask.CurrentPICId).FirstOrDefault();

            if (approvalTask.Title == "SFM Proposal")
            {
                var filteredUser = new List<ApplicationUser>() { };

                if (step.Roles == "Marketing" || step.Roles == "Legal")
                {
                    filteredUser = allUser.Where(a => a.RoleName == "ASM").ToList();
                    var response = _getResponse.GetResponse("getasm_by_soldto?sold_to=" + user.AccountDistributor).Result;
                    if ((int)response.StatusCode != 200) return new NotificationApplicationUser();

                    var listSoldTo = JsonConvert.DeserializeObject<SFMListResponse>(response.Content).data;
                    filteredUser = filteredUser.Where(a => listSoldTo.Any(x => x.ToLower() == a.Email.ToLower())).ToList();

                    if (step.Roles == "Legal")
                    {
                        filteredUser.AddRange(allUser.Where(a => a.RoleName == "Marketing"));
                    }
                }

                filteredUser.Add(user);
                if (filteredUser.Count != 0)
                {
                    foreach (var item in filteredUser)
                    {
                        NotificationApplicationUser notifUser = new NotificationApplicationUser();
                        notifUser.Title = currentPIC.RoleName + " Send Back Proposal";
                        notifUser.IsRead = false;
                        notifUser.Date = DateTime.UtcNow.AddHours(7);
                        notifUser.Icon = "mdi mdi-file-undo";
                        notifUser.Color = "#F34943";
                        notifUser.Url = url;

                        if (item.Email == user.Email)
                        {
                            notifUser.Message = "Proposal Anda dengan nomor " + approvalTask.ClaimNumber + " telah disendback oleh " + step.Title.Replace("Edit Claim From", "");
                        }
                        else notifUser.Message = "Proposal dengan nomor " + approvalTask.ClaimNumber + " telah disendback oleh " + step.Title.Replace("Edit Claim From", "");

                        notifUser.UserId = item.Id;
                        notifResult.Add(notifUser);
                    }
                }
            }
            else if (approvalTask.Title == "SFM Claim")
            {
                var filteredUser = new List<ApplicationUser>() { };

                if (step.Roles == "Marketing" || step.Roles == "Channel Lead" || step.Roles == "Finance" || step.Roles == "Legal")
                {
                    filteredUser = allUser.Where(a => a.RoleName == "ASM").ToList();
                    var response = _getResponse.GetResponse("getasm_by_soldto?sold_to=" + user.AccountDistributor).Result;
                    if ((int)response.StatusCode != 200) return new NotificationApplicationUser();

                    var listSoldTo = JsonConvert.DeserializeObject<SFMListResponse>(response.Content).data;
                    filteredUser = filteredUser.Where(a => listSoldTo.Any(x => x.ToLower() == a.Email.ToLower())).ToList();

                    if (step.Roles == "Channel Lead" || step.Roles == "Finance" || step.Roles == "Legal")
                    {
                        filteredUser.AddRange(allUser.Where(a => a.RoleName == "Marketing"));
                    }
                }

                filteredUser.Add(user);
                if (filteredUser.Count != 0)
                {
                    foreach (var item in filteredUser)
                    {
                        NotificationApplicationUser notifUser = new NotificationApplicationUser();
                        notifUser.Title = currentPIC.RoleName + " Send Back Claim";
                        notifUser.IsRead = false;
                        notifUser.Date = DateTime.UtcNow.AddHours(7);
                        notifUser.Icon = "mdi mdi-file-undo";
                        notifUser.Color = "#F34943";
                        notifUser.Url = url;

                        if (item.Email == user.Email)
                        {
                            notifUser.Message = "Claim Anda dengan nomor " + approvalTask.ClaimNumber + " telah disendback oleh " + step.Title.Replace("Edit Claim From", "");
                        }
                        else notifUser.Message = "Claim dengan nomor " + approvalTask.ClaimNumber + " telah disendback oleh " + step.Title.Replace("Edit Claim From", "");

                        notifUser.UserId = item.Id;
                        notifResult.Add(notifUser);
                    }
                }
            }

            _context.NotificationApplicationUser.AddRange(notifResult);
            _context.SaveChanges();

            return new NotificationApplicationUser();
        }

        public NotificationApplicationUser sendSubmitClaimSFMNotif(string number, string url, string type, string roleTo, string account, string stepBase)
        {
            if (roleTo != "")
            {
                if (!string.IsNullOrEmpty(roleTo) && roleTo.ToLower() != "distributor")
                {
                    var userInRoles = _context.Users.Where(x => x.RoleName == roleTo).ToList();
                    if (roleTo != "Finance") userInRoles = userInRoles.Where(a => a.IsSFMUser == true).ToList();
                    if (roleTo == "ASM")
                    {
                        var response = _getResponse.GetResponse("getasm_by_soldto?sold_to=" + account).Result;
                        if ((int)response.StatusCode != 200) return new NotificationApplicationUser();

                        var listSoldTo = JsonConvert.DeserializeObject<SFMListResponse>(response.Content).data;

                        userInRoles = userInRoles.Where(a => listSoldTo.Any(x => x.ToLower() == a.Email.ToLower())).ToList();
                    }

                    if (stepBase == "3ec7d311-8a95-4df4-824d-6fca6160ea2f")
                    {
                        var userAdded = _context.Users.Where(a => a.IsSFMUser == true).ToList();

                        var response = _getResponse.GetResponse("getasm_by_soldto?sold_to=" + account).Result;
                        if ((int)response.StatusCode != 200) return new NotificationApplicationUser();

                        var listSoldTo = JsonConvert.DeserializeObject<SFMListResponse>(response.Content).data;

                        userInRoles.AddRange(userAdded.Where(a => a.RoleName == "ASM" && listSoldTo.Any(x => x.ToLower() == a.Email.ToLower())).ToList());
                        userInRoles.AddRange(userAdded.Where(a => a.RoleName == "Channel Lead").ToList());
                    }

                    var userDistributor = _context.Users.Where(a => a.AccountDistributor == account).FirstOrDefault();
                    foreach (var _user in userInRoles)
                    {
                        NotificationApplicationUser notifUser = new NotificationApplicationUser();
                        notifUser.IsRead = false;

                        if (type == "Claim") notifUser.Message = userDistributor.CompanyDistributor + " telah mensubmit " + type + " dengan nomor " + number;
                        else
                        {
                            var proposal = _context.Proposal.Where(a => a.ProposalNumber == Convert.ToInt32(number)).FirstOrDefault();
                            notifUser.Message = userDistributor.CompanyDistributor + " telah mensubmit " + type + " dengan nama aktivitas " + proposal.NamaAktivitas;
                        }

                        notifUser.Title = "Distributor Submit " + type;
                        notifUser.Date = DateTime.UtcNow.AddHours(7);
                        notifUser.Url = url;
                        notifUser.Icon = "mdi mdi-file-move";
                        notifUser.Color = "#188AE2";
                        notifUser.UserId = _user.Id;
                        _context.NotificationApplicationUser.Add(notifUser);
                        _context.SaveChanges();
                    }
                }
            }
            return new NotificationApplicationUser();
        }

        public async void SendNotif(string userId)
        {
            try
            {
                var connections = _userConnectionManager.GetUserConnections(userId);
                var totalUnRead = _context.NotificationApplicationUser.AsNoTracking().Where(x => x.IsRead == false && x.UserId == userId).Count();
                if (connections != null && connections.Count > 0)
                {
                    foreach (var connectionId in connections)
                    {
                        await _notificationUserHubContext.Clients.Client(connectionId).SendAsync("sendToUser", totalUnRead.ToString());//send to user 
                    }
                }
            }
            catch (Exception e)
            {
                var msg = e.Message;
            }
        }

    }
}
