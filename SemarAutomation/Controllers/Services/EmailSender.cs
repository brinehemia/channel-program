﻿using Microsoft.Exchange.WebServices.Data;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using SemarAutomation.Data;
using SemarAutomation.Models.ViewModels;
using System.Net;
using System.Net.Mail;

namespace SemarAutomation.Controllers.Services
{
    public interface IEmailSender
    {
        System.Threading.Tasks.Task SendEmailAsync(string email, string subject, SendEmailViewModel data);
        System.Threading.Tasks.Task SendgmailAsync(string email, string subject, SendEmailViewModel data);
        System.Threading.Tasks.Task<SendEmailViewModel> sendEmailViewModel(string email, string subject, string link, string root, string approvalTaskId);
    }

    public class EmailSender : IEmailSender
    {
        private ApplicationDbContext _context;
        public readonly LogHelper _log;
        private readonly IOptions<EmailSettings> appSettings;

        public EmailSender(IOptions<EmailSettings> app, ApplicationDbContext context)
        {
            appSettings = app;
            _context = context;
            _log = new LogHelper(_context);
        }

        private ExchangeService SetEmailServer()
        {
            var EmailServer = _context.EmailSetting.FirstOrDefault();

            ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2007_SP1);
            //var email = appSettings.Value.Email;
            //var pass = appSettings.Value.Pass;

            var email = EmailServer.Email;
            var pass = EmailServer.Password;

            service.Credentials = new WebCredentials(email, pass);
            service.TraceEnabled = true;
            service.TraceFlags = TraceFlags.All;
            service.AutodiscoverUrl(email, RedirectionUrlValidationCallback);
            return service;
        }

        public async System.Threading.Tasks.Task SendEmailAsync(string email, string subject, SendEmailViewModel Data)
        {
            var logemail = _log.SetEmailSend(email, subject, "Processed", "", "");
            try
            {
                ExchangeService service = SetEmailServer();
                EmailMessage emails = new EmailMessage(service);
                emails.ToRecipients.Add(email);
                if (Data.User.OtherEmail != null && Data.User.OtherEmail != string.Empty)
                {
                    string[] emailcc = Data.User.OtherEmail.Split(",");
                    emails.CcRecipients.AddRange(emailcc);
                }

                if (Data.CCApproverSendback != string.Empty && Data.CCApproverSendback != null)
                {
                    string[] emailcc = Data.CCApproverSendback.Split(",");
                    emails.CcRecipients.AddRange(emailcc);
                }

                emails.Subject = subject;
                emails.Body = new MessageBody(this.GenerateEmailApprovalSFM(subject, Data));
                emails.Body.BodyType = BodyType.HTML;

                if (Data.Subject == "Submitted claim for Distributor")
                {
                    emails.Attachments.AddFileAttachment(Data.LinkAttachment);
                    _log.SetEmailSend(email, subject, "Success", JsonConvert.SerializeObject(emails.Body), logemail);
                }

                emails.Send();

                _log.SetEmailSend(email, subject, "Success", JsonConvert.SerializeObject(emails.Body), logemail);
            }
            catch (Exception e)
            {
                _log.SetEmailSend(email, subject, "Failed", JsonConvert.SerializeObject(e), logemail);
            }
        }


        public async System.Threading.Tasks.Task SendgmailAsync(string email, string subject, SendEmailViewModel Data)
        {
            try
            {
                var client = new SmtpClient("smtp.mailtrap.io", 2525)
                {
                    Credentials = new NetworkCredential("086f8f31e6aae7", "1f3a1de1771e48"),
                    EnableSsl = true
                };

                var messageTrap = new MailMessage("noreply.channelprogram@gmail.com", email);
                messageTrap.Subject = this.GenerateSubjectSFM(subject, Data);
                messageTrap.IsBodyHtml = true;
                messageTrap.Body = this.GenerateEmailApprovalSFM(subject, Data);

                client.Send(messageTrap);

                var smtpClient = new SmtpClient
                {
                    UseDefaultCredentials = false,
                    Host = "smtp.gmail.com",
                    Port = 587,
                    //Credentials = new NetworkCredential("noreply.channelprogram@gmail.com", "channelprogramEmail!321"),
                    //Credentials = new NetworkCredential("noreply.channelprogram@gmail.com", "dkwbxlpidequwpfm"), // LOCAL
                    Credentials = new NetworkCredential("noreply.channelprogram@gmail.com", "esstagtkizcgwrdm"), // Prod
                    EnableSsl = true,
                };

                await smtpClient.SendMailAsync(messageTrap);
            }
            catch (Exception e)
            {

            }
        }

        public string GenerateEmailApprovalSFM(string type, SendEmailViewModel item)
        {
            var EmailBody = string.Empty;
            if (type == "Submitted claim for Distributor")
            {
                item.Title = "Claim telah berhasil diajukan";
                EmailBody = "<div style='display:inline;'><p style='text-align: justify; margin-left: 10px;'>Claim Anda " + item.NomorClaim + " telah berhasil diajukan. Anda dapat melihat status proses claim " + item.NomorClaim + " dengan login melalui link <a style='margin-left: 5px;' href='" + item.Link + "'>disini</a></p> </div>";
            }
            else if (type == "Submitted proposal for Distributor")
            {
                item.Title = "Proposal telah berhasil diajukan";
                EmailBody = "<div style='display:inline;'><p style='text-align: justify; margin-left: 10px;'>Proposal Anda " + item.NomorClaim + " telah berhasil diajukan. Anda dapat melihat status proses proposal " + item.NomorClaim + " dengan login melalui link <a style='margin-left: 5px;' href='" + item.Link + "'>disini</a></p> </div>";
            }
            else if (type == "Submitted claim for Approver")
            {
                item.Title = "Pengajuan claim";
                EmailBody = "<div style='display:inline;'><p style='text-align: justify; margin-left: 10px;'>Claim " + item.NomorClaim + " telah diajukan oleh " + item.User2.Name + ". Silahkan mengakses link<a style='margin-left: 5px; margin-right: 5px;' href='" + item.Link + "'>disini</a> untuk melakukan review dan memberikan persetujuan pada claim tersebut.</p></div>";
            }
            else if (type == "Submitted proposal for Approver")
            {
                item.Title = "Pengajuan proposal";
                EmailBody = "<div style='display:inline;'><p style='text-align: justify; margin-left: 10px;'>Proposal " + item.NomorClaim + " pada program " + item.NamaProgram + " telah diajukan oleh " + item.User2.Name + ". Silahkan mengakses link<a style='margin-left: 5px; margin-right: 5px;' href='" + item.Link + "'>disini</a> untuk melakukan review dan memberikan persetujuan pada proposal tersebut.</p></div>";
            }
            else if (type == "SendBack Claim")
            {
                item.Title = "Claim Ditolak";
                EmailBody = "<div style='display:inline;'><p style='text-align: justify; margin-left: 10px;';>Claim nomor " + item.NomorClaim + " telah di tolak dan kembalikan oleh " + item.CurrentPICRole + ". Mohon cek ulang pengajuan claim beserta persyaratan yang diperlukan. Untuk cek dan revisi claim " + item.NomorClaim + " Anda dapat login melalui link <a style='margin-left:5px;' href='" + item.Link + "'> disini</a></p></div>";
            }
            else if (type == "SendBack Claim CC")
            {
                item.Title = "Claim Ditolak";
                EmailBody = "<div style='display:inline;'><p style='text-align: justify; margin-left: 10px;';>Claim nomor " + item.NomorClaim + " telah di tolak dan kembalikan oleh " + item.CurrentPICRole + ". Untuk cek claim " + item.NomorClaim + " Anda dapat login melalui link <a style='margin-left:5px;' href='" + item.Link + "'> disini</a></p></div>";
            }
            else if (type == "SendBack Proposal")
            {
                item.Title = "Proposal Ditolak";
                EmailBody = "<div style='display:inline;'><p style='text-align: justify; margin-left: 10px;';>Proposal nomor " + item.NomorClaim + " pada program " + item.NamaProgram + " telah di tolak dan kembalikan oleh " + item.CurrentPICRole + ". Mohon cek ulang pengajuan proposal beserta persyaratan yang diperlukan. Untuk cek dan revisi proposal " + item.NomorClaim + " Anda dapat login melalui link <a style='margin-left:5px;' href='" + item.Link + "'> disini</a></p></div>";
            }
            else if (type == "SendBack Proposal CC")
            {
                item.Title = "Proposal Ditolak";
                EmailBody = "<div style='display:inline;'><p style='text-align: justify; margin-left: 10px;';>Proposal nomor " + item.NomorClaim + " pada program " + item.NamaProgram + " telah di tolak dan kembalikan oleh " + item.CurrentPICRole + ". Untuk cek proposal " + item.NomorClaim + " Anda dapat login melalui link <a style='margin-left:5px;' href='" + item.Link + "'> disini</a></p></div>";
            }
            else if (type == "SendBack Proposal Marketing-Legal CC")
            {
                item.Title = "Proposal Ditolak";
                EmailBody = "<div style='display:inline;'><p style='text-align: justify; margin-left: 10px;';>Proposal nomor " + item.NomorClaim + " pada program " + item.NamaProgram + " telah di tolak dan kembalikan oleh " + item.CurrentPICRole + ". Mohon cek ulang pengajuan proposal beserta persyaratan yang diperlukan. Untuk cek proposal " + item.NomorClaim + " Anda dapat login melalui link <a style='margin-left:5px;' href='" + item.Link + "'> disini</a></p></div>";
            }
            else if (type == "Approved for Distributor")
            {
                item.Title = "Claim " + item.NomorClaim + " telah disetujui";
                EmailBody = "<div style='display:inline;'><p style='text-align: justify; margin-left: 10px;'>Claim Anda dengan nomor " + item.NomorClaim + " telah disetujui oleh " + item.User2.RoleName + ". Anda dapat cek proses claim " + item.NomorClaim + " dengan login melalui link <a style='margin-left:5px;' href='" + item.Link + "'>disini</a></p></div>";
            }
            else if (type == "Approved for Distributor Proposal")
            {
                item.Title = "Proposal " + item.NomorClaim + " telah disetujui";
                EmailBody = "<div style='display:inline;'><p style='text-align: justify; margin-left: 10px;'>Proposal Anda dengan nomor " + item.NomorClaim + " telah disetujui oleh " + item.User2.RoleName + ". Anda dapat cek proses proposal " + item.NomorClaim + " dengan login melalui link <a style='margin-left:5px;' href='" + item.Link + "'>disini</a></p></div>";
            }
            else if (type == "Approved for Approver")
            {
                item.Title = "Claim " + item.NomorClaim + " telah disetujui";
                EmailBody = "<div style='display:inline;'><p style='text-align: justify; margin-left: 10px;'>Claim " + item.NomorClaim + " telah disetujui oleh " + item.CurrentPICRole + ". Untuk melakukan persetujuan claim tersebut, Anda dapat login melalui link <a style='margin-left:5px;' href='" + item.Link + "'>disini</a></p></div>";
            }
            else if (type == "Approved for Approver Proposal")
            {
                item.Title = "Proposal " + item.NomorClaim + " telah disetujui";
                EmailBody = "<div style='display:inline;'><p style='text-align: justify; margin-left: 10px;'>Proposal " + item.NomorClaim + " telah disetujui oleh " + item.CurrentPICRole + ". Untuk melakukan persetujuan proposal tersebut, Anda dapat login melalui link <a style='margin-left:5px;' href='" + item.Link + "'>disini</a></p></div>";
            }
            else if (type == "Submmited after Send Back")
            {
                item.Title = "Claim telah berhasil disubmit ulang";
                EmailBody = "<div style='display:inline;'><p style='text-align: justify; margin-left: 10px;'>Claim " + item.NomorClaim + " telah berhasil diajukan ulang. Silahkan mengakses link<a style='margin-left: 5px; margin-right: 5px;' href='" + item.Link + "'>disini</a> untuk melakukan review dan memberikan persetujuan claim tersebut.</p></div>";
            }
            else if (type == "Submmited after Send Back CC")
            {
                item.Title = "Claim telah berhasil disubmit ulang";
                EmailBody = "<div style='display:inline;'><p style='text-align: justify; margin-left: 10px;'>Claim " + item.NomorClaim + " telah berhasil diajukan ulang. Silahkan mengakses link<a style='margin-left: 5px; margin-right: 5px;' href='" + item.Link + "'>disini</a> untuk melihat detail claim tersebut.</p></div>";
            }
            else if (type == "Submmited after Send Back Proposal")
            {
                item.Title = "Proposal telah berhasil disubmit ulang";
                EmailBody = "<div style='display:inline;'><p style='text-align: justify; margin-left: 10px;'>Proposal " + item.NomorClaim + " pada program " + item.NamaProgram + " telah berhasil diajukan ulang. Silahkan mengakses link<a style='margin-left: 5px; margin-right: 5px;' href='" + item.Link + "'>disini</a> untuk melakukan review dan memberikan persetujuan proposal tersebut.</p></div>";
            }
            else if (type == "Closed Claim")
            {
                item.Title = "Claim " + item.NomorClaim + " Closed";
                EmailBody = "<div style='display:inline;'><p style='text-align: justify; margin-left: 10px;'>Claim Anda dengan nomor " + item.NomorClaim + " telah selesai terproses (closed). Anda dapat cek status claim " + item.NomorClaim + " dengan login melalui link <a style='margin-left:5px;' href='" + item.Link + "'>disini</a> </p></div>";
            }
            else if (type == "Closed Proposal")
            {
                item.Title = "Proposal " + item.NomorClaim + " Closed";
                EmailBody = "<div style='display:inline;'><p style='text-align: justify; margin-left: 10px;'>Proposal Anda dengan nomor " + item.NomorClaim + " telah selesai terproses (closed). Anda dapat cek status proposal " + item.NomorClaim + " dengan login melalui link <a style='margin-left:5px;' href='" + item.Link + "'>disini</a> </p></div>";
            }

            var emailTemplate = "<!DOCTYPE html><html lang='en'> <style>.button{box-shadow: 0px 0px 5px 0px #3dc21b; background-color:#44c767; border-radius:11px; display:inline-block; cursor:pointer; color:#ffffff; font-family:Arial; font-size:16px; font-weight:bold; padding:16px 35px; text-decoration:none;}.button:hover{background-color:#00e489;}.button:active{position:relative; top:1px;}</style>" +
                "<body> <center><img style='justify-content: center;' width='300px' src='https://www.apacsmart.com/eclaim-dev/images/signify-resize.png'></img></center> <div style='margin: 0 0 0 0; background: rgb(0,228,135); background: linear-gradient(90deg, rgba(0,228,135,1) 0%, rgba(5,224,146,1) 35%, rgba(20,212,183,1) 100%);'> <h1 style='color:white; text-align:center; margin-left: 10px;'>" + item.Title + "</h1></div> <br/> <p style='margin-left: 10px;'>Yth " + item.User.Name + ",</p>" +
                EmailBody +
                "<p style='text-align: justify; margin-left: 10px;'> Jika Anda memerlukan bantuan, silakan segera menghubungi kami melalui email " + item.EmailBantuan + " <br/> </p><p style='text-align: justify;margin-left: 10px;'>Email ini dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.</p><br/> <p style='margin-left: 10px;'>Terima Kasih</p><br/> <b> <p style='text-align: justify; margin-left: 10px;'>Salam, <br/> PT. Signify Commercial Indonesia </p></br></body></html>";

            return emailTemplate;
        }

        public string GenerateSubjectSFM(string type, SendEmailViewModel item)
        {
            var subjectresult = string.Empty;
            if (type == "Submitted claim for Distributor" || type == "Submitted claim for Approver") subjectresult = "Claim dengan no. " + item.NomorClaim + " telah diajukan";
            else if (type == "Submitted proposal for Distributor" || type == "Submitted proposal for Approver") subjectresult = "Proposal dengan no. " + item.NomorClaim + " telah diajukan";
            else if (type == "SendBack Claim" || type == "SendBack Claim CC") subjectresult = "Claim dengan no. " + item.NomorClaim + " telah ditolak";
            else if (type == "SendBack Proposal" || type == "SendBack Proposal CC" || type == "SendBack Proposal Marketing-Legal CC") subjectresult = "Proposal dengan no. " + item.NomorClaim + " telah ditolak";
            else if (type == "Approved for Distributor" || type == "Approved for Approver") subjectresult = "Claim dengan no. " + item.NomorClaim + " telah disetujui";
            else if (type == "Approved for Distributor Proposal" || type == "Approved for Approver Proposal") subjectresult = "Proposal dengan no. " + item.NomorClaim + " telah disetujui";
            else if (type == "Submmited after Send Back" || type == "Submmited after Send Back CC") subjectresult = "Claim dengan no. " + item.NomorClaim + " telah diajukan ulang";
            else if (type == "Submmited after Send Back Proposal") subjectresult = "Proposal dengan no. " + item.NomorClaim + " telah diajukan ulang";
            else if (type == "Closed Claim") subjectresult = "Claim telah selesai disetujui";
            else if (type == "Closed Proposal") subjectresult = "Proposal telah selesai disetujui";

            return subjectresult;
        }

        private static bool RedirectionUrlValidationCallback(string redirectionUrl)
        {
            // The default for the validation callback is to reject the URL.
            bool result = false;
            Uri redirectionUri = new Uri(redirectionUrl);
            // Validate the contents of the redirection URL. In this simple validation
            // callback, the redirection URL is considered valid if it is using HTTPS
            // to encrypt the authentication credentials. 
            if (redirectionUri.Scheme == "https")
            {
                result = true;
            }
            return result;
        }

        public async System.Threading.Tasks.Task<SendEmailViewModel> sendEmailViewModel(string email, string subject, string link, string root, string approvalTaskId)
        {
            var User = _context.Users.AsQueryable().Where(a => a.Email.ToLower() == email.ToLower()).FirstOrDefault();

            SendEmailViewModel send = new SendEmailViewModel()
            {
                Email = email,
                EmailBantuan = "admin.marketing@signify.com",
                User = User,
                Link = link,
                ApprovalTaskId = approvalTaskId,
                Subject = subject,
                RootLogo = link + "/images/signify-resize.png",
                CurrentRole = User.RoleName
            };

            return send;
        }

    }
}
