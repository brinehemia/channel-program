﻿using RestSharp;

namespace SemarAutomation.Controllers.Services
{
    public interface IGetResponseService
    {
        Task<IRestResponse> GetResponse(string url);
        Task<IRestResponse> GetResponseWithAuth(string url, string auth);
    }

    public class GetResponseService : IGetResponseService
    {
        public async Task<IRestResponse> GetResponse(string url)
        {
            var baseUrl = "https://funrate.algostudio.net/api/";

            var client = new RestClient(baseUrl + url);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            var response = await client.ExecuteAsync(request);

            return response;
        }

        public async Task<IRestResponse> GetResponseWithAuth(string url, string auth)
        {
            var baseUrl = "https://funrate.algostudio.net/api/";

            var client = new RestClient(baseUrl + url);
            var request = new RestRequest(Method.GET);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", "Bearer " + auth);
            var response = await client.ExecuteAsync(request);

            return response;
        }
    }
}
