﻿using Hangfire;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SemarAutomation.Data;
using SemarAutomation.Hubs;
using SemarAutomation.Models;
using SemarAutomation.Models.ViewModels;

namespace SemarAutomation.Controllers.Services
{
    public interface IStepApprovalController
    {
        StepBase Read(string IdApprovalTask, string nextStep = null, string TypeFunc = null);
        void Setter(string idApprovalTask, string CurrentStep);
        string setSerialize(object obj);
    }

    public class StepApprovalController : IStepApprovalController
    {
        protected readonly ApplicationDbContext _context;
        protected readonly RoleManager<IdentityRole> _roleManager;
        protected readonly UserManager<ApplicationUser> _userManager;
        protected readonly LogHelper _log;
        protected readonly ApplicationUser _user;
        protected SendNotification _sendNotif;
        protected IAuthorizationService _authorizationService;
        protected readonly IWebHostEnvironment _hostingEnvironment;
        protected readonly IEmailSender _emailSender;
        private readonly IGetResponseService _getResponse;

        public StepApprovalController(
            ApplicationDbContext context,
            IWebHostEnvironment hostingEnvironment,
            RoleManager<IdentityRole> roleManager,
            UserManager<ApplicationUser> userManager,
            IHubContext<NotificationHub> notificationUserHubContext,
            IUserConnectionManager userConnectionManager,
            IAuthorizationService authorizationService,
            IEmailSender emailSender,
            IGetResponseService getResponse)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            _roleManager = roleManager;
            _userManager = userManager;
            _log = new LogHelper(_context);
            _sendNotif = new SendNotification(_context, notificationUserHubContext, userConnectionManager, getResponse);
            _authorizationService = authorizationService;
            _emailSender = emailSender;
            _getResponse = getResponse;
        }

        public StepBase Read(string IdApprovalTask, string nextStep = null, string TypeFunc = null)
        {
            var getApprovalTask = _context.ApprovalTask.Where(x => x.Id == IdApprovalTask).FirstOrDefault();
            var currentStep = _context.StepBase.Where(a => a.Id == getApprovalTask.CurrentStepId).FirstOrDefault();
            var ApprovalTemplate = _context.ApprovalTemplate.Where(a => a.Id == getApprovalTask.ApprovalTemplateId).FirstOrDefault();
            StepBase NextStep = new StepBase();
            ActionViewModel GetActionTask = new ActionViewModel();

            if (TypeFunc != null)
            {
                switch (TypeFunc)
                {
                    case "OnApproved":
                        NextStep = _context.StepBase.Find(currentStep.OnApprovedId);
                        break;
                    case "OnRejected":
                        NextStep = _context.StepBase.Find(currentStep.OnRejectedId);
                        break;
                    case "OnSendBack":
                        NextStep = _context.StepBase.Find(currentStep.OnSendbackId);
                        break;
                    case "PreviousStep":
                        NextStep = _context.StepBase.Find(currentStep.PreviousStepId);
                        break;
                }
                //set current step
                Setter(IdApprovalTask, NextStep.Id);
            }

            if (nextStep != null)
            {
                NextStep = _context.StepBase.Find(nextStep);
            }

            if (NextStep.Type == StepBaseType.Action)
            {
                GetActionTask = JsonConvert.DeserializeObject<ActionViewModel>(NextStep.Description);
                try
                {
                    if (GetActionTask.function == "SendEmail")
                    {
                        try
                        {
                            var appbaseurls = MyHttpContext.AppBaseUrl.Replace("http:", "https:");
                            var wwwroot = _hostingEnvironment.WebRootPath;

                            if (getApprovalTask.Title == "SFM Proposal" || getApprovalTask.Title == "SFM Claim")
                            {
                                BackgroundJob.Enqueue(() => SendEmailSFM(getApprovalTask.Id, GetActionTask, appbaseurls, wwwroot));
                            }
                        }
                        catch (Exception e)
                        {
                            _log.SetLogChanges(_user, "StepSendEmailError-" + _user.RoleName, e.Message, null);
                        }
                    }
                    
                    NextStep = _context.StepBase.Find(NextStep.OnApprovedId);

                    //set current step
                    Setter(IdApprovalTask, NextStep.Id);

                    //rekursi read
                    Read(IdApprovalTask, NextStep.Id, null);
                }
                catch (Exception e)
                {
                    var msg = e.Message;
                    return null;
                }
            }
            else if (NextStep.Type == StepBaseType.Decision)
            {
                // SFM Proposal
                if (ApprovalTemplate.Title == "SFM Proposal")
                {
                    try
                    {
                        var decision = JsonConvert.DeserializeObject<DecisionViewModel>(NextStep.Description);
                        var context = JsonConvert.DeserializeObject<SFMProposalDetail>(getApprovalTask.Data);
                        var expr = new NCalc.Expression(decision.Condition);
                        var conditions = decision.Condition.Split(new Char[] { ' ' });

                        Func<SFMProposalDetail, bool> f = expr.ToLambda<SFMProposalDetail, bool>();

                        var conditionResult = f(context);
                        if (conditionResult)
                        {
                            //onApprove
                            NextStep = _context.StepBase.Find(NextStep.OnApprovedId);
                        }
                        else
                        {
                            //onReject
                            NextStep = _context.StepBase.Find(NextStep.OnRejectedId);
                        }

                        //set current step
                        Setter(IdApprovalTask, NextStep.Id);

                        //rekursi read
                        Read(IdApprovalTask, NextStep.Id, null);
                    }
                    catch (Exception e)
                    {
                        var msg = e.Message;
                        return null;
                    }
                }

                // SFM Claim
                else if (ApprovalTemplate.Title == "SFM Claim")
                {
                    try
                    {
                        var decision = JsonConvert.DeserializeObject<DecisionViewModel>(NextStep.Description);
                        var context = JsonConvert.DeserializeObject<SFMClaimDTO>(getApprovalTask.Data);
                        var expr = new NCalc.Expression(decision.Condition);
                        var conditions = decision.Condition.Split(new Char[] { ' ' });

                        Func<SFMClaimDTO, bool> f = expr.ToLambda<SFMClaimDTO, bool>();

                        var conditionResult = f(context);
                        if (conditionResult)
                        {
                            //onApprove
                            NextStep = _context.StepBase.Find(NextStep.OnApprovedId);
                        }
                        else
                        {
                            //onReject
                            NextStep = _context.StepBase.Find(NextStep.OnRejectedId);
                        }

                        //set current step
                        Setter(IdApprovalTask, NextStep.Id);

                        //rekursi read
                        Read(IdApprovalTask, NextStep.Id, null);
                    }
                    catch (Exception e)
                    {
                        var msg = e.Message;
                        return null;
                    }
                }
            }
            return NextStep;
        }

        public void Setter(string idApprovalTask, string CurrentStep)
        {
            var SetApproval = _context.ApprovalTask.Where(x => x.Id == idApprovalTask).FirstOrDefault();

            //set old approval
            var oldApproval = SetApproval;

            //set new current step
            SetApproval.CurrentStepId = CurrentStep;
            var currentStepData = _context.StepBase.AsNoTracking().Where(x => x.Id == CurrentStep).FirstOrDefault();
            var oldStepData = _context.StepBase.AsNoTracking().Where(x => x.Id == oldApproval.CurrentStepId).FirstOrDefault();

            if (currentStepData.Type == StepBaseType.Approval)
            {
                if (currentStepData.Title.Contains("Approval")) SetApproval.Status = "Waiting Approval from " + currentStepData.Roles;
                else if (currentStepData.Title.Contains("Edit Claim")) SetApproval.Status = currentStepData.Title.Replace("Edit Claim From", "Send Back from");
                else if (oldStepData.Title.Contains("Send Email to Marketing from Finance")) SetApproval.Status = "Send Back from Finance";
                else SetApproval.Status = currentStepData.Title;

            }

            if (currentStepData.Type == StepBaseType.Finish)
            {
                SetApproval.Status = "Closed";
            }

            _context.ApprovalTask.Update(SetApproval);

            _context.SaveChanges();
            _log.SetLogApproval(SetApproval, null, "System-Set New Step" + currentStepData.order, setSerialize(SetApproval), setSerialize(oldApproval));
        }

        public string setSerialize(object obj)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                Formatting = Formatting.None
            };
            string json = JsonConvert.SerializeObject(obj, settings);

            if (obj.GetType().Name == typeof(ApprovalTask).Name.ToString())
            {
                try
                {
                    ApprovalTask app = (ApprovalTask)obj;
                    json = JsonConvert.SerializeObject(app, settings);
                }
                catch (Exception e)
                {
                    var msg = e.Message;
                }
            }

            return json;
        }

        public async Task SendEmailSFM(string approvalTaskId, ActionViewModel action, string appbaseUrl, string webrootpath)
        {
            var user = _context.Users.AsNoTracking().Where(a => a.LockoutEnd == null && a.IsSFMUser == true).ToList();
            var approvalTask = _context.ApprovalTask.Where(a => a.Id == approvalTaskId).FirstOrDefault();
            var SFMData = JsonConvert.DeserializeObject<SFMProposalDetail>(approvalTask.Data);
            var SFMClaimDTO = JsonConvert.DeserializeObject<SFMClaimDTO>(approvalTask.Data);
            var proposal = new Proposal() { };

            if (approvalTask.Title == "SFM Proposal") proposal = _context.Proposal.FirstOrDefault(a => a.ProposalNumber == approvalTask.ClaimNumber);
            else proposal = _context.Proposal.FirstOrDefault(a => a.Id == approvalTask.EntitlementId);

            var getUserSubmitted = user.FirstOrDefault(a => a.AccountDistributor == proposal.AccountNumber);
            var dataLog = new SFMDataEmail
            {
                SendToRole = action.parameter.sendToRole,
                SendFromRole = action.parameter.SendFromRole
            };

            var Subject = action.parameter.title;
            if (approvalTask.Title == "SFM Proposal")
            {
                if (action.parameter.title == "Submitted claim for Approver") Subject = "Submitted proposal for Approver";
                else if (action.parameter.title == "SendBack Claim") Subject = "SendBack Proposal";
                else if (action.parameter.title == "Submmited after Send Back") Subject = "Submmited after Send Back Proposal";
                else if (action.parameter.title == "Approved for Approver") Subject = "Approved for Approver Proposal";
            }

            if (action.parameter.sendToRole == "Distributor")
            {
                var ApprovedSendEmailDistributor = _emailSender.sendEmailViewModel(getUserSubmitted.Email, action.parameter.title, appbaseUrl, webrootpath, approvalTaskId).Result;
                ApprovedSendEmailDistributor.TypeClaim = approvalTask.Title;
                ApprovedSendEmailDistributor.Channel = getUserSubmitted.Channel;
                ApprovedSendEmailDistributor.NomorClaim = Convert.ToInt32(approvalTask.ClaimNumber);
                ApprovedSendEmailDistributor.NamaProgram = SFMData.Program;

                ApprovedSendEmailDistributor.User = getUserSubmitted;
                ApprovedSendEmailDistributor.CurrentPICRole = action.parameter.SendFromRole;

                if (action.parameter.title == "Closed Claim" || action.parameter.title == "Closed Proposal")
                {
                    var getHistoryLog = _context.HistoryLog.Where(a => a.Action == "Send Email SFM" && a.ApprovalTaskId == approvalTask.Id && a.UserId != getUserSubmitted.Id);
                    var getApproverid = getHistoryLog.Select(a => a.UserId).Distinct().ToList();
                    var getApproverEmail = user.Where(a => getApproverid.Any(x => x == a.Id)).Select(a => a.Email);

                    ApprovedSendEmailDistributor.CCApproverSendback = string.Join(",", getApproverEmail);
                }

                if (action.parameter.title == "SendBack Claim" && action.parameter.SendFromRole != "ASM")
                {
                    var getApproverEmail = new List<string>() { };

                    getApproverEmail = user.Where(a => a.RoleName == "ASM").Select(a => a.Email).ToList();
                    var response = _getResponse.GetResponse("getasm_by_soldto?sold_to=" + proposal.AccountNumber).Result;
                    var listSoldTo = JsonConvert.DeserializeObject<SFMListResponse>(response.Content).data;

                    getApproverEmail = getApproverEmail.Where(a => listSoldTo.Any(x => x.ToLower() == a.ToLower())).ToList();

                    if (action.parameter.SendFromRole == "Legal" || action.parameter.SendFromRole == "Channel Lead" || action.parameter.SendFromRole == "Finance")
                    {
                        getApproverEmail.AddRange(user.Where(a => a.RoleName == "Marketing").Select(a => a.Email).ToList());
                    }

                    foreach (var item in getApproverEmail.Distinct())
                    {
                        var sendEmailApprover = _emailSender.sendEmailViewModel(item, action.parameter.title, appbaseUrl, webrootpath, approvalTaskId).Result;
                        sendEmailApprover.User2 = getUserSubmitted;
                        sendEmailApprover.Channel = getUserSubmitted.Channel;
                        sendEmailApprover.TypeClaim = approvalTask.Title;
                        sendEmailApprover.NomorClaim = Convert.ToInt32(approvalTask.ClaimNumber);
                        sendEmailApprover.CurrentPICRole = action.parameter.SendFromRole;

                        if (approvalTask.Title == "SFM Proposal")
                        {
                            sendEmailApprover.NamaProgram = SFMData.Program;
                        }

                        await _emailSender.SendgmailAsync(item, Subject + " CC", sendEmailApprover);
                    };

                    //ApprovedSendEmailDistributor.CCApproverSendback = string.Join(",", getApproverEmail);
                }

                await _emailSender.SendgmailAsync(getUserSubmitted.Email, Subject, ApprovedSendEmailDistributor);
                _log.SetLogSendEmailSFM(getUserSubmitted.Id, "Send Email SFM", approvalTask.Id, JsonConvert.SerializeObject(dataLog), dataLog.SendToRole);
            }
            else // Multiple Send Email
            {
                var getApprover = new List<ApplicationUser>();
                getApprover = user.Where(a => a.RoleName == action.parameter.sendToRole && a.IsSFMUser == true).ToList();

                if (action.parameter.sendToRole == "ASM")
                {
                    var response = _getResponse.GetResponse("getasm_by_soldto?sold_to=" + getUserSubmitted.AccountDistributor).Result;
                    var listSoldTo = JsonConvert.DeserializeObject<SFMListResponse>(response.Content).data;

                    getApprover = getApprover.Where(a => listSoldTo.Any(x => x.ToLower() == a.Email.ToLower())).ToList();
                }

                if (getApprover.Count() != 0)
                {
                    var getOtherApproverEmail = new List<string>() { };
                    var isCC = false;

                    foreach (var item in getApprover)
                    {
                        var SendEmail = true;
                        if (SendEmail)
                        {
                            var sendEmailApprover = _emailSender.sendEmailViewModel(item.Email, action.parameter.title, appbaseUrl, webrootpath, approvalTaskId).Result;
                            sendEmailApprover.User2 = getUserSubmitted;
                            sendEmailApprover.Channel = getUserSubmitted.Channel;
                            sendEmailApprover.TypeClaim = approvalTask.Title;
                            sendEmailApprover.NomorClaim = Convert.ToInt32(approvalTask.ClaimNumber);

                            if (approvalTask.Title == "SFM Proposal")
                            {
                                sendEmailApprover.NamaProgram = SFMData.Program;
                            }

                            if (approvalTask.CurrentPICId != null && approvalTask.CurrentPICId != string.Empty)
                            {
                                sendEmailApprover.User2 = user.Where(a => a.Id == approvalTask.CurrentPICId).FirstOrDefault();
                                sendEmailApprover.CurrentPICRole = sendEmailApprover.User2.RoleName;
                            }

                            if (action.parameter.title == "SendBack Claim" && action.parameter.SendFromRole != "ASM")
                            {
                                getOtherApproverEmail = user.Where(a => a.RoleName == "ASM").Select(a => a.Email).ToList();
                                var response = _getResponse.GetResponse("getasm_by_soldto?sold_to=" + proposal.AccountNumber).Result;
                                var listSoldTo = JsonConvert.DeserializeObject<SFMListResponse>(response.Content).data;

                                getOtherApproverEmail = getOtherApproverEmail.Where(a => listSoldTo.Any(x => x.ToLower() == a.ToLower())).ToList();

                                if (action.parameter.sendToRole != "Marketing" && (action.parameter.SendFromRole == "Legal" || action.parameter.SendFromRole == "Channel Lead" || action.parameter.SendFromRole == "Finance"))
                                {
                                    getOtherApproverEmail.AddRange(user.Where(a => a.RoleName == "Marketing").Select(a => a.Email).ToList());
                                }

                                isCC = true;
                            }

                            if (action.parameter.SendFromRole == "Legal" && sendEmailApprover.CurrentRole == "Marketing")
                            {
                                await _emailSender.SendgmailAsync(item.Email, Subject + " Marketing-Legal CC", sendEmailApprover);
                            }
                            else
                            {
                                await _emailSender.SendgmailAsync(item.Email, Subject, sendEmailApprover);
                            }

                            _log.SetLogSendEmailSFM(getUserSubmitted.Id, "Send Email SFM", approvalTask.Id, JsonConvert.SerializeObject(dataLog), dataLog.SendToRole);
                        }
                    }

                    if (action.parameter.title == "Submmited after Send Back" && action.parameter.sendToRole == "Marketing" && action.parameter.SendFromRole == "Finance")
                    {
                        getOtherApproverEmail = user.Where(a => a.RoleName == "ASM").Select(a => a.Email).ToList();
                        var response = _getResponse.GetResponse("getasm_by_soldto?sold_to=" + proposal.AccountNumber).Result;
                        var listSoldTo = JsonConvert.DeserializeObject<SFMListResponse>(response.Content).data;

                        getOtherApproverEmail = getOtherApproverEmail.Where(a => listSoldTo.Any(x => x.ToLower() == a.ToLower())).ToList();
                        getOtherApproverEmail.AddRange(user.Where(a => a.RoleName == "Channel Lead").Select(a => a.Email).ToList());
                    }

                    if (action.parameter.title == "SendBack Claim" && action.parameter.sendToRole == "Marketing")
                    {
                        getOtherApproverEmail.Add(getUserSubmitted.Email);
                    }
                    else if (action.parameter.title == "Submmited after Send Back" && action.parameter.sendToRole == "Finance") getOtherApproverEmail.Add(getUserSubmitted.Email);

                    foreach (var item in getOtherApproverEmail.Distinct())
                    {
                        var sendEmailApprover = _emailSender.sendEmailViewModel(item, action.parameter.title, appbaseUrl, webrootpath, approvalTaskId).Result;
                        sendEmailApprover.User2 = getUserSubmitted;
                        sendEmailApprover.Channel = getUserSubmitted.Channel;
                        sendEmailApprover.TypeClaim = approvalTask.Title;
                        sendEmailApprover.NomorClaim = Convert.ToInt32(approvalTask.ClaimNumber);

                        if (approvalTask.Title == "SFM Proposal") sendEmailApprover.NamaProgram = SFMData.Program;

                        if (isCC)
                        {
                            sendEmailApprover.CurrentPICRole = action.parameter.SendFromRole;

                            var SubjectSendback = Subject;
                            if (getUserSubmitted.Email != item) SubjectSendback = SubjectSendback + " CC";

                            await _emailSender.SendgmailAsync(item, SubjectSendback, sendEmailApprover);
                        }
                        else
                        {
                            if (action.parameter.title == "Submmited after Send Back" && (sendEmailApprover.CurrentRole == "ASM" || sendEmailApprover.CurrentRole == "Channel Lead"))
                            {
                                await _emailSender.SendgmailAsync(item, Subject + " CC", sendEmailApprover);
                            }
                            else
                            {
                                await _emailSender.SendgmailAsync(item, Subject, sendEmailApprover);
                            }
                        }
                    };

                }
            }

            // yg tidak ada StepBase
            if (action.parameter.title == "Approved for Approver" || action.parameter.title == "Submitted claim for Approver")
            {
                if (action.parameter.title == "Approved for Approver") { action.parameter.title = "Approved for Distributor"; Subject = "Approved for Distributor"; }
                if (action.parameter.title == "Submitted claim for Approver") { action.parameter.title = "Submitted claim for Distributor"; Subject = "Submitted claim for Distributor"; }

                if (approvalTask.Title == "SFM Proposal")
                {
                    if (action.parameter.title == "Approved for Distributor") Subject = "Approved for Distributor Proposal";
                    else if (action.parameter.title == "Submitted claim for Distributor") Subject = "Submitted proposal for Distributor";
                }

                var ApprovedSendEmailDistributor = _emailSender.sendEmailViewModel(getUserSubmitted.Email, action.parameter.title, appbaseUrl, webrootpath, approvalTaskId).Result;
                ApprovedSendEmailDistributor.TypeClaim = approvalTask.Title;
                ApprovedSendEmailDistributor.Channel = getUserSubmitted.Channel;
                ApprovedSendEmailDistributor.NomorClaim = Convert.ToInt32(approvalTask.ClaimNumber);

                if (approvalTask.Title == "SFM Proposal")
                {
                    ApprovedSendEmailDistributor.NamaProgram = SFMData.Program;
                }

                ApprovedSendEmailDistributor.User = getUserSubmitted;
                ApprovedSendEmailDistributor.CurrentPICRole = action.parameter.sendToRole;

                if (approvalTask.CurrentPICId != null || approvalTask.CurrentPICId != string.Empty)
                {
                    ApprovedSendEmailDistributor.User2 = user.Where(a => a.Id == approvalTask.CurrentPICId).FirstOrDefault();
                }
                await _emailSender.SendgmailAsync(getUserSubmitted.Email, Subject, ApprovedSendEmailDistributor);
                _log.SetLogSendEmailSFM(getUserSubmitted.Id, "Send Email SFM", approvalTask.Id, JsonConvert.SerializeObject(dataLog), dataLog.SendToRole);
            }
        }

    }
}
