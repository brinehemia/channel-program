﻿using RestSharp;

namespace SemarAutomation.Controllers.Services
{
    public interface IPutResponseService
    {
        Task<IRestResponse> PutResponseWithAuth(string url, string auth, string obj);
    }

    public class PutResponseService : IPutResponseService
    {
        public async Task<IRestResponse> PutResponseWithAuth(string url, string auth, string obj)
        {
            var baseUrl = "https://funrate.algostudio.net/api/";

            var client = new RestClient(baseUrl + url);
            var request = new RestRequest(Method.PUT);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", "Bearer " + auth);
            request.AddJsonBody(obj);
            var response = await client.ExecuteAsync(request);

            return response;
        }

    }
}
