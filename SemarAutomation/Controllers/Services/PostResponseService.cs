﻿using RestSharp;

namespace SemarAutomation.Controllers.Services
{
    public interface IPostResponseService
    {
        Task<IRestResponse> PostResponse(string url, string obj);
    }

    public class PostResponseService : IPostResponseService
    {
        public async Task<IRestResponse> PostResponse(string url, string obj)
        {
            var baseUrl = "https://funrate.algostudio.net/api/";

            var client = new RestClient(baseUrl + url);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(obj);
            var response = await client.ExecuteAsync(request);

            return response;
        }


    }
}
