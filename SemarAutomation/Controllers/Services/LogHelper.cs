﻿using Newtonsoft.Json;
using SemarAutomation.Data;
using SemarAutomation.Models;

namespace SemarAutomation.Controllers.Services
{
    public class LogHelper
    {
        private ApplicationDbContext _context;

        public LogHelper(ApplicationDbContext context)
        {
            _context = context;
        }

        public void SetLogUpload(ApplicationUser user, string action, string file)
        {
            List<HistoryLog> dataLog = new List<HistoryLog>();
            string userId = null;
            if (user != null)
            {
                userId = user.Id;
            }
            dataLog.Add(new HistoryLog
            {
                UserId = userId,
                Action = action,
                NewData = file,
                CreatedAt = DateTime.Now,
            });
            _context.HistoryLog.AddRange(dataLog);
            _context.SaveChanges();
        }

        public void SetLogUploadUserEntitlement(string action, SFMEntitlement Data)
        {
            string userId = _context.Users.Where(a => a.AccountDistributor == Data.Account).Select(a => a.Id).FirstOrDefault();
            HistoryLog dataLog = new HistoryLog()
            {
                UserId = userId,
                Action = action,
                NewData = JsonConvert.SerializeObject(Data),
                CreatedAt = DateTime.Now
            };
            _context.HistoryLog.Add(dataLog);
            _context.SaveChanges();
        }

        public void SetLogSendEmail(string iduser, string action, string ApprovalTaskId, string Data)
        {
            var SendLog = new HistoryLog()
            {
                UserId = iduser,
                Action = action,
                ApprovalTaskId = ApprovalTaskId,
                NewData = Data,
                CreatedAt = DateTime.Now
            };
            _context.HistoryLog.Add(SendLog);
            _context.SaveChanges();
        }

        public void SetLogSendEmailSFM(string iduser, string action, string ApprovalTaskId, string Data, string SendTo)
        {
            var SendLog = new HistoryLog()
            {
                UserId = iduser,
                Action = action,
                ApprovalTaskId = ApprovalTaskId,
                NewData = Data,
                OldData = SendTo,
                CreatedAt = DateTime.Now
            };
            _context.HistoryLog.Add(SendLog);
            _context.SaveChanges();
        }

        public void SetLogApproval(ApprovalTask approvalTask, ApplicationUser user, string action, string newData, string oldData, string comment = null)
        {
            string userId = null;
            if (user != null)
            {
                userId = user.Id;
            }
            string approvalTaskId = null;
            if (approvalTask != null)
            {
                approvalTaskId = approvalTask.Id;
            }
            HistoryLog dataLog = new HistoryLog
            {
                ApprovalTaskId = approvalTaskId,
                UserId = userId,
                Action = action,
                NewData = newData,
                OldData = oldData,
                Comment = comment,
                CreatedAt = DateTime.Now
            };
            _context.HistoryLog.Add(dataLog);
            _context.SaveChanges();
        }

        public void SetLogChanges(ApplicationUser user, string action, string newData, string oldData)
        {
            string userId = null;
            if (user.Id != null)
            {
                userId = user.Id;
            }
            HistoryLog dataLog = new HistoryLog
            {
                UserId = userId,
                Action = action,
                NewData = newData,
                OldData = oldData,
                CreatedAt = DateTime.Now,
            };
            _context.HistoryLog.Add(dataLog);
            _context.SaveChanges();
        }

        public string SetEmailSend(string SendTo, string Action, string Status, string Data, string Id)
        {
            var result = string.Empty;
            if (Status == "Processed" && (Id == null || Id == string.Empty))
            {
                Guid id = Guid.NewGuid();
                result = id.ToString();
                SendEmailLog emailLog = new SendEmailLog
                {
                    Id = id.ToString(),
                    SendTo = SendTo,
                    Action = Action,
                    Status = Status,
                    Data = Data,
                    CreatedAt = DateTime.Now,
                };
                _context.SendEmailLog.Add(emailLog);
                _context.SaveChanges();
            }
            else
            {
                var get = _context.SendEmailLog.Where(a => a.Id == Id).FirstOrDefault();
                get.Status = Status;
                get.Data = Data;

                _context.SendEmailLog.Update(get);
                _context.SaveChanges();
            }

            return result;
        }

        public void SetLogReport(ApplicationUser user, string action, string newData, string oldData)
        {
            string userId = null;
            if (user.Id != null)
            {
                userId = user.Id;
            }

            HistoryLog dataLog = new HistoryLog
            {
                UserId = userId,
                Action = action,
                NewData = newData,
                OldData = oldData,
                CreatedAt = DateTime.Now,
            };
            _context.HistoryLog.Add(dataLog);
            _context.SaveChanges();
        }
    }
}