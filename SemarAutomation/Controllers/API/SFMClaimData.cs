﻿using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SemarAutomation.Controllers.Services;
using SemarAutomation.Data;
using SemarAutomation.Models;
using SemarAutomation.Models.ViewModels;
using System.Globalization;

namespace SemarAutomation.Controllers.API
{
    [Route("Api/[controller]/[action]")]
    public class SFMClaimData : Controller
    {
        private ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IPostResponseService _postResponse;
        private readonly IGetResponseService _getResponse;

        public SFMClaimData(
            ApplicationDbContext context,
            UserManager<ApplicationUser> userManager,
            IPostResponseService postResponse,
            IGetResponseService getResponse)
        {
            _context = context;
            _userManager = userManager;
            _postResponse = postResponse;
            _getResponse = getResponse;
        }

        [HttpGet]
        public object GetDataProgram(DataSourceLoadOptions loadOptions)
        {
            var get = _context.SFMProgram.AsNoTracking().Select(a =>
                new EditProgram
                {
                    Id = a.Id,
                    IdProgram = a.CreatedAt.ToString("yyyy") + a.Id,
                    ProgramName = a.ProgramName,
                    PPR = a.PPR,
                    StartDate = a.StartDate.ToString("dd MMM yyyy"),
                    DefaultExpDate = a.DefaultExpDate.ToString("dd MMM yyyy")
                }).ToList();
            return DataSourceLoader.Load(get, loadOptions);
        }

        [HttpGet]
        public object GetTipeProposal(DataSourceLoadOptions loadOptions)
        {
            var get = _context.SFMProposalType.AsNoTracking().Select(a =>
                new
                {
                    Id = a.Id,
                    Name = a.Name
                }).ToList();
            return DataSourceLoader.Load(get, loadOptions);
        }

        [HttpGet]
        public object GetSubTipeProposal(DataSourceLoadOptions loadOptions, string proposalType)
        {
            if (!string.IsNullOrEmpty(proposalType))
            {
                var result = _context.SFMFileUploadType.AsNoTracking().Where(a => a.ProposalType == proposalType).Select(a => a.ProposalSubType).ToList();
                return DataSourceLoader.Load(result, loadOptions);
            }
            else return DataSourceLoader.Load(new List<string>() { }, loadOptions);
        }

        [HttpGet]
        public object GetEntitlement(DataSourceLoadOptions loadOptions, string viewDash)
        {
            var _user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            var defaultData = _context.SFMEntitlement.Where(x => x.EntitlementValue != 0).Select(a =>
            new SFMEntitlementDTO
            {
                Id = a.Id,
                Account = a.Account,
                ProgramName = a.ProgramName,
                ExpiredDate = a.ExpiredDate,
                ExpiredDateView = a.ExpiredDate.ToString("dd MMMM yyyy"),
                DistributorName = _context.Users.FirstOrDefault(z => z.AccountDistributor == a.Account).Name,
                EntitlementValue = a.EntitlementValue,
                EntitlementBalance = a.EntitlementBalance,
                PPR = a.PPR,
                Remark = a.Remark,
                IsRejected = a.IsRejected,
                LastClaimNumber = a.LastClaimNumber,
                CreatedAt = a.CreatedAt,
                UpdatedAt = a.UpdatedAt
            }).ToList();

            if (!string.IsNullOrEmpty(viewDash))
            {
                if (viewDash == "60DayExp") defaultData = defaultData.Where(c => (c.ExpiredDate != null || c.ExpiredDate != DateTime.MinValue) && (c.ExpiredDate - DateTime.UtcNow.AddHours(7)).Days >= 0 && (c.ExpiredDate - DateTime.UtcNow.AddHours(7)).Days <= 60).Where(a => a.EntitlementBalance != 0).ToList();
            }

            if (User.IsInRole("Distributor")) defaultData = defaultData.Where(x => x.Account == _user.AccountDistributor).ToList();
            else if (User.IsInRole("ASM"))
            {
                var auth = User.Claims.First(a => a.Type == "token").Value;
                var response = _getResponse.GetResponseWithAuth("soldto", auth).Result;
                var listSoldTo = JsonConvert.DeserializeObject<ResponseSoldTo>(response.Content);

                defaultData = defaultData.Where(x => listSoldTo.data.Contains(x.Account)).ToList();
            }

            foreach (var item in defaultData)
            {
                item.Style = SetStyleDate(item.ExpiredDate);
            }

            return DataSourceLoader.Load(defaultData, loadOptions);
        }

        [HttpGet]
        public object GetProposal(DataSourceLoadOptions loadOptions, string viewDash)
        {
            var _user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            var defaultData = (from a in _context.Proposal
                               join c in _context.ApprovalTask on a.ApprovalTaskId equals c.Id
                               join b in _context.StepBase on c.CurrentStepId equals b.Id
                               join d in _context.Users on a.AccountNumber equals d.AccountDistributor
                               select new ProposalList
                               {
                                   Id = a.Id,
                                   ApprovalTaskId = a.ApprovalTaskId,
                                   AccountNumber = a.AccountNumber,
                                   DistributorName = d.CompanyDistributor,
                                   NamaAktivitas = a.NamaAktivitas,
                                   StatusApproval = c.Status,
                                   TanggalAktivitas = a.TanggalAktivitas,
                                   TipeAktivitas = a.TipeAktivitas,
                                   Kategori = a.Kategori,
                                   KPI = string.IsNullOrEmpty(a.KPI) ? a.KPI : string.Join(", ", JsonConvert.DeserializeObject<List<string>>(a.KPI)),
                                   Nominal = a.Nominal,
                                   SumberBudget = a.SumberBudget,
                                   ProgramName = c.Data,
                                   Opportunity = a.Opportunity,
                                   ProposalNumber = a.ProposalNumber,
                                   Status = a.Status,
                                   SegmentPasar = a.SegmentPasar,
                                   Color = c.Status.Contains("Waiting Approval") ? "#666666" : "white",
                                   Roles = b.Roles,
                                   CreatedAt = c.CreatedAt
                               }).ToList();

            if (!string.IsNullOrEmpty(viewDash))
            {
                if (viewDash == "Inprogress") defaultData = defaultData.Where(a => a.Status != "Canceled" && a.Status != "Closed" && (a.StatusApproval.Contains("Waiting Approval") || a.StatusApproval.Contains("Send Back"))).ToList();
                else if (viewDash == "Canceled") defaultData = defaultData.Where(a => a.Status == "Canceled").ToList();
            }

            if (User.IsInRole("Distributor")) defaultData = defaultData.Where(x => x.AccountNumber == _user.AccountDistributor).ToList();
            else if (_user.RoleName == "ASM")
            {
                var auth = User.Claims.First(a => a.Type == "token").Value;
                var response = _getResponse.GetResponseWithAuth("soldto", auth).Result;
                var listSoldTo = JsonConvert.DeserializeObject<ResponseSoldTo>(response.Content);

                defaultData = defaultData.Where(x => listSoldTo.data.Contains(x.AccountNumber)).ToList();
            }

            foreach (var item in defaultData)
            {
                item.Style = SetStyle(item.StatusApproval);
                item.TextButton = item.StatusApproval == "Rejected" ? "View" : SetText(_user.RoleName, item.Roles);

                if (item.StatusApproval.Contains("Marketing")) item.StatusApproval = item.StatusApproval.Replace("Marketing", "Channel Marketing");
                item.StatusApproval = item.StatusApproval.Contains("Waiting Approval from") ? item.StatusApproval.Replace("Waiting Approval from", "Menunggu disetujui") : item.StatusApproval;
                item.StatusApproval = item.StatusApproval.Contains("Send Back from") ? item.StatusApproval.Replace("Send Back from", "Ditolak oleh") : item.StatusApproval;

                if (item.Status == "Canceled")
                {
                    item.StatusApproval = "Dibatalkan oleh distributor";
                    item.Style = "danger";
                    item.TextButton = "View";
                }

                var proposalDetail = JsonConvert.DeserializeObject<SFMProposalDetail>(item.ProgramName);
                item.ProgramName = proposalDetail.Program;

                var claimProposal = _context.ApprovalTask.Where(a => a.Title == "SFM Claim" && a.EntitlementId == item.Id && a.Status != "Canceled").ToList();
                if (claimProposal.Count() != 0) item.ActualAmount = claimProposal.Sum(a => JsonConvert.DeserializeObject<SFMClaimDTO>(a.Data).NominalClaim);
            }

            return DataSourceLoader.Load(defaultData.OrderByDescending(a => a.CreatedAt), loadOptions);
        }

        [HttpGet]
        public object GetClaim(DataSourceLoadOptions loadOptions, string viewDash)
        {
            var _user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            var defaultData = (from c in _context.ApprovalTask
                               where c.Title == "SFM Claim"
                               join a in _context.Proposal on c.EntitlementId equals a.Id
                               join v in _context.Users on a.AccountNumber equals v.AccountDistributor
                               join b in _context.StepBase on c.CurrentStepId equals b.Id
                               select new ClaimList
                               {
                                   AccountNumber = a.AccountNumber,
                                   NamaAktivitas = a.NamaAktivitas,
                                   StatusApproval = c.Status,
                                   ClaimNumber = Convert.ToInt32(c.ClaimNumber),
                                   Status = a.Status,
                                   DistributorName = v.CompanyDistributor,
                                   Data = c.Data,
                                   Color = c.Status.Contains("Waiting Approval") ? "#666666" : "white",
                                   Roles = b.Roles,
                               }).ToList();

            if (!string.IsNullOrEmpty(viewDash))
            {
                if (viewDash == "Inprogress") defaultData = defaultData.Where(a => a.Status != "Canceled" && (a.StatusApproval.Contains("Waiting Approval") || a.StatusApproval.Contains("Send Back"))).ToList();
                else if (viewDash == "Closed") defaultData = defaultData.Where(a => a.StatusApproval == "Closed").ToList();
            }

            foreach (var item in defaultData)
            {
                item.Style = SetStyle(item.StatusApproval);
                item.TextButton = item.StatusApproval == "Rejected" ? "View" : SetText(_user.RoleName, item.Roles);

                if (item.StatusApproval.Contains("Marketing")) item.StatusApproval = item.StatusApproval.Replace("Marketing", "Channel Marketing");
                item.StatusApproval = item.StatusApproval.Contains("Waiting Approval from") ? item.StatusApproval.Replace("Waiting Approval from", "Menunggu disetujui") : item.StatusApproval;
                item.StatusApproval = item.StatusApproval.Contains("Send Back from") ? item.StatusApproval.Replace("Send Back from", "Ditolak oleh") : item.StatusApproval;

                if (item.StatusApproval == "Canceled")
                {
                    item.StatusApproval = "Dibatalkan oleh distributor";
                    item.Style = "danger";
                    item.TextButton = "View";
                }

                var claimDetail = JsonConvert.DeserializeObject<SFMClaimDTO>(item.Data);
                item.ClaimDate = claimDetail.ClaimDate;
                item.LastUpdate = claimDetail.LastUpdate;
                item.NominalClaim = item.StatusApproval == "Closed" ? claimDetail.AmountFinance : claimDetail.NominalClaim;
            }

            if (User.IsInRole("Distributor")) defaultData = defaultData.Where(x => x.AccountNumber == _user.AccountDistributor).ToList();
            else if (_user.RoleName == "ASM")
            {
                var auth = User.Claims.First(a => a.Type == "token").Value;
                var response = _getResponse.GetResponseWithAuth("soldto", auth).Result;
                var listSoldTo = JsonConvert.DeserializeObject<ResponseSoldTo>(response.Content);

                defaultData = defaultData.Where(x => listSoldTo.data.Contains(x.AccountNumber)).ToList();
            }

            return DataSourceLoader.Load(defaultData.OrderByDescending(a => a.ClaimDate), loadOptions);
        }

        [HttpGet]
        public object GetApprovedProposal(DataSourceLoadOptions loadOptions)
        {
            var _user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            var defaultData = (from a in _context.Proposal
                               where a.Status == "Closed"
                               join c in _context.ApprovalTask on a.ApprovalTaskId equals c.Id
                               join b in _context.StepBase on c.CurrentStepId equals b.Id
                               join d in _context.Users on a.AccountNumber equals d.AccountDistributor
                               select new ProposalList
                               {
                                   Id = a.Id,
                                   ApprovalTaskId = a.ApprovalTaskId,
                                   AccountNumber = a.AccountNumber,
                                   DistributorName = d.CompanyDistributor,
                                   NamaAktivitas = a.NamaAktivitas,
                                   StatusApproval = c.Status,
                                   TanggalAktivitas = a.TanggalAktivitas,
                                   TipeAktivitas = a.TipeAktivitas,
                                   Kategori = a.Kategori,
                                   KPI = string.IsNullOrEmpty(a.KPI) ? a.KPI : string.Join(", ", JsonConvert.DeserializeObject<List<string>>(a.KPI)),
                                   Nominal = a.Nominal,
                                   NominalBalance = a.NominalBalance,
                                   SumberBudget = a.SumberBudget,
                                   ProgramName = c.Data,
                                   Opportunity = a.Opportunity,
                                   ProposalNumber = a.ProposalNumber,
                                   Status = a.Status,
                                   SegmentPasar = a.SegmentPasar,
                                   Color = c.Status.Contains("Waiting Approval") ? "#666666" : "white",
                                   Roles = b.Roles,
                                   CreatedAt = c.CreatedAt
                               }).ToList();

            if (User.IsInRole("Distributor")) defaultData = defaultData.Where(x => x.AccountNumber == _user.AccountDistributor).ToList();
            else if (_user.RoleName == "ASM")
            {
                var auth = User.Claims.First(a => a.Type == "token").Value;
                var response = _getResponse.GetResponseWithAuth("soldto", auth).Result;
                var listSoldTo = JsonConvert.DeserializeObject<ResponseSoldTo>(response.Content);

                defaultData = defaultData.Where(x => listSoldTo.data.Contains(x.AccountNumber)).ToList();
            }

            foreach (var item in defaultData)
            {
                item.Style = SetStyle(item.StatusApproval);
                item.TextButton = item.StatusApproval == "Rejected" ? "View" : SetText(_user.RoleName, item.Roles);
                item.StatusApproval = item.StatusApproval.Contains("Waiting Approval from") ? item.StatusApproval.Replace("Waiting Approval from", "Menunggu disetujui") : item.StatusApproval;

                if (!string.IsNullOrEmpty(item.SumberBudget))
                {
                    var listEntitlementId = Array.ConvertAll(item.SumberBudget.Split(",").ToArray(), a => int.Parse(a));
                    var Entitlement = _context.SFMEntitlement.Where(a => listEntitlementId.Any(z => z == a.Id)).OrderByDescending(a => a.ExpiredDate).FirstOrDefault();
                    item.ExpiredDate = Entitlement.ExpiredDate;

                    var isExpired = "Yes";
                    if (item.ExpiredDate.Date >= DateTime.UtcNow.AddHours(7).Date) isExpired = "No";

                    item.isExpired = isExpired;
                    item.ExpiredDateView = item.ExpiredDate.ToString("dd MMMM yyyy");
                    item.Style = SetStyleDate(item.ExpiredDate);
                }

                var proposalDetail = JsonConvert.DeserializeObject<SFMProposalDetail>(item.ProgramName);
                item.ProgramName = proposalDetail.Program;

                var claimProposal = _context.ApprovalTask.Where(a => a.Title == "SFM Claim" && a.EntitlementId == item.Id && a.Status != "Canceled").ToList();
                if (claimProposal.Count() != 0) item.ActualAmount = claimProposal.Sum(a => JsonConvert.DeserializeObject<SFMClaimDTO>(a.Data).NominalClaim);

            }

            return DataSourceLoader.Load(defaultData.OrderByDescending(a => a.CreatedAt), loadOptions);
        }

        [HttpGet]
        public object GetSumberBudget(DataSourceLoadOptions loadOptions, string date, string AllType, string programName)
        {
            var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            var programList = _context.SFMProgram.AsQueryable();
            var minLimit = DateTime.Today;
            var maxLimit = DateTime.Today;
            var month = DateTime.Today.Month;

            if (!string.IsNullOrEmpty(date))
            {
                DateTime.TryParseExact(date, "M-dd-yyyy", null, DateTimeStyles.None, out minLimit);
                DateTime.TryParseExact(date, "M-dd-yyyy", null, DateTimeStyles.None, out maxLimit);

                //minLimit = DateTime.TryParse(date, "MM-dd-yyyy", CultureInfo.InvariantCultur,);
                //maxLimit = Convert.ToDateTime(date);
                month = maxLimit.Month;
            }

            if (month == 1 || month == 2 || month == 3)
            {
                minLimit = new DateTime(minLimit.Year, 1, 1);
                maxLimit = new DateTime(maxLimit.Year, 6, DateTime.DaysInMonth(maxLimit.Year, 6));
            }
            else if (month == 4 || month == 5 || month == 6)
            {
                minLimit = new DateTime(minLimit.Year, 4, 1);
                maxLimit = new DateTime(maxLimit.Year, 9, DateTime.DaysInMonth(maxLimit.Year, 9));
            }
            else if (month == 7 || month == 8 || month == 9)
            {
                minLimit = new DateTime(minLimit.Year, 7, 1);
                maxLimit = new DateTime(maxLimit.Year, 12, DateTime.DaysInMonth(maxLimit.Year, 12));
            }
            else if (month == 10 || month == 11 || month == 12)
            {
                minLimit = new DateTime(minLimit.Year, 10, 1);
                maxLimit = new DateTime(maxLimit.AddYears(1).Year, 3, DateTime.DaysInMonth(maxLimit.AddYears(1).Year, 3));
            }

            var result = _context.SFMEntitlement.Where(a => a.ExpiredDate >= minLimit && a.ExpiredDate <= maxLimit && a.EntitlementBalance > 0).ToList();

            result = result.Where(a => a.ExpiredDate >= DateTime.UtcNow.AddHours(7).Date).ToList();
            if (user.RoleName == "Distributor") result = result.Where(a => a.Account == user.AccountDistributor).ToList();

            //foreach (var item in result)
            //{
            //    var program = programList.FirstOrDefault(a => a.ProgramName == item.ProgramName);
            //    if (program != null) item.PPR = program.PPR;
            //}

            //if (!string.IsNullOrEmpty(programName)) {
            //    var program = _context.SFMProgram.FirstOrDefault(a => a.ProgramName == programName);
            //    if (program != null)
            //    {
            //        var ProgramNameList = _context.SFMProgram.Where(a => a.PPR == program.PPR).Select(a => a.ProgramName).ToArray();
            //        result = result.Where(a => ProgramNameList.Contains(a.ProgramName)).ToList();
            //    }
            //}

            return DataSourceLoader.Load(result, loadOptions);
        }

        [HttpGet]
        public object GetDetailProgramUser(DataSourceLoadOptions loadOptions, int ProgramId = 0)
        {
            var result = new List<object>() { };
            if (ProgramId != 0)
            {
                var get = _context.ProgramSFMDistributor.Where(x => x.DataProgramId == ProgramId).Select(x => x.UserDistributorNumber);
                var userList = _context.Users.Where(a => a.RoleName == "Distributor").ToList();

                var userSFM = _getResponse.GetResponse("list_om").Result;
                var userSFMList = JsonConvert.DeserializeObject<ReponseListUser>(userSFM.Content).data;

                foreach (var item in get)
                {
                    var addItem = new object() { };
                    var user = userList.FirstOrDefault(a => a.AccountDistributor == item);
                    if (user == null)
                    {
                        var UserAddedSFM = userSFMList.FirstOrDefault(a => a.sold_to == item);
                        if (UserAddedSFM == null) continue;
                        addItem = new
                        {
                            Account = UserAddedSFM.sold_to,
                            Name = UserAddedSFM.name,
                            Email = UserAddedSFM.email
                        };
                    }
                    else
                    {
                        addItem = new
                        {
                            Account = user.AccountDistributor,
                            Name = user.Name,
                            Email = user.Email
                        };
                    }

                    result.Add(addItem);
                }
            }

            return DataSourceLoader.Load(result, loadOptions);
        }

        [HttpGet]
        public object GetOpportunity(DataSourceLoadOptions loadOptions, int ProgramId = 0)
        {
            var token = HttpContext.User.Claims.First(x => x.Type == "token").Value;
            var response = _getResponse.GetResponseWithAuth("opportunity", token).Result;
            var result = JsonConvert.DeserializeObject<SFMListResponse>(response.Content).data;

            return DataSourceLoader.Load(result, loadOptions);
        }

        [HttpGet]
        public object GetPPRProgram(DataSourceLoadOptions loadOptions)
        {
            var result = _context.SFMProgram.Select(a => new { name = a.PPR }).Distinct().ToList();
            return DataSourceLoader.Load(result, loadOptions);
        }

        [HttpGet]
        public object GetDetailEntitlement(DataSourceLoadOptions loadOptions, int entitlementId)
        {
            var dataList = new List<DetailSFMEntitlement>();
            var approval = (from a in _context.ApprovalTask
                            where a.Title == "SFM Proposal"
                            join c in _context.Proposal on a.ClaimNumber equals c.ProposalNumber
                            select new
                            {
                                ClaimNumber = a.ClaimNumber,
                                AccountDistributor = c.AccountNumber,
                                Data = JsonConvert.DeserializeObject<SFMProposalDetail>(a.Data),
                                UpdatedAt = a.UpdatedAt,
                                Status = a.Status,
                                StatusProposal = c.Status
                            }).ToList();

            foreach (var item in approval)
            {
                if (!item.Data.DetailBalanceTaken.Any(a => a.EntitlementId == entitlementId)) continue;

                var dataListClaim = new DetailSFMEntitlement();
                var claimNumber = item.ClaimNumber.ToString();
                var LastUpdate = item.UpdatedAt.ToString("dd MMMM yyyy, HH:mm");

                dataListClaim.Date = item.Data.ProposalDate;

                var entitlementDetail = item.Data.DetailBalanceTaken.FirstOrDefault(a => a.EntitlementId == entitlementId);
                dataListClaim.Value = entitlementDetail.Nominal;
                dataListClaim.ValueTotal = item.Data.Nominal;

                dataListClaim.ProgramName = item.Data.Program;
                dataListClaim.DistributorAccount = item.AccountDistributor;

                dataListClaim.Type = "Proposal - " + claimNumber;

                var setStatus = item.Status;
                if (setStatus.Contains("Marketing")) setStatus = setStatus.Replace("Marketing", "Channel Marketing");
                setStatus = setStatus.Contains("Waiting Approval from") ? setStatus.Replace("Waiting Approval from", "Menunggu disetujui") : setStatus;
                setStatus = setStatus.Contains("Send Back from") ? setStatus.Replace("Send Back from", "Ditolak oleh") : setStatus;

                dataListClaim.Remark = setStatus + " - " + LastUpdate;
                if (item.StatusProposal == "Canceled") dataListClaim.Remark = "Dibatalkan Distributor";

                dataList.Add(dataListClaim);
            }

            return DataSourceLoader.Load(dataList.OrderByDescending(x => x.Date), loadOptions);
        }

        [HttpGet]
        public object GetDetailClaim(DataSourceLoadOptions loadOptions, int entitlementId)
        {
            var dataList = new List<DetailSFMEntitlement>();
            var approval = (from a in _context.ApprovalTask
                            where a.Title == "SFM Claim" && a.EntitlementId == entitlementId
                            join c in _context.Proposal on a.EntitlementId equals c.Id
                            select new
                            {
                                ClaimNumber = a.ClaimNumber,
                                AccountDistributor = c.AccountNumber,
                                Data = JsonConvert.DeserializeObject<SFMClaimDTO>(a.Data),
                                UpdatedAt = a.UpdatedAt,
                                Status = a.Status,
                                StatusProposal = c.Status
                            }).ToList();

            foreach (var item in approval)
            {
                var dataListClaim = new DetailSFMEntitlement();
                var claimNumber = item.ClaimNumber.ToString();
                var LastUpdate = item.UpdatedAt.ToString("dd MMMM yyyy, HH:mm");

                dataListClaim.Date = item.Data.ClaimDate;

                var entitlementDetail = item.Data.NominalClaim;
                if (item.Status == "Closed") entitlementDetail = item.Data.AmountFinance;

                dataListClaim.Value = entitlementDetail;

                dataListClaim.DistributorAccount = item.AccountDistributor;

                dataListClaim.Type = "Claim - " + claimNumber;

                var setStatus = item.Status;
                if (setStatus.Contains("Marketing")) setStatus = setStatus.Replace("Marketing", "Channel Marketing");
                setStatus = setStatus.Contains("Waiting Approval from") ? setStatus.Replace("Waiting Approval from", "Menunggu disetujui") : setStatus;
                setStatus = setStatus.Contains("Send Back from") ? setStatus.Replace("Send Back from", "Ditolak oleh") : setStatus;

                dataListClaim.Remark = setStatus + " - " + LastUpdate;
                if (item.StatusProposal == "Canceled") dataListClaim.Remark = "Dibatalkan Distributor";

                dataList.Add(dataListClaim);
            }

            return DataSourceLoader.Load(dataList.OrderByDescending(x => x.Date), loadOptions);
        }

        [HttpGet]
        public object GetHistoryLogApproval(DataSourceLoadOptions loadOptions, string approvalId)
        {
            var historyLog = _context.HistoryLog.AsNoTracking().Where(a => a.Action.Contains("SendEmailSuccess") == false && a.Action.Contains("SendEmailError") == false && a.Action != "Send Email SFM").Where(x => x.ApprovalTaskId == approvalId && x.UserId != null && !x.Action.Contains("System") && !x.Action.Contains("Change"))
                .Select(x => new
                {
                    Id = x.Id,
                    User = _context.Users.AsNoTracking().Where(u => u.Id == x.UserId).Select(u => u.Name + " (" + u.RoleName + ")").FirstOrDefault(),
                    CreatedAt = x.CreatedAt,
                    Action = x.Action,
                    Comment = x.Comment
                }).ToList();

            var result = new List<object>() { };
            foreach (var item in historyLog)
            {
                result.Add(new
                {
                    Id = item.Id,
                    User = item.User.Contains("(Marketing)") ? item.User.Replace("(Marketing)", "(Channel Marketing)") : item.User,
                    CreatedAt = item.CreatedAt,
                    Action = item.Action.Contains("- Marketing") ? item.Action.Replace("- Marketing", "- Channel Marketing") : item.Action,
                    Comment = item.Comment,
                });
            }

            return DataSourceLoader.Load(result, loadOptions);
        }

        [HttpGet]
        public object PendingClaimChart(DataSourceLoadOptions loadOptions)
        {
            var getUser = _userManager.GetUserAsync(HttpContext.User).Result;
            List<ChartSFMViewModel> ListPending = new List<ChartSFMViewModel>();
            var defaultData = (from c in _context.ApprovalTask
                               where c.Title == "SFM Claim" && c.Status != "Canceled"
                               join b in _context.StepBase on c.CurrentStepId equals b.Id
                               join a in _context.Proposal on c.EntitlementId equals a.Id
                               select new
                               {
                                   AccountNumber = a.AccountNumber,
                                   UserId = c.SubmittedById,
                                   Data = JsonConvert.DeserializeObject<SFMClaimDTO>(c.Data),
                                   Status = c.Status,
                                   Roles = b.Roles
                               }).ToList();

            if (getUser.RoleName == "Distributor")
            {
                defaultData = defaultData.Where(a => a.AccountNumber == getUser.AccountDistributor).ToList();
            }
            else if (getUser.RoleName == "ASM")
            {
                var response = new List<string>() { };

                var token = HttpContext.User.Claims.First(a => a.Type == "token").Value;
                var request = _getResponse.GetResponseWithAuth("soldto", token).Result;
                if ((int)request.StatusCode == 200) response = JsonConvert.DeserializeObject<SFMListResponse>(request.Content).data;

                defaultData = defaultData.Where(a => response.Any(x => x == a.AccountNumber)).ToList();
            }

            var getDistributor = defaultData.Where(x => x.Status.Contains("Send Back")).Sum(a => a.Data.NominalClaim);
            var getDistributorCount = defaultData.Where(x => x.Status.Contains("Send Back")).Count();

            var getASM = defaultData.Where(x => x.Status == "Waiting Approval from ASM").Sum(a => a.Data.NominalClaim);
            var getASMCount = defaultData.Where(x => x.Status == "Waiting Approval from ASM").Count();

            var getChannelLead = defaultData.Where(x => x.Status == "Waiting Approval from Channel Lead").Sum(a => a.Data.NominalClaim);
            var getChannelLeadCount = defaultData.Where(x => x.Status == "Waiting Approval from Channel Lead").Count();

            var getLegal = defaultData.Where(c => c.Status == "Waiting Approval from Legal").Sum(a => a.Data.NominalClaim);
            var getLegalCount = defaultData.Where(c => c.Status == "Waiting Approval from Legal").Count();

            var getMarketing = defaultData.Where(x => x.Status == "Waiting Approval from Marketing").Sum(a => a.Data.NominalClaim);
            var getMarketingCount = defaultData.Where(x => x.Status == "Waiting Approval from Marketing").Count();

            var getFinance = defaultData.Where(x => x.Status == "Waiting Approval from Finance").Sum(a => a.Data.NominalClaim);
            var getFinanceCount = defaultData.Where(x => x.Status == "Waiting Approval from Finance").Count();

            ListPending.Add(new ChartSFMViewModel { Roles = "Distributor", Value = getDistributor, Count = getDistributorCount });
            ListPending.Add(new ChartSFMViewModel { Roles = "ASM", Value = getASM, Count = getASMCount });
            ListPending.Add(new ChartSFMViewModel { Roles = "Channel Marketing", Value = getMarketing, Count = getMarketingCount });
            ListPending.Add(new ChartSFMViewModel { Roles = "Channel Lead", Value = getChannelLead, Count = getChannelLeadCount });
            ListPending.Add(new ChartSFMViewModel { Roles = "Legal", Value = getLegal, Count = getLegalCount });
            ListPending.Add(new ChartSFMViewModel { Roles = "Finance", Value = getFinance, Count = getFinanceCount });
            return DataSourceLoader.Load(ListPending, loadOptions);
        }

        [HttpGet]
        public object HistoryClaim(DataSourceLoadOptions loadOptions, int proposalNumber)
        {
            var getUser = _userManager.GetUserAsync(HttpContext.User).Result;
            List<SFMHistoryClaim> result = new List<SFMHistoryClaim>();
            var defaultData = (from c in _context.ApprovalTask
                               where c.Title == "SFM Claim"
                               join a in _context.Proposal on c.EntitlementId equals a.Id
                               where a.ProposalNumber == proposalNumber
                               select new
                               {
                                   ClaimNumber = c.ClaimNumber,
                                   Status = c.Status,
                                   NamaAktivitas = a.NamaAktivitas,
                                   Data = JsonConvert.DeserializeObject<SFMClaimDTO>(c.Data),
                               }).ToList();

            foreach (var item in defaultData)
            {
                result.Add(new SFMHistoryClaim()
                {
                    ClaimNumber = Convert.ToInt32(item.ClaimNumber),
                    ClaimDate = item.Data.ClaimDate,
                    NamaAktivitas = item.NamaAktivitas,
                    Nominal = item.Status == "Closed" ? item.Data.AmountFinance : item.Data.NominalClaim
                });
            }

            return DataSourceLoader.Load(result, loadOptions);
        }

        [HttpGet]
        public object PendingProposalChart(DataSourceLoadOptions loadOptions)
        {
            var getUser = _userManager.GetUserAsync(HttpContext.User).Result;
            List<ChartSFMViewModel> ListPending = new List<ChartSFMViewModel>();
            var defaultData = (from c in _context.ApprovalTask
                               where c.Title == "SFM Proposal"
                               join b in _context.StepBase on c.CurrentStepId equals b.Id
                               join a in _context.Proposal on c.ClaimNumber equals a.ProposalNumber
                               where a.Status != "Canceled"
                               select new
                               {
                                   AccountNumber = a.AccountNumber,
                                   UserId = c.SubmittedById,
                                   Data = JsonConvert.DeserializeObject<SFMProposalDetail>(c.Data),
                                   Status = c.Status,
                                   Roles = b.Roles
                               }).ToList();

            if (getUser.RoleName == "Distributor")
            {
                defaultData = defaultData.Where(a => a.AccountNumber == getUser.AccountDistributor).ToList();
            }
            else if (getUser.RoleName == "ASM")
            {
                var response = new List<string>() { };

                var token = HttpContext.User.Claims.First(a => a.Type == "token").Value;
                var request = _getResponse.GetResponseWithAuth("soldto", token).Result;
                if ((int)request.StatusCode == 200) response = JsonConvert.DeserializeObject<SFMListResponse>(request.Content).data;

                defaultData = defaultData.Where(a => response.Any(x => x == a.AccountNumber)).ToList();
            }

            var getDistributor = defaultData.Where(x => x.Status.Contains("Send Back")).Sum(a => a.Data.Nominal);
            var getDistributorCount = defaultData.Where(x => x.Status.Contains("Send Back")).Count();

            var getASM = defaultData.Where(x => x.Status == "Waiting Approval from ASM").Sum(a => a.Data.Nominal);
            var getASMCount = defaultData.Where(x => x.Status == "Waiting Approval from ASM").Count();

            var getLegal = defaultData.Where(x => x.Status == "Waiting Approval from Legal").Sum(a => a.Data.Nominal);
            var getLegalCount = defaultData.Where(x => x.Status == "Waiting Approval from Legal").Count();

            var getMarketing = defaultData.Where(x => x.Status == "Waiting Approval from Marketing").Sum(a => a.Data.Nominal);
            var getMarketingCount = defaultData.Where(x => x.Status == "Waiting Approval from Marketing").Count();

            ListPending.Add(new ChartSFMViewModel { Roles = "Distributor", Value = getDistributor, Count = getDistributorCount });
            ListPending.Add(new ChartSFMViewModel { Roles = "ASM", Value = getASM, Count = getASMCount });
            ListPending.Add(new ChartSFMViewModel { Roles = "Channel Marketing", Value = getMarketing, Count = getMarketingCount });
            ListPending.Add(new ChartSFMViewModel { Roles = "Legal", Value = getLegal, Count = getLegalCount });
            return DataSourceLoader.Load(ListPending, loadOptions);
        }

        [HttpGet]
        public object GetDistributorList(DataSourceLoadOptions loadOptions)
        {
            var get = _getResponse.GetResponse("list_om").Result;
            var result = JsonConvert.DeserializeObject<ReponseListUser>(get.Content).data;

            return DataSourceLoader.Load(result, loadOptions);
        }

        [HttpGet]
        public object TahunFilter(DataSourceLoadOptions loadOptions)
        {
            var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            var result = _context.SFMEntitlement.Select(a => new { Text = a.ExpiredDate.Year.ToString(), Name = a.ExpiredDate.Year });

            return DataSourceLoader.Load(result.Distinct(), loadOptions);
        }

        [HttpGet]
        public object DistributorFilter(DataSourceLoadOptions loadOptions)
        {
            var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            var result = _context.Users.Where(a => a.RoleName == "Distributor" && a.IsSFMUser == true);

            if (user.RoleName == "Distributor")
            {
                result = result.Where(a => a.AccountDistributor == user.AccountDistributor);
            }
            else if (user.RoleName == "ASM")
            {
                var auth = User.Claims.First(a => a.Type == "token").Value;
                var response = _getResponse.GetResponseWithAuth("soldto", auth).Result;
                var listSoldTo = JsonConvert.DeserializeObject<ResponseSoldTo>(response.Content);

                result = result.Where(a => listSoldTo.data.Contains(a.AccountDistributor));
            }

            return DataSourceLoader.Load(result.Select(a => new { Text = a.Email, Name = a.Email }).Distinct(), loadOptions);
        }

        [HttpGet]
        public async Task<OpportunityDetail> GetDetailOpportunity(string sfdcNumber)
        {
            var result = new OpportunityDetail() { };
            if (!string.IsNullOrEmpty(sfdcNumber))
            {
                var response = await _getResponse.GetResponse("getoppor_by_sfdc?sfdc_id=" + sfdcNumber);
                result = JsonConvert.DeserializeObject<OpportunityDetailResponse>(response.Content).data.FirstOrDefault();

                if (result.id != 0)
                {
                    result.amountString = "Rp " + result.amount.ToString("N0");
                    result.expected_order_date_DTO = result.last_modified.ToString("dd-MM-yyyy");
                    result.requested_delivery_date_DTO = result.last_modified.ToString("dd-MM-yyyy");
                    result.last_modified_DTO = result.last_modified.ToString("dd-MM-yyyy HH:mm");
                }
            }

            return result;
        }

        public object ProgramFilter(DataSourceLoadOptions loadOptions)
        {
            var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            var program = _context.SFMProgram.Select(a => new { Text = a.ProgramName, Name = a.ProgramName });

            return DataSourceLoader.Load(program.Distinct(), loadOptions);
        }

        public async Task<object> GetDataSegmen(DataSourceLoadOptions loadOptions)
        {
            var result = new SFMListResponse() { };
            var resultDTO = new List<string>() { };
            var response = await _getResponse.GetResponse("list_segment");
            result = JsonConvert.DeserializeObject<SFMListResponse>(response.Content);

            foreach (var item in result.data)
            {
                var itemresult = char.ToUpper(item[0]) + item.Substring(1);
                resultDTO.Add(itemresult);
            }

            return DataSourceLoader.Load(resultDTO, loadOptions);
        }

        public async Task<object> GetDataSubSegmen(DataSourceLoadOptions loadOptions, string Segmentname)
        {
            var result = new SFMListResponse() { };
            if (!string.IsNullOrEmpty(Segmentname))
            {
                var response = await _getResponse.GetResponse("getsubsegment_by_segment?segment=" + Segmentname);
                result = JsonConvert.DeserializeObject<SFMListResponse>(response.Content);
            }

            return DataSourceLoader.Load(result.data, loadOptions);
        }

        public async Task<object> GetProposalUploadType(string proposalType, string proposalSubType)
        {
            var result = new List<TypeFileData>() { };
            var getData = await _context.SFMFileUploadType.Where(c => c.ProposalType == proposalType && c.ProposalSubType == proposalSubType).FirstOrDefaultAsync();
            if (getData != null) result = JsonConvert.DeserializeObject<List<TypeFileData>>(getData.TypeFileProposal);

            return result;
        }

        public async Task<object> GetDescriptionFile(string proposalType, string proposalSubType)
        {
            var result = new DescriptionFile { };
            var getData = await _context.SFMFileUploadType.Where(c => c.ProposalType == proposalType && c.ProposalSubType == proposalSubType).FirstOrDefaultAsync();
            if (getData != null) result = new DescriptionFile { DetailClaim = getData.DetailClaim, DetailProposal = getData.DetailProposal };

            return result;
        }

        [HttpGet]
        public object GetNotificationReadCount(string userId)
        {
            var data = _context.NotificationApplicationUser.AsNoTracking().Where(x => x.UserId == userId && x.IsRead == false).Count();
            return JsonConvert.SerializeObject(data);
        }

        [HttpGet]
        public object GetNotification(string userId, int skip)
        {
            if (skip != 0)
            {
                var data = _context.NotificationApplicationUser.AsNoTracking().Where(x => x.UserId == userId).OrderByDescending(x => x.Date).Skip(skip).Take(6).ToList().OrderByDescending(x => x.Date);
                return JsonConvert.SerializeObject(data);
            }
            else
            {
                var data = _context.NotificationApplicationUser.AsNoTracking().Where(x => x.UserId == userId).OrderByDescending(x => x.Date).Take(6).ToList().OrderByDescending(x => x.Date);
                return JsonConvert.SerializeObject(data);
            }
        }

        [HttpGet]
        public string ClearSession()
        {
            SetNotif _notif = new SetNotif(HttpContext);
            _notif.ClearSession();
            if (_notif.GetAlertStatus() == null)
            {
                return "Empty";
            }
            else
            {
                return "Cleared";
            }
        }

        private string SetStyleDate(DateTime Date)
        {
            var result = "";
            int DateCount = (Date - DateTime.Now).Days;

            if (DateCount <= 13) result = "danger";
            else if (DateCount > 13 && DateCount <= 29) result = "warning";
            else result = "success";

            return result;
        }

        private string SetStyle(string status)
        {
            var result = "primary";
            if (status.Contains("Send Back") || status == "Rejected") result = "danger";
            else if (status.Contains("Waiting Approval")) result = "warning";
            else if (status.Contains("Closed")) result = "success";

            return result;
        }

        private string SetText(string userRoleName, string roles)
        {
            var result = "View";

            if (userRoleName == roles)
            {
                if (userRoleName == "Distributor")
                {
                    result = "Edit";
                }
                else
                {
                    result = "Proses";
                }
            }

            return result;
        }

    }

}
