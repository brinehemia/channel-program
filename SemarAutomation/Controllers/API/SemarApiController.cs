﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SemarAutomation.Controllers.Helpers;
using SemarAutomation.Data;
using SemarAutomation.Models.ViewModels;
using System.Data.Entity;

namespace SemarAutomation.Controllers.API
{
    [ApiKey]
    [Route("api/")]
    [ApiController]
    //[Authorize(AuthenticationSchemes = "Bearer")]
    public class SemarApiController : ControllerBase
    {
        private ApplicationDbContext _context;

        public SemarApiController (
            ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet("Claims/{id?}")]
        public async Task<IActionResult> Claims(string? id, string? year, string? quarter)
        {
            var result = new List<ResponseClaims>();
            var isQuarter = false;
            var intYear = 0;
            
            var startDate = DateTime.Now;
            var endDate = DateTime.Now;

            if (!string.IsNullOrEmpty(quarter) && string.IsNullOrEmpty(year))
            {
                return StatusCode(500, ResponseHelper.Error(500, "Year Parameter Required"));
            }

            if (!string.IsNullOrEmpty(year))
            {
                try { intYear = Convert.ToInt32(year); } 
                catch
                {
                    return StatusCode(500, ResponseHelper.Error(500, "Invalid Year Parameter"));
                }
                
                if (!string.IsNullOrEmpty(quarter))
                {
                    isQuarter = true;
                    if (quarter == "1" || quarter == "Q1")
                    {
                        startDate = new DateTime(intYear, 01, 01);
                        endDate = new DateTime(intYear, 03, DateTime.DaysInMonth(intYear, 03));

                    } else if (quarter == "2" || quarter == "Q2") 
                    {
                        startDate = new DateTime(intYear, 04, 01);
                        endDate = new DateTime(intYear, 06, DateTime.DaysInMonth(intYear, 06));

                    } else if (quarter == "3" || quarter == "Q3")
                    {
                        startDate = new DateTime(intYear, 07, 01);
                        endDate = new DateTime(intYear, 09, DateTime.DaysInMonth(intYear, 09));

                    } else if (quarter == "4" || quarter == "Q4")
                    {
                        startDate = new DateTime(intYear, 10, 01);
                        endDate = new DateTime(intYear, 12, DateTime.DaysInMonth(intYear, 12));

                    } else
                    {
                        return StatusCode(500, ResponseHelper.Error(500, "Invalid Quarter Parameter"));
                    }
                }
            }

            var claims = new List<ClaimCounter>() { };
            if (!string.IsNullOrEmpty(id))
            {
                claims = (from a in _context.ApprovalTask
                    where a.Title == "SFM Claim" && a.Status == "Closed"
                    join b in _context.Proposal on a.EntitlementId equals b.Id
                    where b.AccountNumber == id
                    select new ClaimCounter
                    {
                        ApprovalId = a.Id,
                        TipeAktivitas = b.Kategori,
                        ClaimDate = a.CreatedAt
                    }).AsNoTracking().ToList();
            } else
            {
                claims = (from a in _context.ApprovalTask
                    where a.Title == "SFM Claim" && a.Status == "Closed"
                    join b in _context.Proposal on a.EntitlementId equals b.Id
                    select new ClaimCounter
                    {
                        ApprovalId = a.Id,
                        TipeAktivitas = b.Kategori,
                        ClaimDate = a.CreatedAt
                    }).AsNoTracking().ToList();
            }

            if (intYear != 0)
            {
                if (isQuarter) claims = claims.Where(a => a.ClaimDate >= startDate && a.ClaimDate <= endDate).ToList();
                else claims = claims.Where(a => a.ClaimDate.Year == intYear).ToList();
            }

            var proposalTipe = _context.SFMProposalType.Select(a => a.Name).ToList();
            if (proposalTipe.Count != 0)
            {
                foreach(var item in proposalTipe)
                {
                    var claimsintype = claims.Where(a => a.TipeAktivitas == item).ToList();
                    var count = claimsintype.Count();

                    if (count == 0) continue;

                    var insert = new ResponseClaims()
                    {
                        TipeAktivitas = item,
                        Total = count,
                    };

                    result.Add(insert);
                }
            }
            return Ok(ResponseHelper.Ok(result));
        }
    }
}
