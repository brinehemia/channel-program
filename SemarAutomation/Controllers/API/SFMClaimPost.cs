﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SemarAutomation.Controllers.Services;
using SemarAutomation.Data;
using SemarAutomation.Models;
using SemarAutomation.Models.ViewModels;
using System.Globalization;

namespace SemarAutomation.Controllers.API
{
    [Route("Api/[controller]/[action]")]
    public class SFMClaimPost : Controller
    {
        private ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        protected readonly LogHelper _log;
        protected readonly IEmailSender _emailSender;
        private readonly IStepApprovalController _stepApproval;

        public SFMClaimPost(
            ApplicationDbContext context,
            UserManager<ApplicationUser> userManager,
            IEmailSender emailSender,
            IStepApprovalController stepApproval)
        {
            _context = context;
            _userManager = userManager;
            _log = new LogHelper(_context);
            _emailSender = emailSender;
            _stepApproval = stepApproval;
        }

        [HttpPost]
        public async Task<string> CommentPost(string Comment, string Id)
        {
            SetNotif _notif = new SetNotif(HttpContext);
            // Find User
            var user = _context.Users.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            var approvalTask = _context.ApprovalTask.Where(x => (x.Title == "SFM Proposal" || x.Title == "SFM Claim") && x.Id == Id).FirstOrDefault();

            var CommentPost = new ApprovalComment()
            {
                ApprovalTaskId = approvalTask.Id,
                UserId = user.Id,
                Comment = Comment,
            };

            _context.ApprovalComment.Add(CommentPost);
            int result = _context.SaveChanges();
            if (result == 0)
            {
                _notif.SetFailed("Gagal", "Gagal menambah komentar");
            }
            else if (result == 1)
            {
                _notif.SetSucced("Sukses", "Berhasil menambah komentar");
            }

            if (approvalTask.Title == "SFM Proposal") return "Home/FormProposal/" + approvalTask.ClaimNumber + "?AfterComment=true";
            else return "Home/FormClaim/" + approvalTask.ClaimNumber + "?AfterComment=true";
        }

        [HttpPost]
        public async Task<string> ComfirmDocument(string approvalId)
        {
            SetNotif _notif = new SetNotif(HttpContext);
            var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            var approvalTask = _context.ApprovalTask.FirstOrDefault(a => a.Id == approvalId);
            var approvalJsonData = JsonConvert.DeserializeObject<SFMProposalDetail>(approvalTask.Data);
            string oldData = _stepApproval.setSerialize(approvalJsonData);

            if (user.RoleName == "ASM") { approvalJsonData.DocumentReceivedASM = true; approvalJsonData.DocumentReceivedDateASM = DateTime.UtcNow.AddHours(7); }
            if (user.RoleName == "Marketing") { approvalJsonData.DocumentReceivedMarketing = true; approvalJsonData.DocumentReceivedDateMarketing = DateTime.UtcNow.AddHours(7); }
            if (user.RoleName == "Legal") { approvalJsonData.DocumentReceivedLegal = true; approvalJsonData.DocumentReceivedDateLegal = DateTime.UtcNow.AddHours(7); }

            approvalTask.Data = JsonConvert.SerializeObject(approvalJsonData);
            _context.Update(approvalTask);
            var result = await _context.SaveChangesAsync();
            if (result > 0)
            {
                string newData = _stepApproval.setSerialize(approvalTask);
                _log.SetLogApproval(approvalTask, user, "Konfirmasi telah menerima Dokumen - " + user.RoleName, newData, oldData);
                _notif.SetSucced("Success", "Berhasil mengonfirmasi pemeriksaan dokumen");
            }
            else _notif.SetFailed("Fail", "Gagal mengonfirmasi pemeriksaan dokumen");

            return result > 0 ? "Done" : "Fail";
        }

        [HttpPost]
        public async Task<string> ComfirmDocumentClaim(string approvalId)
        {
            SetNotif _notif = new SetNotif(HttpContext);
            var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            var approvalTask = _context.ApprovalTask.FirstOrDefault(a => a.Id == approvalId);
            var approvalJsonData = JsonConvert.DeserializeObject<SFMClaimDTO>(approvalTask.Data);
            string oldData = _stepApproval.setSerialize(approvalJsonData);

            if (user.RoleName == "ASM") { approvalJsonData.DocumentReceivedASM = true; approvalJsonData.DocumentReceivedDateASM = DateTime.UtcNow.AddHours(7); }
            if (user.RoleName == "Marketing") { approvalJsonData.DocumentReceivedMarketing = true; approvalJsonData.DocumentReceivedDateMarketing = DateTime.UtcNow.AddHours(7); }
            if (user.RoleName == "Channel Lead") { approvalJsonData.DocumentReceivedChannelLead = true; approvalJsonData.DocumentReceivedDateChannelLead = DateTime.UtcNow.AddHours(7); }
            if (user.RoleName == "Finance") { approvalJsonData.DocumentReceivedFinance = true; approvalJsonData.DocumentReceivedDateFinance = DateTime.UtcNow.AddHours(7); }
            if (user.RoleName == "Legal") { approvalJsonData.DocumentReceivedLegal = true; approvalJsonData.DocumentReceivedDateLegal = DateTime.UtcNow.AddHours(7); }

            approvalTask.Data = JsonConvert.SerializeObject(approvalJsonData);
            _context.Update(approvalTask);
            var result = await _context.SaveChangesAsync();
            if (result > 0)
            {
                string newData = _stepApproval.setSerialize(approvalTask);
                _log.SetLogApproval(approvalTask, user, "Konfirmasi telah menerima Dokumen - " + user.RoleName, newData, oldData);
                //_notif.SetSucced("Success", "Berhasil mengonfirmasi pemeriksaan dokumen");
            }
            //else _notif.SetFailed("Fail", "Gagal mengonfirmasi pemeriksaan dokumen");

            return result > 0 ? "Done" : "Fail";
        }

        [HttpPost]
        public async Task<string> DeleteDocument(int proposalNumber, int fileId)
        {
            SetNotif _notif = new SetNotif(HttpContext);
            var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            var Proposal = _context.Proposal.FirstOrDefault(a => a.ProposalNumber == proposalNumber);
            if (Proposal != null)
            {
                var Files = JsonConvert.DeserializeObject<List<ProposalFile>>(Proposal.UploadedFile);
                Files.Remove(Files.FirstOrDefault(a => a.Id == fileId));
                Proposal.UploadedFile = JsonConvert.SerializeObject(Files);

                _context.Update(Proposal);
                _context.SaveChanges();
            }
            else return "Fail";
            return "Done";
        }

        [HttpPost]
        public async Task<string> DeleteDocumentClaim(string approvalId, int fileId)
        {
            SetNotif _notif = new SetNotif(HttpContext);
            var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            var approval = _context.ApprovalTask.FirstOrDefault(a => a.Id == approvalId);
            if (approval != null)
            {
                var JsonData = JsonConvert.DeserializeObject<SFMClaimDTO>(approval.Data);
                if (JsonData.UploadedFile.Count != 0) JsonData.UploadedFile.Remove(JsonData.UploadedFile.FirstOrDefault(a => a.Id == fileId));
                approval.Data = JsonConvert.SerializeObject(JsonData);

                _context.ApprovalTask.Update(approval);
                _context.SaveChanges();
            }
            else return "Fail";
            return "Done";
        }

        [HttpPost]
        public async Task<string> CancelProposal(int proposalNumber)
        {
            SetNotif _notif = new SetNotif(HttpContext);
            var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            var proposal = _context.Proposal.FirstOrDefault(a => a.ProposalNumber == proposalNumber);
            if (proposal != null)
            {
                proposal.Status = "Canceled";
                _context.Update(proposal);
                _context.SaveChanges();

                var approvalTask = _context.ApprovalTask.FirstOrDefault(a => a.ClaimNumber == proposalNumber);
                var approvalJsonData = JsonConvert.DeserializeObject<SFMProposalDetail>(approvalTask.Data);
                var sendbackEntitlement = approvalJsonData.DetailBalanceTaken;

                foreach (var item in approvalJsonData.DetailBalanceTaken)
                {
                    var entitlement = _context.SFMEntitlement.FirstOrDefault(a => a.Id == item.EntitlementId);
                    if (entitlement == null) continue;
                    entitlement.EntitlementBalance = entitlement.EntitlementBalance + item.Nominal;

                    _context.Update(entitlement);
                    _context.SaveChanges();
                }

                _notif.SetSucced("Sukses", "Berhasil membatalkan proposal Anda");
            }
            else return "Fail";
            return "Done";
        }

        [HttpPost]
        public async Task<string> CancelClaim(int claimNumber)
        {
            SetNotif _notif = new SetNotif(HttpContext);
            var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            var approvalTask = _context.ApprovalTask.FirstOrDefault(a => a.Title == "SFM Claim" && a.ClaimNumber == claimNumber);
            var JsonData = JsonConvert.DeserializeObject<SFMClaimDTO>(approvalTask.Data);

            var proposal = _context.Proposal.FirstOrDefault(a => a.Id == approvalTask.EntitlementId);
            if (proposal != null)
            {
                proposal.NominalBalance = proposal.NominalBalance + JsonData.NominalClaim;
                _context.Proposal.Update(proposal);
                var result = _context.SaveChanges();
                if (result != 0)
                {
                    approvalTask.Status = "Canceled";
                    _context.ApprovalTask.Update(approvalTask);
                    _context.SaveChanges();
                    _notif.SetSucced("Sukses", "Berhasil membatalkan claim Anda");
                }
                else return "Fail";
            }
            return "Done";
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public object EditEntitlementPost([FromBody] EditEntitlementViewModel data)
        {
            SetNotif _notif = new SetNotif(HttpContext);
            ResultAjax result = new ResultAjax();
            var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            var entitlement = _context.SFMEntitlement.FirstOrDefault(x => x.Id == data.Id);

            if (entitlement.EntitlementValue == data.Entitlement && entitlement.ExpiredDate.Date == data.ExpiredDate.Date)
            {
                result.Status = "Peringatan";
                //result.Content = "Your input and entitlement data are same. No update required";
                result.Content = "Inputan yang Anda masukkan dan Data Entitlement Sama/Sudah Ada. Gagal untuk mengupdate";
                return JsonConvert.SerializeObject(result);
            }

            var oldEntitlement = entitlement;
            if (entitlement.ExpiredDate != data.ExpiredDate) entitlement.IsRejected = false;

            entitlement.EntitlementValue = data.Entitlement;
            entitlement.EntitlementBalance = data.Balance;
            entitlement.ExpiredDate = data.ExpiredDate;
            _context.SFMEntitlement.Update(entitlement);
            if (_context.SaveChanges() > 0)
            {
                var newData = JsonConvert.SerializeObject(entitlement);
                var oldData = JsonConvert.SerializeObject(oldEntitlement);

                _log.SetLogChanges(user, "Change-EditEntitlement", newData, oldData);

                _notif.SetSucced("Sukses", "Sukses mengupdate Entitlement");
                result.Status = "Sukses";
                result.Content = "Sukses mengupdate Entitlement";
                return JsonConvert.SerializeObject(result);
            }
            else
            {
                _notif.SetSucced("Gagal", "Gagal mengupdate Entitlement, Mohon untuk mengulangi");

                result.Status = "Gagal";
                result.Content = "Gagal mengupdate entitlement, Mohon untuk mengulangi";
                return JsonConvert.SerializeObject(result);
            }
        }

        public async Task<string> UpdateBillingDokumen(UpdateCloseClaim data)
        {
            var result = string.Empty;
            var approvalTask = _context.ApprovalTask.Where(a => a.Title == "SFM Claim" && a.ClaimNumber == data.claimNumber).FirstOrDefault();
            if (approvalTask != null)
            {
                var JsonDataDetail = JsonConvert.DeserializeObject<SFMClaimDTO>(approvalTask.Data);

                if (!string.IsNullOrEmpty(data.billingdokumen)) JsonDataDetail.BillingDocNumber = data.billingdokumen;
                if (!string.IsNullOrEmpty(data.prnumber)) JsonDataDetail.PPRNumber = data.prnumber;

                var datebilling = DateTime.Now;
                DateTime.TryParseExact(data.billingdate, "dd.MM.yyyy", new CultureInfo("id-ID"), DateTimeStyles.None, out datebilling);
                JsonDataDetail.BillingDate = datebilling;

                approvalTask.Data = JsonConvert.SerializeObject(JsonDataDetail);

                _context.ApprovalTask.Update(approvalTask);
                _context.SaveChanges();

                result = "Sukses";
            }

            return result;
        }

        [HttpPost]
        public object ReadNotification([FromBody] NotificationApplicationUser _notif)
        {
            ResultAjax result = new ResultAjax();
            var notif = _context.NotificationApplicationUser.Where(x => x.Id == _notif.Id).FirstOrDefault();
            notif.IsRead = true;
            _context.NotificationApplicationUser.Update(notif);
            if (_context.SaveChanges() > 0)
            {
                result.Status = "Success";
                result.Content = notif.Url;
            }
            else
            {
                result.Status = "Fail";
                result.Content = "Fail. Please retry";
            }
            return JsonConvert.SerializeObject(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public object ReadNotificationAll([FromBody] NotificationApplicationUser _notif)
        {
            ResultAjax result = new ResultAjax();
            var notif = _context.NotificationApplicationUser.Where(x => x.UserId == _notif.UserId).ToList();
            foreach (var item in notif)
            {
                item.IsRead = true;
            }
            _context.NotificationApplicationUser.UpdateRange(notif);
            if (_context.SaveChanges() > 0)
            {
                result.Status = "Success";
                result.Content = "Success";
            }
            else
            {
                result.Status = "Fail";
                result.Content = "Fail. Please retry";
            }
            return JsonConvert.SerializeObject(result);
        }


    }
}
