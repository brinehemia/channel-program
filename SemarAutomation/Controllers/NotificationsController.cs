﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SemarAutomation.Data;

namespace SemarAutomation.Controllers
{
    public class NotificationsController : Controller
    {
        protected readonly ApplicationDbContext _context;
        public NotificationsController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var user = _context.Users.AsNoTracking().Where(x => x.Email == User.Identity.Name).FirstOrDefault();
            var notif = _context.NotificationApplicationUser.AsNoTracking().Where(x => x.UserId == user.Id).OrderByDescending(x => x.Date).Take(10).ToList();
            ViewBag.user = user;
            return View(notif);
        }

        [HttpGet]
        public object GetNotificationSkip(string userId, int skip)
        {
            var notif = _context.NotificationApplicationUser.AsNoTracking().Where(x => x.UserId == userId).OrderByDescending(x => x.Date).Skip(skip).Take(10).ToList();
            return JsonConvert.SerializeObject(notif);
        }


    }
}
