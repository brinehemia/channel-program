﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using OfficeOpenXml;
using SemarAutomation.Controllers.Services;
using SemarAutomation.Data;
using SemarAutomation.Hubs;
using SemarAutomation.Models;
using SemarAutomation.Models.ViewModels;
using System.Globalization;
using System.Security.Claims;
using System.Text.RegularExpressions;

namespace SemarAutomation.Controllers
{
    public class HomeController : Controller
    {
        private readonly IPostResponseService _postResponse;
        private readonly IGetResponseService _getResponse;
        private readonly IStepApprovalController _stepApproval;
        private readonly SetNotif _notif;

        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly LogHelper _log;

        private SendNotification _sendNotif;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly IEmailSender _emailSender;

        public HomeController(ApplicationDbContext context, SignInManager<ApplicationUser> signInManager,
            IWebHostEnvironment hostingEnvironment,
            RoleManager<IdentityRole> roleManager,
            UserManager<ApplicationUser> userManager,
            IHubContext<NotificationHub> notificationUserHubContext,
            IUserConnectionManager userConnectionManager,
            IEmailSender emailSender,
            IPostResponseService postResponse,
            IGetResponseService getResponse, IHttpContextAccessor HttpContext,
            IStepApprovalController stepApproval)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _userManager = userManager;
            _log = new LogHelper(_context);
            _sendNotif = new SendNotification(_context, notificationUserHubContext, userConnectionManager, getResponse);
            _emailSender = emailSender;

            _postResponse = postResponse;
            _getResponse = getResponse;
            _notif = new SetNotif(HttpContext.HttpContext);
            _stepApproval = stepApproval;

        }

        public async Task<IActionResult> Auth(string token)
        {
            try
            {
                var responseContent = new ReponseDetailUser() { };

                var requestUser = await _getResponse.GetResponseWithAuth("detail", token);
                if ((int)requestUser.StatusCode == 200) responseContent = JsonConvert.DeserializeObject<ReponseDetailUser>(requestUser.Content);
                else throw new Exception("User SFM Not Found");

                var role = responseContent.user.role;
                if (responseContent.user.role.ToLower() == "om") role = "Distributor";
                else if (responseContent.user.role.ToLower() == "sales") role = "ASM";
                else if (responseContent.user.role.ToLower() == "channel-marketing") role = "Marketing";
                else if (responseContent.user.role.ToLower() == "legal") role = "Legal";
                else if (responseContent.user.role.ToLower() == "commercial-lead") role = "Channel Lead";
                else if (responseContent.user.role.ToLower() == "finance") role = "Finance";
                else if (responseContent.user.role.ToLower() == "editor") role = "Guest";
                else if (responseContent.user.role.ToLower() == "product-manager") role = "Guest";

                var user = await _userManager.FindByNameAsync(responseContent.user.email);
                if (user == null)
                {
                    var insert = new ApplicationUser
                    {
                        UserName = responseContent.user.email,
                        Email = responseContent.user.email,
                        Name = responseContent.user.name,
                        Channel = "Prof",
                        PhoneNumber = responseContent.user.phone_number,
                        RoleName = role,
                        AccountDistributor = role == "Distributor" ? responseContent.user.sold_to.FirstOrDefault() : null,
                        IsSFMUser = true,
                        CompanyDistributor = responseContent.user.distributor_company,
                        UpdatedAt = DateTime.UtcNow.AddHours(7),
                        CreatedAt = DateTime.UtcNow.AddHours(7)
                    };

                    await _userManager.CreateAsync(insert, "f6036D6!80caf44d");
                    user = await _userManager.FindByNameAsync(responseContent.user.email);
                }
                else
                {
                    var isUpdate = false;
                    if (user.CompanyDistributor != responseContent.user.distributor_company) { user.CompanyDistributor = responseContent.user.distributor_company; isUpdate = true; }
                    if (user.Name != responseContent.user.name) { user.Name = responseContent.user.name; isUpdate = true; }
                    if (user.PhoneNumber != responseContent.user.phone_number) { user.PhoneNumber = responseContent.user.phone_number; isUpdate = true; }

                    if (isUpdate) await _userManager.UpdateAsync(user);
                }

                var userClaim = await _userManager.GetClaimsAsync(user);
                if (userClaim.Count > 0)
                {
                    var DataUserClaim = _context.UserClaims.Where(a => a.UserId == user.Id);
                    _context.UserClaims.RemoveRange(DataUserClaim);
                    _context.SaveChanges();
                }

                var claims = new List<Claim>
                {
                    new Claim("email", responseContent.user.email),
                    new Claim("token", token),
                    new Claim("username", user.Name),
                    new Claim(ClaimTypes.Role, role)
                };

                await _userManager.AddClaimsAsync(user, claims);

                var result = await _signInManager.PasswordSignInAsync(user.UserName, "f6036D6!80caf44d", false, lockoutOnFailure: false);

                //if (result.Succeeded)
                //{
                //    //var userGetClaim = await _userManager.FindByNameAsync(user.Name);
                //    await _signInManager.RefreshSignInAsync(user);
                //}

                //var appIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                //await _signInManager.SignInWithClaimsAsync(user, true, claims) ;

                //await HttpContext.SignInAsync(
                //CookieAuthenticationDefaults.AuthenticationScheme,
                //new ClaimsPrincipal(appIdentity), new AuthenticationProperties { IsPersistent = true, ExpiresUtc = DateTime.UtcNow.AddDays(1) });

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public async Task FirstGenerate()
        {
            var legal = new ApplicationUser
            {
                UserName = "legalsfm@getnada.com",
                Email = "legalsfm@getnada.com",
                Name = "Legal Testing User",
                Channel = "Prof",
                PhoneNumber = "0875657454",
                RoleName = "Legal",
                AccountDistributor = null,
                IsSFMUser = true,
                UpdatedAt = DateTime.UtcNow.AddHours(7),
                CreatedAt = DateTime.UtcNow.AddHours(7)
            };
            await _userManager.CreateAsync(legal, "f6036D6!80caf44d");

            var marketing = new ApplicationUser
            {
                UserName = "marketingsfm@getnada.com",
                Email = "marketingsfm@getnada.com",
                Name = "Marketing Testing User",
                Channel = "Prof",
                PhoneNumber = "0875657454",
                RoleName = "Marketing",
                AccountDistributor = null,
                IsSFMUser = true,
                UpdatedAt = DateTime.UtcNow.AddHours(7),
                CreatedAt = DateTime.UtcNow.AddHours(7)
            };
            await _userManager.CreateAsync(marketing, "f6036D6!80caf44d");

            var channelLead = new ApplicationUser
            {
                UserName = "channelleadsfm@getnada.com",
                Email = "channelleadsfm@getnada.com",
                Name = "Channel Lead Testing User",
                Channel = "Prof",
                PhoneNumber = "0875657454",
                RoleName = "Channel Lead",
                AccountDistributor = null,
                IsSFMUser = true,
                UpdatedAt = DateTime.UtcNow.AddHours(7),
                CreatedAt = DateTime.UtcNow.AddHours(7)
            };
            await _userManager.CreateAsync(channelLead, "f6036D6!80caf44d");

            var finance = new ApplicationUser
            {
                UserName = "financesfm@getnada.com",
                Email = "financesfm@getnada.com",
                Name = "Finance Testing User",
                Channel = "Prof",
                PhoneNumber = "0875657454",
                RoleName = "Finance",
                AccountDistributor = null,
                IsSFMUser = true,
                UpdatedAt = DateTime.UtcNow.AddHours(7),
                CreatedAt = DateTime.UtcNow.AddHours(7)
            };
            await _userManager.CreateAsync(finance, "f6036D6!80caf44d");
        }

        [Authorize]
        public IActionResult Index()
        {
            var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            ViewBag.user = user;

            var proposal = _context.Proposal.ToList();
            var SFMEntitlement = _context.SFMEntitlement.ToList();
            var approvalTaskClaim = _context.ApprovalTask.Where(a => a.Title == "SFM Claim").ToList();
            var approvalTaskProposal = _context.ApprovalTask.Where(a => a.Title == "SFM Proposal").ToList();

            if (user.RoleName == "Distributor")
            {
                proposal = proposal.Where(a => a.AccountNumber == user.AccountDistributor).ToList();
                SFMEntitlement = SFMEntitlement.Where(a => a.Account == user.AccountDistributor).ToList();
                approvalTaskClaim = approvalTaskClaim.Where(a => a.SubmittedById == user.Id).ToList();
                approvalTaskProposal = approvalTaskProposal.Where(a => a.SubmittedById == user.Id).ToList();
            }
            else if (user.RoleName == "ASM")
            {
                var response = new List<string>() { };

                var token = HttpContext.User.Claims.First(a => a.Type == "token").Value;
                var request = _getResponse.GetResponseWithAuth("soldto", token).Result;
                if ((int)request.StatusCode == 200) response = JsonConvert.DeserializeObject<SFMListResponse>(request.Content).data;

                proposal = proposal.Where(a => response.Any(c => c == a.AccountNumber)).ToList();
                SFMEntitlement = SFMEntitlement.Where(a => response.Any(c => c == a.Account)).ToList();
                approvalTaskClaim = approvalTaskClaim.Where(a => proposal.Any(x => x.Id == a.EntitlementId)).ToList();
                approvalTaskProposal = approvalTaskProposal.Where(a => proposal.Any(x => x.ProposalNumber == a.ClaimNumber)).ToList();
            }

            var vm = new DashBoardSFMViewModel() { };

            long resultOnprocess = 0;
            foreach (var item in approvalTaskClaim.Where(a => a.Status != "Close" && a.Status != "Closed" && a.Status != "Rejected" && a.Status != "Canceled"))
            {
                var JsonData = JsonConvert.DeserializeObject<SFMClaimDTO>(item.Data);
                resultOnprocess = resultOnprocess + JsonData.NominalClaim;
            }

            long resultClosed = 0;
            foreach (var item in approvalTaskClaim.Where(a => a.Status == "Close" || a.Status == "Closed"))
            {
                var JsonData = JsonConvert.DeserializeObject<SFMClaimDTO>(item.Data);
                resultClosed = resultClosed + JsonData.NominalClaim;
            }

            vm.OnprocessClaim = "Rp " + resultOnprocess.ToString("N0");
            vm.OnprocessClaimCount = approvalTaskClaim.Where(a => a.Status != "Close" && a.Status != "Closed" && a.Status != "Rejected" && a.Status != "Canceled").Count();

            vm.OnprocessProposal = "Rp " + proposal.Where(a => a.Status != "Canceled" && a.Status != "Draft" && a.Status != "Closed").Sum(a => a.Nominal).ToString("N0");
            vm.OnprocessProposalCount = proposal.Where(a => a.Status != "Canceled" && a.Status != "Draft" && a.Status != "Closed").Count();

            vm.CloseClaim = "Rp " + resultClosed.ToString("N0");
            vm.CloseClaimCount = approvalTaskClaim.Where(a => a.Status == "Close" || a.Status == "Closed").Count();

            vm.CloseProposal = "Rp " + proposal.Where(a => a.Status == "Closed").Sum(a => a.Nominal).ToString("N0");
            vm.CloseProposalCount = proposal.Where(a => a.Status == "Closed").Count();

            vm.CancelProposalCount = proposal.Where(a => a.Status == "Canceled").Count();

            var List = SFMEntitlement.Where(c => (c.ExpiredDate != null || c.ExpiredDate != DateTime.MinValue)).Select(x => new { Date = (x.ExpiredDate - DateTime.UtcNow.AddHours(7)).Days, EntitlementBalance = x.EntitlementBalance }).ToList();
            vm.ExpDateEntitlement = "Rp " + List.Where(a => a.Date >= 0 && a.Date <= 60).Sum(c => c.EntitlementBalance).ToString("N0");
            vm.ExpDateEntitlementCount = List.Where(x => x.Date >= 0 && x.Date <= 60 && x.EntitlementBalance != 0).Count();

            return View(vm);
        }

        [Authorize(Policy = "MarketingFinance")]
        public IActionResult Program()
        {
            return View();
        }

        [Authorize(Policy = "MarketingFinance")]
        public IActionResult FormProgram(int Id)
        {
            var result = new EditProgram()
            {
                RangeStartDate = DateTime.Today,
            };

            if (Id != 0)
            {
                var item = _context.SFMProgram.FirstOrDefault(a => a.Id == Id);
                var IdDistributor = _context.ProgramSFMDistributor.Where(a => a.DataProgramId == Id).Select(a => a.UserDistributorNumber).ToList();
                result = new EditProgram()
                {
                    Id = item.Id,
                    ProgramName = item.ProgramName,
                    PPR = item.PPR,
                    RangeStartDate = item.StartDate,
                    ExpiredDate = item.DefaultExpDate,
                    IdDistributor = IdDistributor != null && IdDistributor.Count() != 0 ? string.Join(",", IdDistributor) : null,
                };
            }

            ViewBag.ProgramInput = result;
            return View();
        }

        [Authorize]
        public IActionResult Entitlement(string Id)
        {
            var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            ViewBag.user = user;

            if (!string.IsNullOrEmpty(Id)) ViewData["dashboardView"] = Id;
            else ViewData["dashboardView"] = string.Empty;

            return View();
        }

        [Authorize]
        public IActionResult Proposal(string Id)
        {
            var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            ViewBag.user = user;

            if (!string.IsNullOrEmpty(Id)) ViewData["dashboardView"] = Id;
            else ViewData["dashboardView"] = string.Empty;

            return View();
        }

        [Authorize]
        public IActionResult Claim(string Id)
        {
            var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            ViewBag.user = user;

            if (!string.IsNullOrEmpty(Id)) ViewData["dashboardView"] = Id;
            else ViewData["dashboardView"] = string.Empty;

            return View();
        }

        [Authorize]
        public IActionResult ApprovedProposal()
        {
            var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            ViewBag.user = user;
            return View();
        }

        [Authorize]
        public IActionResult FormProposal(int Id, int loadcomment = 0, string AfterComment = "")
        {
            var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            if (user.RoleName != "Distributor" && Id == 0)
            {
                var accessdenied = new PathString("/Identity/Account/AccessDenied");
                return Redirect(accessdenied);
            }

            SFMFormClaimViewModel vm = new SFMFormClaimViewModel()
            {
                ApprovalTask = new ApprovalTask() { },
                CurrentStep = new StepBase() { },
                ApprovalComment = new List<ApprovalCommentVM>(),
                ClaimNumber = 0,
                BalanceTemp = 0,
                ValueSumberBudget = new int[] { },
                ValueKPI = new List<ProposalKPI>() { },
                SFMClaimDTO = new SFMProposalDetail(),
                Proposal = new Proposal() { },
                UploadedFile = new List<ListProposalFile>(),
                ProposalFile = new List<ProposalFile>(),
                OpportunityList = new List<OpportunityData>() { },
                User = user,
                DraftId = 0,
                SegmenList = new List<string>() { },
                SubSegmenList = new List<string>() { },
                TypeFileData = new List<TypeFileData>() { },
                DescriptionFile = new DescriptionFile { }
            };

            if (user.RoleName == "Distributor" && Id == 0)
            {
                var Draft = _context.Proposal.Where(a => a.Status == "Draft" && a.AccountNumber == user.AccountDistributor).FirstOrDefault();
                if (Draft == null)
                {
                    var InsertDraft = new Proposal()
                    {
                        AccountNumber = user.AccountDistributor,
                        Status = "Draft"
                    };

                    _context.Proposal.Add(InsertDraft);
                    _context.SaveChanges();

                    vm.DraftId = InsertDraft.Id;
                }
                else vm.DraftId = Draft.Id;
            }

            if (Id != 0)
            {
                vm.ApprovalTask = _context.ApprovalTask.FirstOrDefault(a => a.ClaimNumber == Id);
                vm.CurrentStep = _context.StepBase.Where(a => a.Id == vm.ApprovalTask.CurrentStepId).FirstOrDefault();
                vm.ClaimNumber = Convert.ToInt32(vm.ApprovalTask.ClaimNumber);
                vm.Proposal = _context.Proposal.FirstOrDefault(a => a.ProposalNumber == Id);

                if (vm.Proposal != null)
                {
                    var responseDetailOpportunity = _getResponse.GetResponse("getoppor_by_sfdc?sfdc_id=" + vm.Proposal.Opportunity).Result;
                    if ((int)responseDetailOpportunity.StatusCode == 200)
                    {
                        var JsonOpportunity = JsonConvert.DeserializeObject<OpportunityDetailResponse>(responseDetailOpportunity.Content).data.FirstOrDefault();
                        if (JsonOpportunity != null) vm.OpportunityValue = vm.Proposal.Opportunity + " - " + JsonOpportunity.name;
                    }

                    if (user.RoleName == "Distributor")
                    {
                        var responseSegmen = _getResponse.GetResponse("list_segment").Result;
                        var resultSegmen = JsonConvert.DeserializeObject<SFMListResponse>(responseSegmen.Content);

                        foreach (var item in resultSegmen.data)
                        {
                            var itemresult = char.ToUpper(item[0]) + item.Substring(1);
                            vm.SegmenList.Add(itemresult);
                        }

                        if (!string.IsNullOrEmpty(vm.Proposal.SegmentPasar))
                        {
                            var responseSubSegmen = _getResponse.GetResponse("getsubsegment_by_segment?segment=" + vm.Proposal.SegmentPasar).Result;
                            var resultSubSegmen = JsonConvert.DeserializeObject<SFMListResponse>(responseSubSegmen.Content).data;
                            vm.SubSegmenList = resultSubSegmen;
                        }
                    }

                    if (!string.IsNullOrEmpty(vm.Proposal.Kategori) && !string.IsNullOrEmpty(vm.Proposal.TipeAktivitas))
                    {
                        var getTypeFileData = _context.SFMFileUploadType.Where(a => a.ProposalType == vm.Proposal.Kategori && a.ProposalSubType == vm.Proposal.TipeAktivitas).FirstOrDefault();
                        if (getTypeFileData != null)
                        {
                            vm.TypeFileData = JsonConvert.DeserializeObject<List<TypeFileData>>(getTypeFileData.TypeFileProposal);
                            vm.DescriptionFile = new DescriptionFile { DetailProposal = getTypeFileData.DetailProposal };
                        }
                    }
                }

                vm.ApprovalComment = (from a in _context.ApprovalComment where a.ApprovalTaskId == vm.ApprovalTask.Id
                                   join d in _context.Users on a.UserId equals d.Id
                                   select new ApprovalCommentVM
                                   {
                                       ApprovalTaskId = a.ApprovalTaskId,
                                       Comment = a.Comment,
                                       Id = a.Id,
                                       CreatedAt = a.CreatedAt,
                                       UserId = a.UserId,
                                       Role = d.RoleName,
                                       Name = d.Name
                                   }).ToList();

                vm.SFMClaimDTO = JsonConvert.DeserializeObject<SFMProposalDetail>(vm.ApprovalTask.Data);
                vm.ProposalFile = JsonConvert.DeserializeObject<List<ProposalFile>>(vm.Proposal.UploadedFile);

                if (vm.ProposalFile.Count != 0)
                {
                    var SFMFileUploadType = _context.SFMFileUploadType.Where(a => a.ProposalType == vm.Proposal.Kategori && a.ProposalSubType == vm.Proposal.TipeAktivitas).FirstOrDefault();
                    var dataDefaultFile = JsonConvert.DeserializeObject<List<TypeFileData>>(SFMFileUploadType.TypeFileProposal);

                    var listFieldname = vm.ProposalFile.Select(a => a.Fieldname).Distinct().ToList();
                    foreach (var uplodedfilename in listFieldname)
                    {
                        var FullnameText = dataDefaultFile.Where(a => a.IdName == uplodedfilename).FirstOrDefault();
                        vm.UploadedFile.Add(new ListProposalFile { 
                            FieldName = Regex.Replace(uplodedfilename, "([a-z])([A-Z])", "$1 $2"), 
                            FullName = FullnameText != null ? FullnameText.Filename : "",
                            ProposalFile = vm.ProposalFile.Where(a => a.Fieldname == uplodedfilename).ToList() 
                        });
                    }
                }

                var Distributor = _context.Users.Where(a => a.RoleName == "Distributor" && a.AccountDistributor == vm.Proposal.AccountNumber).FirstOrDefault();
                if (Distributor != null) vm.DistributorName = Distributor.Name;

                vm.ValueSumberBudget = Array.ConvertAll(vm.Proposal.SumberBudget.Split(",").ToArray(), a => int.Parse(a));
                var ValueKPI = string.IsNullOrEmpty(vm.Proposal.KPI) ? new List<string>() { } : JsonConvert.DeserializeObject<List<string>>(vm.Proposal.KPI);
                var kpiCounter = 1;

                foreach (var item in ValueKPI)
                {
                    vm.ValueKPI.Add(new ProposalKPI { Id = "Input-" + kpiCounter, Name = item });
                    kpiCounter++;
                }

                if (user.RoleName == "ASM") vm.isReadDocument = vm.SFMClaimDTO.DocumentReceivedASM;
                else if (user.RoleName == "Marketing") vm.isReadDocument = vm.SFMClaimDTO.DocumentReceivedMarketing;
                else if (user.RoleName == "Legal") vm.isReadDocument = vm.SFMClaimDTO.DocumentReceivedLegal;

                vm.BalanceTemp = _context.SFMEntitlement.Where(a => vm.ValueSumberBudget.Contains(a.Id)).Sum(a => a.EntitlementBalance);

                if (vm.ApprovalTask.Status == "Closed")
                {
                    var claimProposal = _context.ApprovalTask.Where(a => a.Title == "SFM Claim" && a.EntitlementId == vm.Proposal.Id).ToList();
                    if (claimProposal.Count != 0) vm.ActualAmount = claimProposal.Sum(a => JsonConvert.DeserializeObject<SFMClaimDTO>(a.Data).NominalClaim).ToString();
                    else vm.ActualAmount = "0";
                }
            }

            var token = HttpContext.User.Claims.First(x => x.Type == "token").Value;
            var response = _getResponse.GetResponseWithAuth("opportunity", token).Result;
            var result = JsonConvert.DeserializeObject<OpportunityList>(response.Content).data;

            foreach (var item in result)
            {
                item.amountString = "Rp " + item.amount.ToString("N0");
            }

            vm.OpportunityList = result;

            //GetComment
            int CommentLoaded = loadcomment + 4;
            ViewBag.UserLogged = user.Name + " (" + user.RoleName + ")";
            ViewBag.Commentload = CommentLoaded;

            // Set Active tab after submit
            ViewBag.ActiveTab = AfterComment;

            return View(vm);
        }

        [Authorize]
        public IActionResult FormClaim(int Id, int e, int loadcomment = 0, string AfterComment = null)
        {
            var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
            SFMFormClaimViewModel vm = new SFMFormClaimViewModel()
            {
                ApprovalTask = new ApprovalTask() { },
                CurrentStep = new StepBase() { },
                ApprovalComment = new List<ApprovalCommentVM>(),
                ClaimNumber = 0,
                BalanceTemp = 0,
                ValueSumberBudget = new int[] { },
                ValueKPI = new List<ProposalKPI>() { },
                SFMClaimData = new SFMClaimDTO(),
                Proposal = new Proposal() { },
                UploadedFile = new List<ListProposalFile>(),
                TypeFileData = new List<TypeFileData>(),
                User = user,
                DescriptionFile = new DescriptionFile()
            };

            vm.SFMClaimData.Hasil = new List<HasilValue>() { };

            if (Id != 0)
            {
                vm.ApprovalTask = _context.ApprovalTask.Where(a => a.ClaimNumber == Id).FirstOrDefault();
                vm.CurrentStep = _context.StepBase.Where(a => a.Id == vm.ApprovalTask.CurrentStepId).FirstOrDefault();
                vm.ClaimNumber = Convert.ToInt32(vm.ApprovalTask.ClaimNumber);
                vm.Proposal = _context.Proposal.Where(a => a.Id == vm.ApprovalTask.EntitlementId).FirstOrDefault();
                vm.ApprovalComment = (from a in _context.ApprovalComment where a.ApprovalTaskId == vm.ApprovalTask.Id
                                      join d in _context.Users on a.UserId equals d.Id
                                      select new ApprovalCommentVM
                                      {
                                          ApprovalTaskId = a.ApprovalTaskId,
                                          Comment = a.Comment,
                                          Id = a.Id,
                                          CreatedAt = a.CreatedAt,
                                          UserId = a.UserId,
                                          Role = d.RoleName,
                                          Name = d.Name
                                      }).ToList();
                vm.SFMClaimData = JsonConvert.DeserializeObject<SFMClaimDTO>(vm.ApprovalTask.Data);
                //vm.UploadedFile = vm.SFMClaimData.UploadedFile;

                if (vm.SFMClaimData.UploadedFile.Count != 0)
                {
                    var SFMFileUploadType = _context.SFMFileUploadType.Where(a=> a.ProposalType == vm.Proposal.Kategori && a.ProposalSubType == vm.Proposal.TipeAktivitas).FirstOrDefault();
                    var dataDefaultFile = JsonConvert.DeserializeObject<List<TypeFileData>>(SFMFileUploadType.TypeFileClaim);

                    var listFieldname = vm.SFMClaimData.UploadedFile.Select(a => a.Fieldname).Distinct().ToList();
                    foreach (var item in listFieldname)
                    {
                        var FullnameText = dataDefaultFile.Where(a => a.IdName == item).FirstOrDefault();
                        vm.UploadedFile.Add(new ListProposalFile { 
                            FieldName = Regex.Replace(item, "([a-z])([A-Z])", "$1 $2"),
                            FullName = FullnameText != null ? FullnameText.Filename : "",
                            ProposalFile = vm.SFMClaimData.UploadedFile.Where(a => a.Fieldname == item).ToList()
                        });
                    }

                }

                vm.IsRebateBooster = vm.Proposal.TipeAktivitas == "Rebate Booster";

                var ValueKPI = string.IsNullOrEmpty(vm.Proposal.KPI) ? new List<string>() { } : JsonConvert.DeserializeObject<List<string>>(vm.Proposal.KPI);
                var kpiCounter = 1;

                foreach (var item in ValueKPI)
                {
                    vm.ValueKPI.Add(new ProposalKPI { Id = "Input-" + kpiCounter, Order = kpiCounter, Name = item });
                    kpiCounter++;
                }

                if (user.RoleName == "ASM") vm.isReadDocument = vm.SFMClaimData.DocumentReceivedASM;
                else if (user.RoleName == "Marketing") vm.isReadDocument = vm.SFMClaimData.DocumentReceivedMarketing;
                else if (user.RoleName == "Channel Lead") vm.isReadDocument = vm.SFMClaimData.DocumentReceivedChannelLead;
                else if (user.RoleName == "Finance") vm.isReadDocument = vm.SFMClaimData.DocumentReceivedFinance;
                else if (user.RoleName == "Legal") vm.isReadDocument = vm.SFMClaimData.DocumentReceivedLegal;

                vm.BalanceTemp = vm.Proposal.NominalBalance;
            }

            if (e != 0)
            {
                vm.Proposal = _context.Proposal.Where(a => a.ProposalNumber == e).FirstOrDefault();
                vm.IsRebateBooster = vm.Proposal.TipeAktivitas == "Rebate Booster";
                var ValueKPI = string.IsNullOrEmpty(vm.Proposal.KPI) ? new List<string>() { } : JsonConvert.DeserializeObject<List<string>>(vm.Proposal.KPI);
                var kpiCounter = 1;

                foreach (var item in ValueKPI)
                {
                    vm.ValueKPI.Add(new ProposalKPI { Id = "Input-" + kpiCounter, Order = kpiCounter, Name = item });
                    kpiCounter++;
                }

                vm.BalanceTemp = vm.Proposal.NominalBalance;
            }

            var getTypeFileData = _context.SFMFileUploadType.Where(a => a.ProposalType == vm.Proposal.Kategori && a.ProposalSubType == vm.Proposal.TipeAktivitas).FirstOrDefault();
            vm.TypeFileData = JsonConvert.DeserializeObject<List<TypeFileData>>(getTypeFileData.TypeFileClaim);
            vm.DescriptionFile = new DescriptionFile { DetailClaim = getTypeFileData.DetailClaim, DetailProposal = getTypeFileData.DetailProposal };

            //GetComment
            int CommentLoaded = loadcomment + 4;
            ViewBag.UserLogged = user.Name + " (" + user.RoleName + ")";
            ViewBag.Commentload = CommentLoaded;

            // Set Active tab after submit
            ViewBag.ActiveTab = AfterComment;

            return View(vm);
        }

        [Authorize(Policy = "MarketingFinance")]
        public IActionResult UploadEntitlement()
        {
            string baseURL = $"{this.Request.PathBase}/";
            ViewData["template"] = baseURL + "templates/Upload_SFMEntitlement.xlsx";
            return View();
        }

        [Authorize]
        public async Task<IActionResult> SubmitProgram(SFMProgramDTO data)
        {
            try
            {
                var result = 0;
                var isUpdateProgram = false;

                if (!string.IsNullOrEmpty(data.ProgramName)) data.ProgramName = data.ProgramName.Trim();
                if (!string.IsNullOrEmpty(data.PPR)) data.PPR = data.PPR.Trim();

                if (data.Id == 0)
                {
                    var checkName = _context.SFMProgram.Any(a => a.ProgramName == data.ProgramName);
                    if (checkName)
                    {
                        _notif.SetFailed("Gagal", "Nama program ini telah digunakan. Mohon untuk dapat menginputkan nama yang berbeda");
                        return RedirectToAction("FormProgram", "Home");
                    }

                    var program = new SFMProgram()
                    {
                        ProgramName = data.ProgramName,
                        PPR = "BDF",
                        StartDate = data.StartDate,
                        DefaultExpDate = data.DefaultExpDate,
                        CreatedAt = DateTime.UtcNow.AddHours(7),
                        UpdatedAt = DateTime.UtcNow.AddHours(7)
                    };
                    await _context.SFMProgram.AddAsync(program);
                    result = _context.SaveChanges();
                    _notif.SetSucced("Sukses", "Berhasil menambahkan program");
                }
                else
                {
                    isUpdateProgram = true;
                    var checkName = _context.SFMProgram.Any(a => a.ProgramName == data.ProgramName && a.ProgramName != data.ProgramName);
                    if (checkName)
                    {
                        _notif.SetFailed("Gagal", "Nama program ini telah digunakan. Mohon untuk dapat menginputkan nama yang berbeda");
                        return RedirectToAction("FormProgram", "Home");
                    }

                    var program = _context.SFMProgram.FirstOrDefault(a => a.Id == data.Id);
                    var oldProgram = program.ProgramName;
                    program.ProgramName = data.ProgramName;
                    program.PPR = "BDF";
                    program.StartDate = data.StartDate;
                    program.DefaultExpDate = data.DefaultExpDate;
                    program.UpdatedAt = DateTime.Now;

                    _context.SFMProgram.Update(program);
                    result = _context.SaveChanges();
                    if (result != 0)
                    {
                        if (oldProgram != program.ProgramName)
                        {
                            var EntitlementRelate = _context.SFMEntitlement.AsNoTracking().Where(a => a.ProgramName == oldProgram).ToList();
                            var proposalRelate = _context.ApprovalTask.Where(a => a.Title == "SFM Proposal").ToList();

                            foreach (var item in EntitlementRelate)
                            {
                                item.ProgramName = data.ProgramName;
                                _context.SFMEntitlement.Update(item);
                                await _context.SaveChangesAsync();

                                foreach (var proposalItem in proposalRelate)
                                {
                                    var JsonData = JsonConvert.DeserializeObject<SFMProposalDetail>(proposalItem.Data);
                                    if (!JsonData.DetailBalanceTaken.Any(x => x.EntitlementId == item.Id)) continue;
                                    JsonData.Program = JsonData.Program.Replace(oldProgram, data.ProgramName);

                                    proposalItem.Data = JsonConvert.SerializeObject(JsonData);

                                    _context.ApprovalTask.Update(proposalItem);
                                    await _context.SaveChangesAsync();
                                }
                            }
                        }
                        _notif.SetSucced("Sukses", "Berhasil membaharui program");
                    }

                }

                if (result != 0 && !string.IsNullOrEmpty(data.IdDistributor))
                {
                    if (isUpdateProgram)
                    {
                        var getprogramdistributor = _context.ProgramSFMDistributor.Where(a => a.DataProgramId == data.Id).ToList();
                        _context.ProgramSFMDistributor.RemoveRange(getprogramdistributor);
                    }
                    else data.Id = _context.SFMProgram.FirstOrDefault(a => a.ProgramName == data.ProgramName).Id;

                    var dataDistributor = data.IdDistributor.Split(",").ToList();
                    var AddRangeProgramDistributor = new List<ProgramSFMDistributor>() { };

                    foreach (var item in dataDistributor)
                    {
                        var programdistributor = new ProgramSFMDistributor()
                        {
                            DataProgramId = data.Id,
                            UserDistributorNumber = item
                        };
                        AddRangeProgramDistributor.Add(programdistributor);
                    }

                    if (AddRangeProgramDistributor.Count != 0)
                    {
                        _context.ProgramSFMDistributor.AddRange(AddRangeProgramDistributor);
                        await _context.SaveChangesAsync();
                    }
                }

                return RedirectToAction("Program", "Home");
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        [Authorize(Policy = "Distributor")]
        public async Task<IActionResult> SubmitProposal(ProposalDTO data)
        {
            try
            {
                if (!string.IsNullOrEmpty(data.NamaAktivitas)) data.NamaAktivitas = data.NamaAktivitas.Trim();
                if (!string.IsNullOrEmpty(data.SegmentPasar)) data.SegmentPasar = data.SegmentPasar.Trim();

                var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
                var IsSuccess = false;
                //var IsRebateBooster = data.TipeAktivitas == "Rebate Booster" ? true : false;
                var programName = string.Empty;

                if (string.IsNullOrEmpty(data.SumberBudget))
                {
                    _notif.SetFailed("Gagal", "Mohon untuk menentukan sumber budget terlebih dahulu");
                    return RedirectToAction("FormProposal", "Home");
                }

                if (data.ProposalNumber == 0)
                {
                    var proposalNumber = EClaimNumber();
                    data.ProposalNumber = proposalNumber;
                }

                // File Uploaded Handler
                var upload = new List<ProposalFile>() { };
                string[] imageExtensions = { ".jpg", ".jpeg", ".jfif", ".pjpeg", ".pjp", ".png", ".doc", ".docx", ".pdf", ".pptx", ".ppt", ".xls", ".xlsx", ".zip", ".rar", ".7z" };
                var myFile = Request.Form.Files;

                if (data.UploadedFile != null)
                {
                    var FolderPathDefault = Path.Combine(_hostingEnvironment.WebRootPath, "uploads", "proposal", user.AccountDistributor);
                    var FolderPath = Path.Combine(FolderPathDefault, data.ProposalNumber.ToString());
                    if (!Directory.Exists(FolderPath)) Directory.CreateDirectory(FolderPath);
                    var IdFileCounter = 1;

                    var listFilename = new List<UploadedFilename>() { };
                    if (!string.IsNullOrEmpty(data.ListFilename)) listFilename = JsonConvert.DeserializeObject<List<UploadedFilename>>(data.ListFilename);


                    foreach (var uploadedname in listFilename)
                    {
                        foreach(var itemname in uploadedname.listFilename)
                        {
                            var file = data.UploadedFile.Where(a => a.FileName == itemname).FirstOrDefault();

                            var itemExtension = System.IO.Path.GetExtension(file.FileName);
                            if (!imageExtensions.Any(a => a == itemExtension)) continue;

                            var fileName = file.FileName.ToLower();
                            var fieldname = uploadedname.fieldname;

                            var pathDefault = Path.Combine("uploads", "proposal", user.AccountDistributor, data.ProposalNumber.ToString());
                            var path = Path.Combine(pathDefault, file.FileName);
                            var fullPath = Path.Combine(_hostingEnvironment.WebRootPath, path);

                            if (System.IO.File.Exists(fullPath)) System.IO.File.Delete(fullPath);

                            using (var fileStream = System.IO.File.Create(fullPath))
                            {
                                file.CopyTo(fileStream);
                            }

                            var itemUploaded = new ProposalFile()
                            {
                                Id = IdFileCounter,
                                Filename = file.FileName,
                                Fieldname = fieldname,
                                Url = path.Replace("\\", "/")
                            };
                            upload.Add(itemUploaded);
                            IdFileCounter = IdFileCounter + 1;

                            _log.SetLogUpload(user, "Upload Proposal Document", path);
                        }
                    }

                    //foreach (var item in data.UploadedFile)
                    //{
                    //    var itemExtension = System.IO.Path.GetExtension(item.FileName);
                    //    if (!imageExtensions.Any(a => a == itemExtension)) continue;

                    //    var fileName = item.FileName.ToLower();
                    //    var fieldname = string.Empty;
                    //    //var path = Path.Combine("uploads\\proposal\\" + user.AccountDistributor + "\\" + data.ProposalNumber, item.FileName);

                    //    var pathDefault = Path.Combine("uploads", "proposal", user.AccountDistributor, data.ProposalNumber.ToString());
                    //    var path = Path.Combine(pathDefault, item.FileName);
                    //    var fullPath = Path.Combine(_hostingEnvironment.WebRootPath, path);

                    //    if (System.IO.File.Exists(fullPath)) System.IO.File.Delete(fullPath);

                    //    using (var fileStream = System.IO.File.Create(fullPath))
                    //    {
                    //        item.CopyTo(fileStream);
                    //    }

                    //    var itemUploaded = new ProposalFile()
                    //    {
                    //        Id = IdFileCounter,
                    //        Filename = item.FileName,
                    //        Fieldname = fieldname,
                    //        Url = path.Replace("\\", "/")
                    //    };
                    //    upload.Add(itemUploaded);
                    //    IdFileCounter = IdFileCounter + 1;

                    //    _log.SetLogUpload(user, "Upload Proposal Document", path);
                    //}
                }

                var proposal = new Proposal() { };
                var approvalTask = new ApprovalTask() { };
                var oldapprovalTask = new ApprovalTask() { };
                var input = new SFMProposalDetail() { };
                var EntitlementList = new List<SFMEntitlement>() { };
                var EntitlementIdList = string.IsNullOrEmpty(data.SumberBudget) ? new int[] { } : Array.ConvertAll(data.SumberBudget.Split(",").ToArray(), a => int.Parse(a));
                long NominalRebateBooster = data.Nominal;

                //proposal = _context.Proposal.FirstOrDefault(a => a.ProposalNumber == data.ProposalNumber);
                //if (proposal != null) NominalRebateBooster = proposal.Nominal;

                var DetailBalance = new List<DetailBalanceTaken>() { };

                foreach (var item in EntitlementIdList)
                {
                    var Entitlement = _context.SFMEntitlement.FirstOrDefault(a => a.Id == item);
                    if (Entitlement != null)
                    {
                        EntitlementList.Add(Entitlement);
                    }
                }

                programName = string.Join(" & ", EntitlementList.Select(a => a.ProgramName));
                var expiry = EntitlementList.OrderBy(a => a.ExpiredDate).Select(a => a.ExpiredDate).FirstOrDefault();

                //KPI setter
                if (!string.IsNullOrEmpty(data.KPI))
                {
                    var resultKPI = new List<string>() { };
                    var dataKPI = data.KPI.Split("##Separator##").ToList();
                    foreach (var kpi in dataKPI)
                    {
                        if (!string.IsNullOrEmpty(kpi)) resultKPI.Add(kpi.Trim());
                    }
                    data.KPI = JsonConvert.SerializeObject(resultKPI);
                }

                if (data.Id == 0)
                {
                    var Nominal = data.Nominal;
                    var isCount = false;
                    var z = 0;
                    foreach (var item in EntitlementList.OrderBy(a => a.ExpiredDate))
                    {
                        if (isCount) continue;
                        z = z + 1;
                        if (item.EntitlementBalance > Nominal)
                        {
                            item.EntitlementBalance = item.EntitlementBalance - Nominal;
                            DetailBalance.Add(new DetailBalanceTaken() { EntitlementId = item.Id, Order = z, Nominal = Nominal });
                            Nominal = 0;
                            isCount = true;
                        }
                        else
                        {
                            Nominal = Nominal - item.EntitlementBalance;
                            DetailBalance.Add(new DetailBalanceTaken() { EntitlementId = item.Id, Order = z, Nominal = item.EntitlementBalance });
                            item.EntitlementBalance = 0;
                        }

                        _context.SFMEntitlement.Update(item);
                        await _context.SaveChangesAsync();
                    }

                    input = new SFMProposalDetail()
                    {
                        ProposalDate = DateTime.Now,
                        EntitlementId = data.SumberBudget,
                        Program = programName,
                        Nominal = data.Nominal,
                        ExpiryDate = expiry,
                        ProposalNumber = data.ProposalNumber,
                        DetailBalanceTaken = DetailBalance,
                    };

                    approvalTask = new ApprovalTask()
                    {
                        Title = "SFM Proposal",
                        Description = "SFM proposal by " + user.Id,
                        SubmittedById = user.Id,
                        Data = JsonConvert.SerializeObject(input),
                        TypeData = "Claim",
                        CurrentStepId = "238fb004-cbfb-4b79-9de7-9d28bdf7dfee",
                        ApprovalTemplateId = "6778c30e-77be-4a94-b20d-e067308462bf",
                        ClaimNumber = data.ProposalNumber,
                        Status = "Open",
                        CreatedAt = DateTime.UtcNow.AddHours(7),
                        UpdatedAt = DateTime.UtcNow.AddHours(7)
                    };

                    _context.ApprovalTask.Add(approvalTask);
                    var result = _context.SaveChanges();
                    if (result != 0)
                    {
                        IsSuccess = true;
                        if (data.DraftId != 0)
                        {
                            var draftProposal = _context.Proposal.Where(a => a.Id == data.DraftId).FirstOrDefault();
                            if (draftProposal.Status == "Draft") proposal = draftProposal;
                        }

                        proposal.ProposalNumber = data.ProposalNumber;
                        proposal.AccountNumber = user.AccountDistributor;
                        proposal.ApprovalTaskId = approvalTask.Id;
                        proposal.Kategori = data.Kategori;
                        proposal.TipeAktivitas = data.TipeAktivitas;
                        proposal.NamaAktivitas = data.NamaAktivitas;
                        proposal.SegmentPasar = data.SegmentPasar;
                        proposal.SubSegmentPasar = data.SubSegmentPasar;
                        proposal.TanggalAktivitas = data.TanggalAktivitas;
                        proposal.SumberBudget = data.SumberBudget;
                        proposal.Opportunity = data.Opportunity;
                        proposal.KPI = data.KPI;
                        proposal.Nominal = data.Nominal;
                        proposal.NominalBalance = data.Nominal;
                        proposal.UploadedFile = JsonConvert.SerializeObject(upload);
                        proposal.Status = approvalTask.Status;
                        proposal.ProposalAktivitas = data.ProposalAktivitas;
                        proposal.QuotationVendor = data.QuotationVendor;

                        _context.Proposal.Update(proposal);
                        await _context.SaveChangesAsync();
                    }
                }
                else
                {
                    proposal = await _context.Proposal.FirstOrDefaultAsync(a => a.ProposalNumber == data.ProposalNumber);
                    approvalTask = await _context.ApprovalTask.FirstOrDefaultAsync(a => a.ClaimNumber == data.ProposalNumber);
                    oldapprovalTask = approvalTask;
                    input = JsonConvert.DeserializeObject<SFMProposalDetail>(approvalTask.Data);

                    if (data.Nominal != proposal.Nominal)
                    {
                        var iscount = false;
                        long selisih = data.Nominal > proposal.Nominal ? data.Nominal - proposal.Nominal : proposal.Nominal - data.Nominal;
                        input.DetailBalanceTaken = data.Nominal > proposal.Nominal ? input.DetailBalanceTaken.OrderBy(a => a.Order).ToList() : input.DetailBalanceTaken.OrderByDescending(a => a.Order).ToList();

                        foreach (var item in input.DetailBalanceTaken)
                        {
                            var Entitlement = _context.SFMEntitlement.FirstOrDefault(a => a.Id == item.EntitlementId);
                            if (iscount) continue;

                            if (data.Nominal > proposal.Nominal)
                            {
                                if (Entitlement.EntitlementBalance == 0) continue;
                                if (selisih > Entitlement.EntitlementBalance)
                                {
                                    selisih = selisih - Entitlement.EntitlementBalance;
                                    item.Nominal = item.Nominal + Entitlement.EntitlementBalance;
                                    Entitlement.EntitlementBalance = 0;
                                }
                                else
                                {
                                    Entitlement.EntitlementBalance = Entitlement.EntitlementBalance - selisih;
                                    item.Nominal = item.Nominal + selisih;
                                    selisih = 0;
                                }
                            }
                            else
                            {
                                if (item.Nominal == 0) continue;
                                if (item.Nominal > selisih)
                                {
                                    item.Nominal = item.Nominal - selisih;
                                    Entitlement.EntitlementBalance = Entitlement.EntitlementBalance + selisih;
                                    selisih = 0;
                                }
                                else
                                {
                                    selisih = selisih - item.Nominal;
                                    Entitlement.EntitlementBalance = Entitlement.EntitlementBalance + item.Nominal;
                                    item.Nominal = 0;
                                }
                            }

                            if (selisih == 0) iscount = true;
                            _context.SFMEntitlement.Update(Entitlement);
                            _context.SaveChanges();
                        }
                        input.Nominal = data.Nominal;
                    }

                    approvalTask.Data = JsonConvert.SerializeObject(input);
                    _context.ApprovalTask.Update(approvalTask);
                    await _context.SaveChangesAsync();

                    proposal.Kategori = data.Kategori;
                    proposal.TipeAktivitas = data.TipeAktivitas;
                    proposal.NamaAktivitas = data.NamaAktivitas;
                    proposal.SegmentPasar = data.SegmentPasar;
                    proposal.SubSegmentPasar = data.SubSegmentPasar;
                    proposal.TanggalAktivitas = data.TanggalAktivitas;
                    proposal.Opportunity = data.Opportunity;
                    proposal.KPI = data.KPI;
                    proposal.Nominal = data.Nominal;
                    proposal.NominalBalance = data.Nominal;

                    var UpdateUploaded = JsonConvert.DeserializeObject<List<ProposalFile>>(proposal.UploadedFile);
                    UpdateUploaded.AddRange(upload);
                    proposal.UploadedFile = JsonConvert.SerializeObject(UpdateUploaded);

                    _context.Proposal.Update(proposal);
                    await _context.SaveChangesAsync();

                    IsSuccess = true;
                }

                if (IsSuccess)
                {
                    var newData = _stepApproval.setSerialize(approvalTask);
                    if (approvalTask.Status == "Open")
                    {
                        _log.SetLogApproval(approvalTask, user, "Submit Form", newData, null);
                    }
                    else
                    {
                        var oldData = _stepApproval.setSerialize(oldapprovalTask);
                        _log.SetLogApproval(approvalTask, user, "Submit after " + approvalTask.Status, newData, oldData);
                    }

                    _stepApproval.Read(approvalTask.Id, null, "OnApproved");

                    var url = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}{HttpContext.Request.PathBase}" + "/Home/FormProposal/" + input.ProposalNumber;
                    var SendEmailApproval = _context.ApprovalTask.AsNoTracking().FirstOrDefault(x => x.ClaimNumber == input.ProposalNumber);
                    var currentStep = _context.StepBase.Where(a => a.Id == SendEmailApproval.CurrentStepId).FirstOrDefault();
                    _sendNotif.sendSubmitClaimSFMNotif(SendEmailApproval.ClaimNumber.ToString(), url, "Proposal", currentStep.Roles, user.AccountDistributor, null);
                    _notif.SetSucced("Sukses", "Berhasil Mengajukan Proposal");
                }
                else
                {
                    _notif.SetFailed("Terjadi Kesalahan", "Mohon untuk mengulangi beberapa saat lagi");
                }

                return RedirectToAction("Proposal", "Home");
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        [Authorize(Policy = "Distributor")]
        public async Task<IActionResult> SubmitClaim(ClaimDTO data)
        {
            try
            {
                if (!string.IsNullOrEmpty(data.Invoice)) data.Invoice = data.Invoice.Trim();

                var user = _userManager.FindByNameAsync(User.Identity.Name).Result;
                var IsSuccess = false;

                if (data.ClaimNumber == 0)
                {
                    var claimNumber = EClaimNumber();
                    data.ClaimNumber = claimNumber;
                }

                // File Uploaded Handler
                var upload = new List<ProposalFile>() { };
                string[] imageExtensions = { ".jpg", ".jpeg", ".jfif", ".pjpeg", ".pjp", ".png", ".doc", ".docx", ".pdf", ".xls", ".xlsx", ".zip", ".rar", ".7z" };
                var myFile = Request.Form.Files;

                if (data.UploadedFile != null)
                {
                    var FolderPathDefault = Path.Combine(_hostingEnvironment.WebRootPath, "uploads", "claimSFM", user.AccountDistributor);
                    var FolderPath = Path.Combine(FolderPathDefault, data.ClaimNumber.ToString());

                    if (!Directory.Exists(FolderPath)) Directory.CreateDirectory(FolderPath);
                    var IdFileCounter = 1;

                    var listFilename = new List<UploadedFilename>() { };
                    if (!string.IsNullOrEmpty(data.ListFilename)) listFilename = JsonConvert.DeserializeObject<List<UploadedFilename>>(data.ListFilename);

                    foreach (var uploadedname in listFilename)
                    {
                        foreach (var itemname in uploadedname.listFilename)
                        {
                            var file = data.UploadedFile.Where(a => a.FileName == itemname).FirstOrDefault();

                            var itemExtension = System.IO.Path.GetExtension(file.FileName);
                            if (!imageExtensions.Any(a => a == itemExtension)) continue;

                            var fileName = file.FileName.ToLower();
                            var fieldname = uploadedname.fieldname;

                            var pathDefault = Path.Combine("uploads", "claimSFM", user.AccountDistributor, data.ClaimNumber.ToString());
                            var path = Path.Combine(pathDefault, file.FileName);
                            var fullPath = Path.Combine(_hostingEnvironment.WebRootPath, path);

                            if (System.IO.File.Exists(fullPath)) System.IO.File.Delete(fullPath);

                            using (var fileStream = System.IO.File.Create(fullPath))
                            {
                                file.CopyTo(fileStream);
                            }

                            var itemUploaded = new ProposalFile()
                            {
                                Id = IdFileCounter,
                                Filename = file.FileName,
                                Url = path.Replace("\\", "/"),
                                Fieldname = fieldname
                            };
                            upload.Add(itemUploaded);
                            IdFileCounter = IdFileCounter + 1;

                            _log.SetLogUpload(user, "Upload Claim Document", path);
                        }
                    }

                    //foreach (var item in data.UploadedFile)
                    //{
                    //    var itemExtension = System.IO.Path.GetExtension(item.FileName);
                    //    if (!imageExtensions.Any(a => a == itemExtension)) continue;

                    //    var fileName = item.FileName.ToLower();
                    //    var fieldname = string.Empty;

                    //    var pathDefault = Path.Combine("uploads", "claimSFM", user.AccountDistributor, data.ClaimNumber.ToString());
                    //    var path = Path.Combine(pathDefault, item.FileName);
                    //    var fullPath = Path.Combine(_hostingEnvironment.WebRootPath, path);

                    //    if (System.IO.File.Exists(fullPath)) System.IO.File.Delete(fullPath);

                    //    using (var fileStream = System.IO.File.Create(fullPath))
                    //    {
                    //        item.CopyTo(fileStream);
                    //    }

                    //    var itemUploaded = new ProposalFile()
                    //    {
                    //        Id = IdFileCounter,
                    //        Filename = item.FileName,
                    //        Url = path.Replace("\\", "/"),
                    //        Fieldname = fieldname 
                    //    };
                    //    upload.Add(itemUploaded);
                    //    IdFileCounter = IdFileCounter + 1;

                    //    _log.SetLogUpload(user, "Upload Claim Document", path);
                    //}
                }

                var proposal = _context.Proposal.FirstOrDefault(a => a.ProposalNumber == data.ProposalNumber);
                var proposalApprovalTask = _context.ApprovalTask.FirstOrDefault(a => a.ClaimNumber == data.ProposalNumber);

                var approvalTask = new ApprovalTask() { };
                var oldapprovalTask = new ApprovalTask() { };
                var input = new SFMClaimDTO() { };

                var approvalValidation = _context.ApprovalTask.Any(a => a.Id == data.Id);
                if (!approvalValidation)
                {
                    proposal.NominalBalance = proposal.NominalBalance - data.Nominal;

                    input = new SFMClaimDTO()
                    {
                        ClaimNumber = data.ClaimNumber,
                        Invoice = data.Invoice,
                        InvoiceDate = data.InvoiceDate,
                        Hasil = string.IsNullOrEmpty(data.Hasil) ? null : JsonConvert.DeserializeObject<List<HasilValue>>(data.Hasil),
                        NamaAktivitas = proposal.NamaAktivitas,
                        ProposalId = proposal.Id,
                        Entitlement = proposal.Nominal,
                        UploadedFile = upload,
                        ClaimDate = DateTime.UtcNow.AddHours(7),
                        LastUpdate = DateTime.UtcNow.AddHours(7),
                        NominalClaim = data.Nominal,
                        InvoicePeserta = data.InvoicePeserta,
                        InvoiceVendor = data.InvoiceVendor,
                        LaporanAktivitas = data.LaporanAktivitas
                    };

                    approvalTask = new ApprovalTask()
                    {
                        Title = "SFM Claim",
                        Description = "SFM Claim by " + user.Email + " with account number " + user.AccountDistributor,
                        SubmittedById = user.Id,
                        Data = JsonConvert.SerializeObject(input),
                        TypeData = "Claim",
                        EntitlementId = proposal.Id,
                        CurrentStepId = "f85f580c-1a61-4bca-9854-e41796984856",
                        ApprovalTemplateId = "d403b1b1-a5cb-400e-8bca-6b7913f75b20",
                        ClaimNumber = data.ClaimNumber,
                        Status = "Open",
                        CreatedAt = DateTime.UtcNow.AddHours(7),
                        UpdatedAt = DateTime.UtcNow.AddHours(7)
                    };

                    _context.ApprovalTask.Add(approvalTask);
                    var result = _context.SaveChanges();
                    if (result != 0)
                    {
                        IsSuccess = true;

                        _context.Proposal.Update(proposal);
                        await _context.SaveChangesAsync();
                    }
                }
                else
                {
                    proposal = await _context.Proposal.FirstOrDefaultAsync(a => a.ProposalNumber == data.ProposalNumber);
                    approvalTask = await _context.ApprovalTask.FirstOrDefaultAsync(a => a.ClaimNumber == data.ClaimNumber);
                    oldapprovalTask = approvalTask;
                    input = JsonConvert.DeserializeObject<SFMClaimDTO>(approvalTask.Data);

                    if (data.Nominal != input.NominalClaim)
                    {
                        proposal.NominalBalance = (proposal.NominalBalance + input.NominalClaim) - data.Nominal;
                        _context.Proposal.Update(proposal);
                        await _context.SaveChangesAsync();
                        input.NominalClaim = data.Nominal;
                    }

                    input.Hasil = string.IsNullOrEmpty(data.Hasil) ? null : JsonConvert.DeserializeObject<List<HasilValue>>(data.Hasil);
                    input.Invoice = data.Invoice;
                    input.InvoiceDate = data.InvoiceDate;
                    input.UploadedFile.AddRange(upload);
                    input.LastUpdate = DateTime.UtcNow.AddHours(7);
                    input.InvoicePeserta = data.InvoicePeserta;
                    input.InvoiceVendor = data.InvoiceVendor;
                    input.LaporanAktivitas = data.LaporanAktivitas;

                    approvalTask.Data = JsonConvert.SerializeObject(input);
                    approvalTask.UpdatedAt = DateTime.UtcNow.AddHours(7);
                    _context.ApprovalTask.Update(approvalTask);
                    _context.SaveChanges();

                    IsSuccess = true;
                }

                if (IsSuccess)
                {
                    var newData = _stepApproval.setSerialize(approvalTask);
                    if (approvalTask.Status == "Open")
                    {
                        _log.SetLogApproval(approvalTask, user, "Submit Form", newData, null);
                    }
                    else
                    {
                        var oldData = _stepApproval.setSerialize(oldapprovalTask);
                        _log.SetLogApproval(approvalTask, user, "Submit after " + approvalTask.Status, newData, oldData);
                    }

                    _stepApproval.Read(approvalTask.Id, null, "OnApproved");

                    var url = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}{HttpContext.Request.PathBase}" + "/Home/FormClaim/" + input.ClaimNumber;
                    var SendEmailApproval = _context.ApprovalTask.AsNoTracking().FirstOrDefault(x => x.ClaimNumber == input.ClaimNumber);
                    var currentStep = _context.StepBase.Where(a => a.Id == SendEmailApproval.CurrentStepId).FirstOrDefault();
                    _sendNotif.sendSubmitClaimSFMNotif(SendEmailApproval.ClaimNumber.ToString(), url, "Claim", currentStep.Roles, user.AccountDistributor, approvalTask.CurrentStepId);
                    _notif.SetSucced("Sukses", "Berhasil Mengajukan Claim");
                }
                else
                {
                    _notif.SetFailed("Terjadi Kesalahan", "Mohon untuk mengulangi beberapa saat lagi");
                }

                return RedirectToAction("Claim", "Home");
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        [Authorize(Policy = "MarketingFinance")]
        public ActionResult UploadSFMEntitlement(string id)
        {
            var myFile = Request.Form.Files["myFile"];
            var targetLocation = Path.Combine(_hostingEnvironment.WebRootPath, "uploads");
            var extensionFile = Path.GetExtension(myFile.FileName);
            var user = _userManager.FindByNameAsync(User.Identity.Name).Result;

            try
            {
                var newName = Regex.Replace(id + DateTime.Now.ToString("hh mm ss fff"), @"\s+", "");
                newName = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(newName));
                var path = Path.Combine(targetLocation, newName + extensionFile);
                if (!Directory.Exists(targetLocation)) Directory.CreateDirectory(targetLocation);
                using (var fileStream = System.IO.File.Create(path))
                {
                    myFile.CopyTo(fileStream);
                }

                List<SFMEntitlement> EntitlementList = new List<SFMEntitlement>() { };
                List<SFMEntitlement> EntitlementUploadList = new List<SFMEntitlement>() { };
                List<SFMEntitlement> EntitlementUpdateList = new List<SFMEntitlement>() { };
                List<SFMEntitlement> EntitlementErrorList = new List<SFMEntitlement>() { };

                var ProgramList = _context.SFMProgram.AsNoTracking().ToList();
                var UserList = _context.Users.AsNoTracking();

                var listRow = new List<string>();
                var UserLogged = _userManager.FindByNameAsync(User.Identity.Name).Result;

                var DataSendEmailList = new List<UploadSendEmailViewModel>();
                var LinkBase = MyHttpContext.AppBaseUrl.Replace("http:", "https:");

                FileInfo file = new FileInfo(Path.GetFullPath(path));
                EntitlementList = _context.SFMEntitlement.AsNoTracking().ToList();

                using (ExcelPackage package = new ExcelPackage(file))
                {
                    DateTime _min = DateTime.Parse("01/01/2018");
                    DateTime _min2 = DateTime.Parse("01/01/1900");
                    ExcelWorksheet workSheet = package.Workbook.Worksheets[1];

                    if (workSheet.Dimension.Rows < 2)
                    {
                        _notif.SetFailed("Gagal", "File berisi data kosong. Mohon masukan data sesuai template terlebih dahulu");
                        return new EmptyResult();
                    }

                    Dictionary<int, string> columns = new Dictionary<int, string>();
                    for (var i = 1; i <= workSheet.Dimension.Columns; i++)
                    {
                        var textColumns = workSheet.Cells[1, i].Value;
                        if (textColumns == null) columns.Add(i, "");
                        else columns.Add(i, textColumns.ToString().ToLower());
                    }

                    if (!IsTemplateValid(columns))
                    {
                        _notif.SetFailed("Gagal", "Gagal karena file yang dimasukkan berbeda format dengan template");
                        return new EmptyResult();
                    }

                    for (int i = 2; i <= workSheet.Dimension.Rows; i++)
                    {
                        SFMEntitlement data = new SFMEntitlement();

                        data.Account = Convert.ToString(workSheet.Cells[i, 1].Value ?? string.Empty);
                        if (!string.IsNullOrEmpty(data.Account)) data.Account = data.Account.Trim();

                        var ProgramName = Convert.ToString(workSheet.Cells[i, 2].Value ?? string.Empty);
                        if (!string.IsNullOrEmpty(ProgramName)) ProgramName = ProgramName.Trim();

                        var entitlement = Convert.ToString(workSheet.Cells[i, 4].Value ?? string.Empty);
                        if (!string.IsNullOrEmpty(entitlement)) entitlement = entitlement.Trim();

                        // if row excel just deleted content
                        if (i > 2)
                        {
                            if (data.Account == string.Empty || ProgramName == string.Empty || entitlement == string.Empty) continue;
                        }

                        data.ProgramName = ProgramList.Where(a => a.ProgramName.ToLower() == ProgramName.ToLower()).Select(a => a.ProgramName).FirstOrDefault();

                        if (string.IsNullOrEmpty(data.ProgramName))
                        {
                            data.Remark = "Baris " + i.ToString() + " - Program tidak ditemukan";
                            EntitlementErrorList.Add(data);
                            continue;
                        }

                        var isNumeric = long.TryParse(entitlement, out long n);
                        if (isNumeric)
                        {
                            data.EntitlementValue = Convert.ToInt64(workSheet.Cells[i, 4].Value ?? 0);
                        }
                        else
                        {
                            data.Remark = "Baris " + i.ToString() + " - Format entitlement harus angka";
                            EntitlementErrorList.Add(data);
                            continue;
                        }

                        if (string.IsNullOrEmpty(data.Account) || string.IsNullOrEmpty(data.ProgramName))
                        {
                            data.Remark = "Baris " + i.ToString() + " - Account dan Program harap diisi";
                            EntitlementErrorList.Add(data);
                            continue;
                        }

                        var programUpload = ProgramList.Where(a => a.ProgramName.ToLower() == data.ProgramName.ToLower()).FirstOrDefault();
                        if (programUpload != null)
                        {
                            var dataProgramDistributor = _context.ProgramSFMDistributor.Where(x => x.DataProgramId == programUpload.Id && x.UserDistributorNumber == data.Account).FirstOrDefault();
                            if (dataProgramDistributor == null)
                            {
                                data.Remark = "Baris " + i.ToString() + " - Data Program untuk Distributor " + data.Account + " tidak ditemukan";
                                EntitlementErrorList.Add(data);
                                continue;
                            }
                        }

                        var uservalidation = UserList.Where(a => a.AccountDistributor == data.Account && a.LockoutEnd == null).Count();
                        if (uservalidation == 0)
                        {
                            data.Remark = "Baris " + i.ToString() + " - User Dengan Account " + data.Account + " tidak ditemukan";
                            EntitlementErrorList.Add(data);
                            continue;
                        }
                        data.ExpiredDate = GetDateInCell(workSheet.Cells[i, 3].Value);
                        if (data.ExpiredDate.Date == _min2.Date)
                        {
                            data.Remark = "Baris " + i.ToString() + " - Mohon perhatikan format expired date";
                            EntitlementErrorList.Add(data);
                            continue;
                        }

                        if (workSheet.Cells[i, 3].Value != null && data.ExpiredDate.Date < DateTime.Now.Date)
                        {
                            data.Remark = "Baris " + i.ToString() + " - Expired date yang dimasukkan sudah lewat dari hari ini";
                            EntitlementErrorList.Add(data);
                            continue;
                        }

                        if (data.ExpiredDate.Date == _min || data.ExpiredDate.Date == _min2.Date || data.ExpiredDate.Date == DateTime.MinValue)
                        {
                            data.ExpiredDate = ProgramList.Where(a => a.ProgramName.ToLower() == ProgramName.ToLower()).Select(a => a.DefaultExpDate).FirstOrDefault();
                        }

                        data.PPR = Convert.ToString(workSheet.Cells[i, 5].Value ?? string.Empty);
                        data.Remark = Convert.ToString(workSheet.Cells[i, 6].Value ?? string.Empty);
                        data.IsRejected = false;
                        data.CreatedAt = DateTime.Now;

                        //check data jika ada yang sama
                        if (EntitlementList.Any(x => x.Account.ToLower() == data.Account.ToLower() && x.ProgramName.ToLower() == data.ProgramName.ToLower()))
                        {
                            var dataExist = EntitlementList.Where(x => x.Account.ToLower() == data.Account.ToLower() && x.ProgramName.ToLower() == data.ProgramName.ToLower()).FirstOrDefault();
                            if (data.ExpiredDate.Date == _min.Date) data.ExpiredDate = dataExist.ExpiredDate;

                            //tambah update data
                            data.Id = dataExist.Id;
                            var newBalance = data.EntitlementValue;

                            if (dataExist.EntitlementValue > 0)
                            {
                                //handle perubahan entitlement
                                newBalance = dataExist.EntitlementBalance + (data.EntitlementValue - dataExist.EntitlementValue);
                                if (newBalance < 0)
                                {
                                    data.Remark = "Baris " + i.ToString() + " - Balance tidak cukup";
                                    EntitlementErrorList.Add(data);
                                    continue;
                                }
                            }

                            data.EntitlementBalance = newBalance;
                            data.LastClaimNumber = dataExist.LastClaimNumber;
                            EntitlementUpdateList.Add(data);
                        }
                        else
                        {
                            //check dateTime
                            //if (data.ExpiredDate.Date == _min.Date) data.ExpiredDate = dataProgramDistributor.DefaultExpDate;

                            //masukkan data baru
                            var EntitlementValidation = EntitlementUploadList.Where(a => a.Account.ToLower() == data.Account.ToLower() && a.ProgramName.ToLower() == data.ProgramName.ToLower()).FirstOrDefault();
                            if (EntitlementValidation != null && EntitlementUploadList.Count() != 0)
                            {
                                EntitlementUploadList.Remove(EntitlementValidation);
                            }

                            data.EntitlementBalance = data.EntitlementValue;
                            EntitlementUploadList.Add(data);

                            UploadSendEmailViewModel uploadData = new UploadSendEmailViewModel();
                            uploadData.Account = data.Account;
                            uploadData.PPR = data.PPR;
                            uploadData.EntitlementValue = data.EntitlementValue;
                            uploadData.User = UserList.Where(a => a.AccountDistributor == data.Account).FirstOrDefault();
                            uploadData.SendEmail = _emailSender.sendEmailViewModel(uploadData.User.Email, "", LinkBase, _hostingEnvironment.WebRootPath, null).Result;
                            uploadData.ProgramName = data.ProgramName;
                            uploadData.Remark = data.Remark;
                            uploadData.ExpiredDate = data.ExpiredDate;
                            uploadData.CreatedAt = data.CreatedAt;

                            DataSendEmailList.Add(uploadData);

                            _log.SetLogUploadUserEntitlement("Upload UserEntitlement Detail", data);
                        }
                    }

                    if (EntitlementErrorList.Count > 0)
                    {
                        listRow = EntitlementErrorList.Select(x => x.Remark.ToString()).ToList();
                        _notif.SetFailed("Gagal", "Gagal memasukkan data, mohon cek terlebih dahulu untuk data pada baris berikut : " + "\n\n" + String.Join(", \n\n", listRow));
                    }
                    else
                    {
                        var url = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}{HttpContext.Request.PathBase}" + "/Home/Entitlement";
                        if (EntitlementUpdateList.Count > 0)
                        {
                            var oldEntitlement = _context.SFMEntitlement.AsNoTracking().Where(x => EntitlementUpdateList.Select(x => x.Id).ToList().Contains(x.Id)).ToList();
                            _context.SFMEntitlement.UpdateRange(EntitlementUpdateList);
                            if (_context.SaveChanges() > 0)
                            {
                                _notif.SetSucced("Sukses", "Sukses Upload Data Entitlement");
                                foreach (var item in EntitlementUpdateList)
                                {
                                    _log.SetLogChanges(user, "Update Upload Entitlement Id - " + item.Id, JsonConvert.SerializeObject(item), JsonConvert.SerializeObject(oldEntitlement.Where(x => x.Id == item.Id).FirstOrDefault()));
                                }
                            }
                            else
                            {
                                _notif.SetFailed("Gagal", "Gagal Upload Entitlement. Silakan ulangi kembali");
                            }
                        }

                        if (EntitlementUploadList.Count > 0)
                        {
                            _context.SFMEntitlement.AddRange(EntitlementUploadList);
                            if (_context.SaveChanges() > 0)
                            {
                                _notif.SetSucced("Sukses", "Sukses Upload Data Entitlement");
                                var accountEntitlement = EntitlementUploadList.GroupBy(x => x.Account).Distinct();
                                foreach (var item in accountEntitlement)
                                {
                                    _sendNotif.sendUploadSFMEntitlementNotif(url, item.Key);
                                }
                            }
                            else
                            {
                                _notif.SetFailed("Gagal", "Gagal Upload Entitlement. Silakan ulangi kembali");
                            }
                        }
                    }
                }

                return new EmptyResult();
            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                _notif.SetFailed("Gagal", "Terjadi kesalahan. Mohon ulangi lagi.");
                return new BadRequestResult();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ResultAjax ApproveClaim([FromBody] SFMClaimDTO dataInput)
        {
            ResultAjax result = new ResultAjax();
            var user = _context.Users.Where(x => x.Id == dataInput.UserId).FirstOrDefault();

            var approvalTask = _context.ApprovalTask.Where(x => x.Id == dataInput.ApprovalTaskId).FirstOrDefault();
            var approvalJson = JsonConvert.DeserializeObject<SFMClaimDTO>(approvalTask.Data);
            var oldApprovalTask = approvalTask;
            var oldStep = _context.StepBase.AsNoTracking().Where(x => x.Id == oldApprovalTask.CurrentStepId).FirstOrDefault();
            if (oldStep != null && oldStep.Roles != user.RoleName)
            {
                result.Status = "InvalidRole";
                _notif.SetFailed("Gagal", "Approval di claim ini sudah terupdate sebelumnya. Mohon untuk refresh ulang halaman Anda.");
                return result;
            }

            if (oldStep.Roles == "Finance")
            {
                approvalJson.AmountFinance = dataInput.AmountFinance;
                approvalJson.BillingDocNumber = dataInput.BillingDocNumber;
                approvalJson.VATAmount = dataInput.VATAmount;
                approvalJson.WHTAmount = dataInput.WHTAmount;
                approvalJson.BillingDate = dataInput.BillingDate;
                approvalJson.PPRNumber = dataInput.PPRNumber;

                approvalTask.Data = JsonConvert.SerializeObject(approvalJson);

                var proposal = _context.Proposal.FirstOrDefault(a => a.Id == approvalTask.EntitlementId);
                proposal.NominalBalance = (proposal.NominalBalance + approvalJson.NominalClaim) - dataInput.AmountFinance;

                _context.Proposal.Update(proposal);
                _context.SaveChanges();
            }

            if (dataInput.IsNeedLegal)
            {
                approvalJson.IsNeedLegal = true;
                approvalJson.NeedLegalApproval = true;
                approvalTask.Data = JsonConvert.SerializeObject(approvalJson);
            }

            approvalTask.CurrentPICId = user.Id;
            approvalTask.UpdatedAt = DateTime.UtcNow.AddHours(7);

            _context.ApprovalTask.Update(approvalTask);
            if (_context.SaveChanges() > 0)
            {
                string newData = _stepApproval.setSerialize(approvalTask);
                string oldData = _stepApproval.setSerialize(oldApprovalTask);

                if (dataInput.Note != "")
                {
                    var CommentPost = new ApprovalComment()
                    {
                        ApprovalTaskId = approvalTask.Id,
                        UserId = user.Id,
                        Comment = dataInput.Note
                    };
                    _context.ApprovalComment.Add(CommentPost);
                    _context.SaveChanges();

                    _log.SetLogApproval(approvalTask, user, "Approve Claim - " + user.RoleName, newData, oldData, dataInput.Note);
                }
                else
                {
                    _log.SetLogApproval(approvalTask, user, "Approve Claim - " + user.RoleName, newData, oldData);
                }

                var nextStep = _stepApproval.Read(dataInput.ApprovalTaskId, null, "OnApproved");
                if (nextStep == null)
                {
                    //RevertApprovalTaskSet(oldApprovalJson, dataInput, null, null, "BDF");
                    result.Content = "Terjadi kesalahan. Mohon ulangi lagi.";
                    _notif.SetFailed("Gagal", result.Content);
                    result.Status = "Failed";
                }
                else
                {
                    _notif.SetSucced("Sukses", "Berhasil melakukan approve");
                    result.Content = "Selamat.";
                    result.Status = "Success";
                    var url = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}{HttpContext.Request.PathBase}" + "/Home/FormClaim/" + approvalTask.ClaimNumber;
                    var approval = _context.ApprovalTask.Where(a => a.Title == "SFM Claim" && a.ClaimNumber == dataInput.ClaimNumber).AsNoTracking().FirstOrDefault();
                    _sendNotif.sendApproveSFMNotif(oldApprovalTask, url, "No");
                }
            }
            else
            {
                //RevertApprovalTaskSet(oldApprovalJson, dataInput, null, null, "BDF");
                result.Content = "Terjadi kesalahan. Mohon ulangi lagi.";
                _notif.SetFailed("Gagal", result.Content);
                result.Status = "Failed";
            }

            return result;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ResultAjax SendBackClaim([FromBody] SFMClaimDTO dataInput)
        {
            ResultAjax result = new ResultAjax();
            var user = _context.Users.Where(x => x.Id == dataInput.UserId).First();
            var approvalTask = _context.ApprovalTask.Where(x => x.Id == dataInput.ApprovalTaskId).FirstOrDefault();
            var oldApprovalTask = approvalTask;

            var oldStep = _context.StepBase.AsNoTracking().Where(x => x.Id == oldApprovalTask.CurrentStepId).FirstOrDefault();
            if (oldStep != null && oldStep.Roles != user.RoleName)
            {
                result.Status = "InvalidRole";
                _notif.SetFailed("Gagal", "Approval di claim ini sudah terupdate sebelumnya. Mohon untuk refresh ulang halaman Anda.");
                return result;
            }

            if (dataInput.Note == "")
            {
                result.Content = "Mohon masukkan komentar sebelum melakukan send back.";
                result.Status = "NoComment";
                return result;
            }
            else
            {
                SetNotif _notif = new SetNotif(HttpContext);
                
                approvalTask.CurrentPICId = user.Id;
                approvalTask.UpdatedAt = DateTime.UtcNow.AddHours(7);

                _context.ApprovalTask.Update(approvalTask);
                if (_context.SaveChanges() > 0)
                {
                    string newData = _stepApproval.setSerialize(approvalTask);
                    string oldData = _stepApproval.setSerialize(oldApprovalTask);

                    if (dataInput.Note != "")
                    {
                        var CommentPost = new ApprovalComment()
                        {
                            ApprovalTaskId = approvalTask.Id,
                            UserId = user.Id,
                            Comment = dataInput.Note
                        };

                        _context.ApprovalComment.Add(CommentPost);
                        _context.SaveChanges();

                        _log.SetLogApproval(approvalTask, user, "Send Back Claim From " + user.RoleName, newData, oldData, dataInput.Note);
                    }
                    else
                    {
                        _log.SetLogApproval(approvalTask, user, "Send Back Claim From " + user.RoleName, newData, oldData);
                    }

                    var nextStep = _stepApproval.Read(dataInput.ApprovalTaskId, null, "OnRejected");
                    if (nextStep == null)
                    {
                        //RevertApprovalTaskSet(oldApprovalJson, dataInput, null, null, "BDF");
                        result.Content = "Terjadi kesalahan. Mohon ulangi lagi.";
                        _notif.SetFailed("Gagal", result.Content);
                        result.Status = "Failed";
                    }
                    else
                    {
                        _notif.SetSucced("Sukses", "Berhasil melakukan send back");
                        result.Content = "Selamat.";
                        result.Status = "Success";
                        var url = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}{HttpContext.Request.PathBase}" + "/Home/FormClaim/" + approvalTask.ClaimNumber;
                        _sendNotif.sendSendBackSFMNotif(oldApprovalTask, url);
                    }
                }
                else
                {
                    //RevertApprovalTaskSet(oldApprovalJson, dataInput, null, null, "BDF");
                    result.Content = "Terjadi kesalahan. Mohon ulangi lagi.";
                    _notif.SetFailed("Gagal", result.Content);
                    result.Status = "Failed";
                }
                return result;
            }
        }

        [HttpPost]
        public async Task<string> DelegateToChannelLead(SFMClaimDTO dataInput)
        {
            //ResultAjax result = new ResultAjax();
            var user = _context.Users.Where(x => x.Id == dataInput.UserId).FirstOrDefault();

            var approvalTask = _context.ApprovalTask.Where(x => x.Id == dataInput.ApprovalTaskId).FirstOrDefault();
            var oldApprovalTask = approvalTask;

            approvalTask.CurrentStepId = "654ee2de-06cd-4783-92dc-ba5b17a28fb5";
            approvalTask.CurrentPICId = user.Id;
            approvalTask.UpdatedAt = DateTime.UtcNow.AddHours(7);

            _context.ApprovalTask.Update(approvalTask);
            if (_context.SaveChanges() > 0)
            {
                string newData = _stepApproval.setSerialize(approvalTask);
                string oldData = _stepApproval.setSerialize(oldApprovalTask);

                var nextStep = _stepApproval.Read(dataInput.ApprovalTaskId, null, "OnApproved");
                if (nextStep == null)
                {
                    //RevertApprovalTaskSet(oldApprovalJson, dataInput, null, null, "BDF");
                    //result.Content = "Terjadi kesalahan. Mohon ulangi lagi.";
                    //_notif.SetFailed("Gagal", result.Content);
                    //result.Status = "Failed";
                    return "gagal";
                }
                else
                {
                    //_notif.SetSucced("Sukses", "Berhasil mendelegasikan claim");
                    //result.Content = "Selamat.";
                    //result.Status = "Success";
                    var url = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}{HttpContext.Request.PathBase}" + "/Home/FormClaim/" + approvalTask.ClaimNumber;
                    //var approval = _context.ApprovalTask.Where(a => a.Title == "SFM Claim" && a.ClaimNumber == dataInput.ClaimNumber).AsNoTracking().Include(x => x.CurrentStep).FirstOrDefault();

                    _log.SetLogApproval(approvalTask, user, "Delegasi Claim", newData, null);
                    _sendNotif.sendApproveSFMNotif(oldApprovalTask, url, "Yes");
                }
            }
            else
            {
                //RevertApprovalTaskSet(oldApprovalJson, dataInput, null, null, "BDF");
                //result.Content = "Terjadi kesalahan. Mohon ulangi lagi.";
                //_notif.SetFailed("Gagal", result.Content);
                //result.Status = "Failed";
                return "gagal";
            }

            return "Done";
        }

        [HttpGet]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delegate(int Id, string token)
        {
            ResultAjax result = new ResultAjax();
            var user = _context.Users.Where(x => x.RoleName == "Marketing" && x.IsSFMUser && x.OtherEmail == token).FirstOrDefault();
            if (user == null) RedirectToAction("FormClaim", "Home", new { Id });

            var authToken = string.Empty;

            var userClaim = await _userManager.GetClaimsAsync(user);
            if (userClaim.Count > 0)
            {
                var DataUserClaim = _context.UserClaims.Where(a => a.UserId == user.Id && a.ClaimType != "token");
                authToken = DataUserClaim.Where(a => a.ClaimType == "token").Select(a => a.ClaimValue).FirstOrDefault();

                _context.UserClaims.RemoveRange(DataUserClaim);
                _context.SaveChanges();
            }

            var claims = new List<Claim>
            {
                new Claim("email", user.Email),
                new Claim("username", user.Name),
                new Claim(ClaimTypes.Role, user.RoleName)
            };

            await _userManager.AddClaimsAsync(user, claims);
            await _signInManager.PasswordSignInAsync(user.UserName, "f6036D6!80caf44d", false, lockoutOnFailure: false);

            var approvalTask = _context.ApprovalTask.Where(x => x.ClaimNumber == Id).FirstOrDefault();
            if (approvalTask == null) RedirectToAction("", "");
            var oldApprovalTask = approvalTask;

            approvalTask.CurrentStepId = "654ee2de-06cd-4783-92dc-ba5b17a28fb5";
            approvalTask.CurrentPICId = user.Id;
            approvalTask.UpdatedAt = DateTime.UtcNow.AddHours(7);

            _context.ApprovalTask.Update(approvalTask);
            if (_context.SaveChanges() > 0)
            {
                string newData = _stepApproval.setSerialize(approvalTask);
                string oldData = _stepApproval.setSerialize(oldApprovalTask);

                var nextStep = _stepApproval.Read(approvalTask.Id, null, "OnApproved");
                if (nextStep == null) return RedirectToAction("", "");
                else
                {
                    var url = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}{HttpContext.Request.PathBase}" + "/Home/FormClaim/" + approvalTask.ClaimNumber;

                    _log.SetLogApproval(approvalTask, user, "Delegasi Claim", newData, null);
                    _sendNotif.sendApproveSFMNotif(oldApprovalTask, url, "Yes");
                }
            }
            else return RedirectToAction("", "");

            return RedirectToAction("", "");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ResultAjax ApproveProposal([FromBody] SFMProposalDetail dataInput)
        {
            ResultAjax result = new ResultAjax();
            var user = _context.Users.Where(x => x.Id == dataInput.UserId).FirstOrDefault();

            var approvalTask = _context.ApprovalTask.Where(x => x.Id == dataInput.ApprovalTaskId).FirstOrDefault();
            var oldApprovalTask = approvalTask;

            var oldStep = _context.StepBase.AsNoTracking().Where(x => x.Id == oldApprovalTask.CurrentStepId).FirstOrDefault();
            if (oldStep != null && oldStep.Roles != user.RoleName)
            {
                result.Status = "InvalidRole";
                _notif.SetFailed("Gagal", "Approval di form ini sudah terupdate sebelumnya. Mohon untuk refresh ulang halaman Anda.");
                return result;
            }

            if (dataInput.IsNeedLegal)
            {
                var ApprovalJson = JsonConvert.DeserializeObject<SFMProposalDetail>(approvalTask.Data);
                ApprovalJson.IsNeedLegal = true;
                ApprovalJson.NeedLegalApproval = true;
                approvalTask.Data = JsonConvert.SerializeObject(ApprovalJson);
            }

            approvalTask.CurrentPICId = user.Id;
            approvalTask.UpdatedAt = DateTime.UtcNow.AddHours(7);

            _context.ApprovalTask.Update(approvalTask);
            if (_context.SaveChanges() > 0)
            {
                string newData = _stepApproval.setSerialize(approvalTask);
                string oldData = _stepApproval.setSerialize(oldApprovalTask);

                if (dataInput.Note != "")
                {
                    var CommentPost = new ApprovalComment()
                    {
                        ApprovalTaskId = approvalTask.Id,
                        UserId = user.Id,
                        Comment = dataInput.Note
                    };
                    _context.ApprovalComment.Add(CommentPost);
                    _context.SaveChanges();

                    _log.SetLogApproval(approvalTask, user, "Approve Proposal - " + user.RoleName, newData, oldData, dataInput.Note);
                }
                else
                {
                    _log.SetLogApproval(approvalTask, user, "Approve Proposal - " + user.RoleName, newData, oldData);
                }

                var nextStep = _stepApproval.Read(dataInput.ApprovalTaskId, null, "OnApproved");
                if (nextStep == null)
                {
                    //RevertApprovalTaskSet(oldApprovalJson, dataInput, null, null, "BDF");
                    result.Content = "Terjadi kesalahan. Mohon ulangi lagi.";
                    _notif.SetFailed("Gagal", result.Content);
                    result.Status = "Failed";
                }
                else
                {
                    if (nextStep.Title == "Send Email Proposal Closed" || nextStep.OnApprovedId == "DONE")
                    {
                        var proposal = _context.Proposal.FirstOrDefault(a => a.ProposalNumber == approvalTask.ClaimNumber);
                        proposal.Status = "Closed";
                        _context.Update(proposal);
                        _context.SaveChanges();
                    }

                    _notif.SetSucced("Sukses", "Berhasil melakukan approve");
                    result.Content = "Selamat.";
                    result.Status = "Success";
                    var url = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}{HttpContext.Request.PathBase}" + "/Home/FormProposal/" + approvalTask.ClaimNumber;
                    var approval = _context.ApprovalTask.Where(a => a.Title == "SFM Proposal" && a.ClaimNumber == dataInput.ProposalNumber).AsNoTracking().FirstOrDefault();
                    _sendNotif.sendApproveSFMNotif(oldApprovalTask, url, "No");
                }
            }
            else
            {
                //RevertApprovalTaskSet(oldApprovalJson, dataInput, null, null, "BDF");
                result.Content = "Terjadi kesalahan. Mohon ulangi lagi.";
                _notif.SetFailed("Gagal", result.Content);
                result.Status = "Failed";
            }

            return result;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ResultAjax SendBackProposal([FromBody] SFMProposalDetail dataInput)
        {
            ResultAjax result = new ResultAjax();
            var user = _context.Users.Where(x => x.Id == dataInput.UserId).First();
            var approvalTask = _context.ApprovalTask.Where(x => x.Id == dataInput.ApprovalTaskId).FirstOrDefault();
            var oldApprovalTask = approvalTask;
            
            var oldStep = _context.StepBase.AsNoTracking().Where(x => x.Id == oldApprovalTask.CurrentStepId).FirstOrDefault();
            if (oldStep != null && oldStep.Roles != user.RoleName)
            {
                result.Status = "InvalidRole";
                _notif.SetFailed("Gagal", "Approval di form ini sudah terupdate sebelumnya. Mohon untuk refresh ulang halaman Anda.");
                return result;
            }

            if (dataInput.Note == "")
            {
                result.Content = "Mohon masukkan komentar sebelum melakukan send back.";
                result.Status = "NoComment";
                return result;
            }
            else
            {
                SetNotif _notif = new SetNotif(HttpContext);
                

                approvalTask.CurrentPICId = user.Id;
                approvalTask.UpdatedAt = DateTime.UtcNow.AddHours(7);

                _context.ApprovalTask.Update(approvalTask);
                if (_context.SaveChanges() > 0)
                {
                    string newData = _stepApproval.setSerialize(approvalTask);
                    string oldData = _stepApproval.setSerialize(oldApprovalTask);

                    if (dataInput.Note != "")
                    {
                        var CommentPost = new ApprovalComment()
                        {
                            ApprovalTaskId = approvalTask.Id,
                            UserId = user.Id,
                            Comment = dataInput.Note
                        };

                        _context.ApprovalComment.Add(CommentPost);
                        _context.SaveChanges();

                        _log.SetLogApproval(approvalTask, user, "Send Back Proposal From " + user.RoleName, newData, oldData, dataInput.Note);
                    }
                    else
                    {
                        _log.SetLogApproval(approvalTask, user, "Send Back Proposal From " + user.RoleName, newData, oldData);
                    }

                    var nextStep = _stepApproval.Read(dataInput.ApprovalTaskId, null, "OnRejected");
                    if (nextStep == null)
                    {
                        //RevertApprovalTaskSet(oldApprovalJson, dataInput, null, null, "BDF");
                        result.Content = "Terjadi kesalahan. Mohon ulangi lagi.";
                        _notif.SetFailed("Gagal", result.Content);
                        result.Status = "Failed";
                    }
                    else
                    {
                        _notif.SetSucced("Sukses", "Berhasil melakukan send back");
                        result.Content = "Selamat.";
                        result.Status = "Success";
                        var url = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host}{HttpContext.Request.PathBase}" + "/Home/FormProposal/" + approvalTask.ClaimNumber;
                        _sendNotif.sendSendBackSFMNotif(oldApprovalTask, url);
                    }
                }
                else
                {
                    //RevertApprovalTaskSet(oldApprovalJson, dataInput, null, null, "BDF");
                    result.Content = "Terjadi kesalahan. Mohon ulangi lagi.";
                    _notif.SetFailed("Gagal", result.Content);
                    result.Status = "Failed";
                }
                return result;
            }
        }

        public DateTime GetDateInCell(object value)
        {
            string DateMin = "01.01.2018";
            if (value is DateTime)
            {
                return (DateTime)value;
            }
            string date;
            try
            {
                date = value.ToString();
            }
            catch (Exception)
            {
                date = String.Empty;
            }
            DateTime dateTime;
            DateTime min = DateTime.Parse("01/01/1900");
            DateTime result = min;
            if (String.IsNullOrEmpty(date))
            {
                if (DateTime.TryParseExact(DateMin, "dd.MM.yyyy", new CultureInfo("id-ID"), DateTimeStyles.None, out dateTime))
                {
                    result = dateTime;
                }
            }
            else
            {
                if (!date.ToString().Contains('.'))
                {
                    if (!date.ToString().Contains('/'))
                    {
                        if (Convert.ToInt32(date.ToString()) >= 1000)
                        {
                            int addDay = Convert.ToInt32(date.ToString()) - 2;
                            result = min.AddDays(addDay);
                        }
                    }
                    else
                    {
                        if (DateTime.TryParseExact(date, "dd/MM/yyyy", new CultureInfo("id-ID"), DateTimeStyles.None, out dateTime))
                        {
                            result = dateTime;
                        }
                    }
                }
                else if (DateTime.TryParseExact(String.IsNullOrEmpty(date) ? DateMin : date, "dd.MM.yyyy", new CultureInfo("id-ID"), DateTimeStyles.None, out dateTime))
                {
                    result = dateTime;
                }
            }
            return result;
        }

        public bool IsTemplateValid(Dictionary<int, string> column)
        {
            bool isvalid = false;
            DocumentUploadTemplates templates = new DocumentUploadTemplates();
            var template = templates.UploadEntitlementSFM;

            if (template.SequenceEqual(column))
            {
                isvalid = true;
            }

            return isvalid;
        }

        public int EClaimNumber()
        {
            int ClaimNumber = 0;
            var dateEclaim = DateTime.Now.ToString("yyMM");
            if (_context.ApprovalTask.AsNoTracking().Any(x => x.ClaimNumber.Value.ToString().Substring(0, 4) == dateEclaim))
            {
                var LastClaimNumber = _context.ApprovalTask.AsNoTracking().Where(x => x.ClaimNumber.Value.ToString().Substring(0, 4) == dateEclaim)
                    .OrderByDescending(x => x.ClaimNumber).Select(x => x.ClaimNumber).FirstOrDefault();
                ClaimNumber = LastClaimNumber.Value + 1;
            }
            else
            {
                var number = DateTime.Now.ToString("yyMM") + "001";
                ClaimNumber = Convert.ToInt32(number);
            }
            return ClaimNumber;
        }

        public async Task<string> ResetSFMData()
        {
            var result = string.Empty;
            var UserId = await _context.Users.Where(a => a.IsSFMUser == true).Select(a => a.Id).ToListAsync();

            var approvalData = _context.ApprovalTask.Where(a => a.Title == "SFM Claim" || a.Title == "SFM Proposal").ToList();
            var approvalDataId = approvalData.Select(a => a.Id).ToList();

            var ApprovalComment = _context.ApprovalComment.Where(a => approvalDataId.Any(x => x == a.ApprovalTaskId)).ToList();
            _context.ApprovalComment.RemoveRange(ApprovalComment);
            await _context.SaveChangesAsync();

            var HistoryLog = _context.HistoryLog.Where(a => approvalDataId.Any(x => x == a.ApprovalTaskId)).ToList();
            _context.HistoryLog.RemoveRange(HistoryLog);
            await _context.SaveChangesAsync();

            var Proposal = _context.Proposal.ToList();
            _context.Proposal.RemoveRange(Proposal);
            await _context.SaveChangesAsync();

            var SFMEntitlement = _context.SFMEntitlement.ToList();
            _context.SFMEntitlement.RemoveRange(SFMEntitlement);
            await _context.SaveChangesAsync();

            _context.ApprovalTask.RemoveRange(approvalData);
            await _context.SaveChangesAsync();

            var ProgramSFMDistributor = _context.ProgramSFMDistributor.ToList();
            _context.ProgramSFMDistributor.RemoveRange(ProgramSFMDistributor);
            await _context.SaveChangesAsync();

            var Program = _context.SFMProgram.ToList();
            _context.SFMProgram.RemoveRange(Program);
            await _context.SaveChangesAsync();

            var ApplicationNotif = _context.NotificationApplicationUser.Where(a => UserId.Any(x => x == a.UserId)).ToList();
            _context.NotificationApplicationUser.RemoveRange(ApplicationNotif);
            await _context.SaveChangesAsync();

            return "Sukses Reset Data";
        }

        public async Task<string> ResetNotifSFMData()
        {
            var result = string.Empty;
            var UserId = await _context.Users.Where(a => a.IsSFMUser == true).Select(a => a.Id).ToListAsync();

            var ApplicationNotif = _context.NotificationApplicationUser.Where(a => UserId.Any(x => x == a.UserId)).ToList();
            _context.NotificationApplicationUser.RemoveRange(ApplicationNotif);
            await _context.SaveChangesAsync();

            return "Sukses Reset Data";
        }
    }
}