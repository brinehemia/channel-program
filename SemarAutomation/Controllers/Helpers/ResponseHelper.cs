﻿using Microsoft.AspNetCore.Mvc.Infrastructure;
using SemarAutomation.Models.ViewModels;

namespace SemarAutomation.Controllers.Helpers
{
    public static class ResponseHelper
    {
        public static GlobalResponse Ok([ActionResultObjectValue] object data)
        {
            var result = new GlobalResponse()
            {
                Data = data,
                StatusCode = 200,
                Success = true
            };
            
            return result;
        }

        public static GlobalResponse Ok()
        {
            var result = new GlobalResponse()
            {
                Data = null,
                StatusCode = 200,
                Success = true
            };

            return result;
        }

        public static GlobalResponse Error(int statusCode, string message)
        {
            var result = new GlobalResponse()
            {
                Data = null,
                Message = message,
                StatusCode = statusCode,
                Success = false,
            };
            return result;
        }
    }
}
