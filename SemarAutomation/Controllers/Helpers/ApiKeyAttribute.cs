﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace SemarAutomation.Controllers.Helpers
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    internal class ApiKeyAttribute : Attribute, IAsyncActionFilter
    {
        private const string ApiKeyHeaderName = "Semar-ApiKey";
        private string ApiKeyHeaderValue = "d0f9825bb02d4b76bf78510c31972f02";

        //private string[] WebApp = { "AuthController", "" };

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            //var ControllerName = context.Controller.ToString().Substring(context.Controller.ToString().LastIndexOf("Controllers.") + 0).Replace("Controllers.", "");
            //if (WebApp.Contains(ControllerName)) ApiKeyHeaderValue = "488a05dd61d0412397e8ff0a8fa499d52fe89be1";

            if (!context.HttpContext.Request.Headers.TryGetValue(ApiKeyHeaderName, out var potentialApiKey))
            {
                context.Result = new UnauthorizedResult();
                return;
            }

            var apiKey = ApiKeyHeaderValue;

            if (!apiKey.Equals(potentialApiKey))
            {
                context.Result = new UnauthorizedResult();
                return;
            }

            await next();
        }

    }
}
