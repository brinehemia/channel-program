using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;
using SemarAutomation.Controllers.Services;
using SemarAutomation.Data;
using SemarAutomation.Models;
using SemarAutomation.Models.ViewModels;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using UAParser;

namespace SemarAutomation.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class LoginModel : PageModel
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<LoginModel> _logger;
        private ApplicationDbContext _context;
        private IPostResponseService _postResponse;
        private IGetResponseService _getResponse;

        public LoginModel(
            SignInManager<ApplicationUser> signInManager,
            UserManager<ApplicationUser> userManager,
            ILogger<LoginModel> logger,
            ApplicationDbContext context,
            IHttpContextAccessor httpContextAccessor,
            IPostResponseService postResponse,
            IGetResponseService getResponse)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _logger = logger;
            _context = context;
            _httpContextAccessor = httpContextAccessor;
            _postResponse = postResponse;
            _getResponse = getResponse;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public string ReturnUrl { get; set; }

        [TempData]
        public string ErrorMessage { get; set; }

        public class InputModel
        {
            [Required]
            public string Email { get; set; }

            [Required]
            [DataType(DataType.Password)]
            public string Password { get; set; }

            [Display(Name = "Remember me?")]
            public bool RememberMe { get; set; }
        }

        public async Task<ActionResult> OnGetAsync(string returnUrl = null)
        {
            if (User.Identity.IsAuthenticated)
            {
                var logged = _userManager.GetUserAsync(HttpContext.User).Result;
                return RedirectToAction("Index", "SFM");
            }

            if (!string.IsNullOrEmpty(ErrorMessage))
            {
                ModelState.AddModelError(string.Empty, ErrorMessage);
            }

            returnUrl = returnUrl ?? Url.Content("~/");

            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();

            ReturnUrl = returnUrl;
            return Page();
        }

        public void CreateLog(string message, string exception)
        {
            using (StreamWriter sw = new StreamWriter("Logs/Eclaim-Log-Report-" + DateTime.Now.Date.ToString("dd-mm-yyyy") + ".txt", true))
            {
                sw.WriteLine(DateTime.Now + ": Exception occured. Message: " + message + ". Exception Details: " + exception);
            }
            System.Diagnostics.Debug.WriteLine("[{0}]: Exception occured. Message: '{1}'. Exception Details:\r\n{2}",
                DateTime.Now.Date.ToString("dd-mm-yyyy"), message, exception);
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            try
            {
                if (ModelState.IsValid)
                {
                    var userName = Input.Email;
                    ApplicationUser user;

                    user = await _userManager.FindByEmailAsync(Input.Email);

                    //validasi SFM User
                    if (user == null || user.IsSFMUser == true)
                    {
                        var responseContent = new ResponseLoginSFM() { };

                        var data = new { email = Input.Email, password = Input.Password };
                        var requestUser = await _postResponse.PostResponse("login", JsonConvert.SerializeObject(data));
                        if ((int)requestUser.StatusCode == 200)
                        {
                            responseContent = JsonConvert.DeserializeObject<ResponseLoginSFM>(requestUser.Content);

                            var responseContentDetail = new ReponseDetailUser() { };
                            var requestUserDetail = await _getResponse.GetResponseWithAuth("detail", responseContent.data.Token);
                            if ((int)requestUserDetail.StatusCode == 200) responseContentDetail = JsonConvert.DeserializeObject<ReponseDetailUser>(requestUserDetail.Content);
                            else throw new Exception("User SFM Not Found");

                            if (user == null)
                            {
                                var role = responseContentDetail.user.role;
                                if (responseContentDetail.user.role.ToLower() == "om") role = "Distributor";
                                else if (responseContentDetail.user.role.ToLower() == "sales") role = "ASM";
                                else if (responseContentDetail.user.role.ToLower() == "channel-marketing") role = "Marketing";
                                else if (responseContentDetail.user.role.ToLower() == "legal") role = "Legal";
                                else if (responseContentDetail.user.role.ToLower() == "commercial-lead") role = "Channel Lead";
                                else if (responseContentDetail.user.role.ToLower() == "finance") role = "Finance";
                                else if (responseContentDetail.user.role.ToLower() == "editor") role = "Guest";
                                else if (responseContentDetail.user.role.ToLower() == "product-manager") role = "Guest";

                                var insert = new ApplicationUser
                                {
                                    UserName = responseContentDetail.user.email,
                                    Email = responseContentDetail.user.email,
                                    Name = responseContentDetail.user.name,
                                    Channel = "Prof",
                                    PhoneNumber = responseContentDetail.user.phone_number,
                                    RoleName = role,
                                    AccountDistributor = role == "Distributor" ? responseContentDetail.user.sold_to.FirstOrDefault() : string.Empty,
                                    OtherEmail = string.Empty,
                                    CompanyDistributor = !string.IsNullOrEmpty(responseContentDetail.user.distributor_company) ? responseContentDetail.user.distributor_company : string.Empty,
                                    IsSFMUser = true,
                                    UpdatedAt = DateTime.UtcNow.AddHours(7),
                                    CreatedAt = DateTime.UtcNow.AddHours(7)
                                };

                                await _userManager.CreateAsync(insert, "f6036D6!80caf44d");
                                user = await _userManager.FindByNameAsync(responseContentDetail.user.email);
                            }
                            else
                            {
                                var isUpdate = false;
                                if (user.CompanyDistributor != responseContentDetail.user.distributor_company) { user.CompanyDistributor = responseContentDetail.user.distributor_company; isUpdate = true; }
                                if (user.Name != responseContentDetail.user.name) { user.Name = responseContentDetail.user.name; isUpdate = true; }
                                if (user.PhoneNumber != responseContentDetail.user.phone_number) { user.PhoneNumber = responseContentDetail.user.phone_number; isUpdate = true; }

                                if (isUpdate) await _userManager.UpdateAsync(user);
                            }

                            var userClaim = await _userManager.GetClaimsAsync(user);
                            if (userClaim.Count > 0)
                            {
                                var DataUserClaim = _context.UserClaims.Where(a => a.UserId == user.Id);
                                _context.UserClaims.RemoveRange(DataUserClaim);
                                _context.SaveChanges();
                            }

                            var claims = new List<Claim>
                            {
                                new Claim("email", user.Email),
                                new Claim("username", user.Name),
                                new Claim("token", responseContent.data.Token),
                                new Claim(ClaimTypes.Role, user.RoleName)
                            };
                            await _userManager.AddClaimsAsync(user, claims);

                            Input.Password = "f6036D6!80caf44d";
                        }
                    }

                    if (user == null)
                    {
                        var notfinded = true;
                        user = await _userManager.FindByNameAsync(userName);
                        if (user == null)
                        {
                            ModelState.AddModelError(string.Empty, "Username/Email tidak ditemukan.");
                            return Page();
                        }
                        else
                        {
                            notfinded = false;
                        }

                        if (notfinded)
                        {
                            ModelState.AddModelError(string.Empty, "Username/Email tidak ditemukan.");
                            return Page();
                        }
                    }

                    if (user.LockoutEnd != null && user.LockoutEnd != DateTime.MinValue)
                    {
                        ModelState.AddModelError(string.Empty, "Username/Email terkunci.");
                        return Page();
                    }

                    var result = await _signInManager.PasswordSignInAsync(user.UserName, Input.Password, Input.RememberMe, lockoutOnFailure: false);

                    if (result.Succeeded)
                    {
                        if (Input.RememberMe)
                        {
                            CookieOptions option = new CookieOptions();
                            option.Expires = DateTime.Now.AddHours(24);
                            option.HttpOnly = true;
                            option.IsEssential = true;
                            Response.Cookies.Append("user", Input.Email, option);
                            //HttpContext.Request.Cookies["user"].Append(Input.Email);
                        }

                        CreateLog("User logged in.", "");
                        _logger.LogInformation("User logged in.");

                        // User Claims
                        var DataUserClaim = _context.UserClaims;

                        try
                        {
                            var data = new UserLoginLog();
                            data.IP = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                            data.TimeLogin = DateTime.Now;
                            data.UserName = user.UserName;
                            var useragent = Request.Headers["User-Agent"].ToString();

                            var uaParser = Parser.GetDefault();

                            ClientInfo c = uaParser.Parse(useragent);
                            data.Detail = "Browser : " + c.UA.Family + " ver " + c.UA.Major + " | OS : " + c.OS.Family + " " + c.OS.Major;

                            _context.UserLoginLog.Add(data);
                            _context.SaveChanges();
                        }

                        catch (Exception e)
                        {
                            var err = e.Message;
                            CreateLog(e.Message, e.Source);
                        }

                        returnUrl = Url.Content("~/Home");

                        return LocalRedirect(returnUrl);
                    }

                    if (result.RequiresTwoFactor)
                    {
                        return RedirectToPage("./LoginWith2fa", new { ReturnUrl = returnUrl, RememberMe = Input.RememberMe });
                    }
                    if (result.IsLockedOut)
                    {
                        ModelState.AddModelError(string.Empty, "User Tidak Ditemukan.");
                        return Page();
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Invalid password.");
                        return Page();
                    }

                }
                else
                {
                    CreateLog(ModelState.Values.ToString(), ModelState.ErrorCount.ToString());
                }

            }
            catch (Exception e)
            {
                var err = e.Message;
                CreateLog(e.Message, e.Source);
            }

            return Page();
        }
    }
}
