using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SemarAutomation.Controllers.Services;
using SemarAutomation.Data;
using SemarAutomation.Models;
using System.ComponentModel.DataAnnotations;

namespace SemarAutomation.Areas.Identity.Pages.Account.Manage
{
    public class IndexModel : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private ApplicationDbContext _context;
        private IPutResponseService _putResponse;

        public IndexModel(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            IPutResponseService putResponse,
            ApplicationDbContext context)

        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _putResponse = putResponse;
        }

        public string Username { get; set; }

        public bool IsEmailConfirmed { get; set; }

        [TempData]
        public string StatusMessage { get; set; }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "Password sekarang")]
            public string OldPassword { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "Password Harus Mengandung unsur Huruf Kapital, Angka dan dengan panjang minimum 6 karakter", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password baru")]
            public string NewPassword { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Konfirmasi password baru")]
            [Compare("NewPassword", ErrorMessage = "Konfirmasi password baru tidak sama dengan password baru.")]
            public string ConfirmPassword { get; set; }

            public string Id { get; set; }
            public long Account { get; set; }
            [Required]
            public string Nama { get; set; }

            [Required]
            [EmailAddress]
            [DataType(DataType.EmailAddress)]
            public string Email { get; set; }
            public string Telp { get; set; }
            public string Channel { get; set; }
            public string role { get; set; }
            public bool SetAdmin { get; set; }
            public bool IsSFM { get; set; }
            public string Status { get; set; }
            public string CompanyDistributor { get; set; }

        }

        public async Task<IActionResult> OnGetAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var getuser = _userManager.GetUserAsync(HttpContext.User).Result;
            int getmultiple = _context.UserRoles.Where(a => a.RoleId == "b2b1c508-7843-4472-a033-23f277bc48be" && a.UserId == getuser.Id).Count();

            Input = new InputModel()
            {
                Id = getuser.Id,
                Account = getuser.RoleName == "Distributor" ? Convert.ToInt64(getuser.AccountDistributor) : 0,
                Channel = getuser.Channel,
                Email = getuser.Email,
                Nama = getuser.Name,
                role = getuser.RoleName,
                Telp = getuser.PhoneNumber,
                IsSFM = getuser.IsSFMUser,
                CompanyDistributor = getuser.CompanyDistributor
            };

            if (getmultiple > 0)
            {
                Input.SetAdmin = true;
            }
            else
            {
                Input.SetAdmin = false;
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            SetNotif _notif = new SetNotif(HttpContext);
            var getAllUser = _context.Users;
            if (!ModelState.IsValid)
            {
                if (Input.Status == "editprofile")
                {
                    ModelState.Remove("OldPassword");
                    ModelState.Remove("NewPassword");
                    ModelState.Remove("ConfirmPassword");
                }
                else if (Input.Status == "editpassword")
                {
                    ModelState.Remove("Id");
                    ModelState.Remove("Nama");
                    ModelState.Remove("Email");
                    ModelState.Remove("Telp");
                }
                else
                {
                    return Page();
                }
            }

            if (Input.Status == "editprofile")
            {
                if (Input.Telp != null)
                {
                    if (!Input.Telp.StartsWith("0"))
                    {
                        _notif.SetFailed("Gagal", "Mohon menginput nomor telp dengan awalan '0' contoh 0822233333");
                        Input = Input;
                        return RedirectToPage(Input);
                    }
                }

                var getsameemail = getAllUser.Where(a => a.Email == Input.Email && a.Id != Input.Id).Count();
                if (getsameemail > 0)
                {
                    _notif.SetFailed("Gagal", "Email yang Anda inputkan sudah tersedia, Mohon untuk menggunakan Email lain");
                    return RedirectToPage(Input);
                }
                if (Input.Email != null)
                {
                    if (Input.Email.StartsWith(" ") || Input.Nama.StartsWith(" "))
                    {
                        _notif.SetFailed("Gagal", "Mohon tidak mengawali Inputan Menggunakan Spasi");
                        return RedirectToPage(Input);
                    }

                    if (!Input.Email.Substring(Input.Email.IndexOf("@")).Contains("."))
                    {
                        _notif.SetFailed("Gagal", "Email Anda tidak valid, Mohon Menambahkan '.DomainEmailAnda' setelah @, contoh alamatemail@gmail.com");
                        return RedirectToPage(Input);
                    }
                }
                else
                {
                    _notif.SetFailed("Gagal", "Masukkan email anda");
                    return RedirectToPage(Input);
                }

                var token = string.Empty;
                var getuser = _context.Users.AsNoTracking().Where(a => a.Id == Input.Id).FirstOrDefault();
                getuser.Email = Input.Email;
                getuser.UserName = Input.Email;
                getuser.NormalizedEmail = Input.Email.ToUpper();
                getuser.NormalizedUserName = Input.Email.ToUpper();
                getuser.Name = Input.Nama;
                getuser.PhoneNumber = Input.Telp;

                _context.Users.Update(getuser);

                int result = _context.SaveChanges();
                if (result == 0)
                {
                    _notif.SetFailed("Gagal", "Update Date Gagal, Mohon untuk Mengulang");
                    Input = Input;
                    return RedirectToPage(Input);
                }
                else
                {
                    if (getuser.IsSFMUser)
                    {
                        token = HttpContext.User.Claims.First(a => a.Type == "token").Value;

                        var obj = new
                        {
                            name = Input.Nama,
                            email = Input.Email,
                            phone_number = Input.Telp
                        };
                        await _putResponse.PutResponseWithAuth("update_profile", token, JsonConvert.SerializeObject(obj));

                        var nameclaims = _context.UserClaims.Where(a => a.UserId == getuser.Id && a.ClaimType == "username").FirstOrDefault();
                        if (nameclaims != null)
                        {
                            nameclaims.ClaimValue = getuser.Name;
                            _context.UserClaims.Update(nameclaims);
                            _context.SaveChanges();
                        }

                        await _signInManager.RefreshSignInAsync(getuser);
                    }

                    _notif.SetSucced("Berhasil", "Data Sukses Terupdate");
                    return RedirectToPage();
                }

            }
            else if (Input.Status == "editpassword")
            {
                if (Input.NewPassword == null || Input.OldPassword == null)
                {
                    _notif.SetFailed("Gagal", "Mohon untuk mengisi Password");
                    return RedirectToPage(Input);
                }

                if (Input.NewPassword != Input.ConfirmPassword)
                {
                    _notif.SetFailed("Gagal", "Password yang Anda Masukkan tidak sama");
                    return RedirectToPage(Input);
                }

                if (!Input.NewPassword.Any(a => char.IsUpper(a)) || !Input.NewPassword.Any(a => char.IsDigit(a)) || !Input.NewPassword.Any(a => char.IsLower(a)) || !Input.NewPassword.Any(a => char.IsPunctuation(a)) || Input.NewPassword.Length < 6)
                {
                    _notif.SetFailed("Gagal", "Password baru yang Anda masukkan tidak sesuai dengan format yang ditentukan. Mohon untuk ubah sesuai dengan format yang ditentukan.");
                    return RedirectToPage(Input);
                }

                var user = await _userManager.GetUserAsync(User);
                if (user == null)
                {
                    return NotFound($"User dengan ID '{_userManager.GetUserId(User)}'.");
                }

                if (user.IsSFMUser)
                {
                    var obj = new
                    {
                        old_password = Input.OldPassword,
                        new_password = Input.NewPassword
                    };
                    var token = HttpContext.User.Claims.First(a => a.Type == "token").Value;
                    var result = await _putResponse.PutResponseWithAuth("update_profile", token, JsonConvert.SerializeObject(obj));
                    if ((int)result.StatusCode == 200)
                    {
                        _notif.SetSucced("Sukses", "Update Password berhasil");
                        return RedirectToPage();
                    }
                    else
                    {
                        _notif.SetFailed("Gagal", "Password lama yang Anda masukkan tidak sesuai dengan password yang terdaftar. Mohon untuk ubah sesuai dengan password yang terdaftar");
                        return RedirectToPage();
                    }
                }
                else
                {
                    var changePasswordResult = await _userManager.ChangePasswordAsync(user, Input.OldPassword, Input.NewPassword);
                    if (changePasswordResult.Succeeded)
                    {
                        foreach (var error in changePasswordResult.Errors)
                        {
                            ModelState.AddModelError(string.Empty, error.Description);
                        }
                        await _signInManager.RefreshSignInAsync(user);
                        _notif.SetSucced("Sukses", "Update Password berhasil");
                        return RedirectToPage();
                    }
                    else
                    {
                        _notif.SetFailed("Gagal", "Password lama yang Anda masukkan tidak sesuai dengan password yang terdaftar. Mohon untuk ubah sesuai dengan password yang terdaftar");
                        return RedirectToPage();
                    }
                }


            }
            else
            {
                return RedirectToPage();
            }
        }

        public async Task<IActionResult> PasswordPass()
        {
            SetNotif _notif = new SetNotif(HttpContext);
            if (Input.NewPassword == null || Input.OldPassword == null)
            {
                _notif.SetFailed("Gagal", "Mohon untuk mengisi Password");
                return RedirectToPage(Input);
            }

            if (Input.NewPassword != Input.ConfirmPassword)
            {
                _notif.SetFailed("Gagal", "Password yang Anda Masukkan tidak sama");
                return RedirectToPage(Input);
            }

            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"User dengan ID '{_userManager.GetUserId(User)}'.");
            }

            if (Input.NewPassword == null)
            {
                _notif.SetFailed("Gagal", "Mohon untuk menginputkan password ");
                return RedirectToPage(Input);
            }

            var changePasswordResult = await _userManager.ChangePasswordAsync(user, Input.OldPassword, Input.NewPassword);
            if (changePasswordResult.Succeeded)
            {
                foreach (var error in changePasswordResult.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
                await _signInManager.RefreshSignInAsync(user);
                _notif.SetSucced("Sukses", "Update Password berhasil");
                return RedirectToPage();
            }
            else
            {
                _notif.SetFailed("Gagal", "Update Password tidak berhasil ");
                return RedirectToPage();
            }

            //_logger.LogInformation("User changed their password successfully.");
            //_notif.SetSucced("Success", "Your password has been changed.");
            //return RedirectToPage("Index","Admin");
        }
    }
}
