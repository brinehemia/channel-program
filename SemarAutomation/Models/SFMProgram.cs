﻿using System.ComponentModel.DataAnnotations;

namespace SemarAutomation.Models
{
    public class SFMProgram
    {
        [Key]
        public int Id { get; set; }
        public string? ProgramName { get; set; }
        public string? PPR { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime DefaultExpDate { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public SFMProgram()
        {
            UpdatedAt = DateTime.UtcNow.AddHours(7);
        }
    }
}
