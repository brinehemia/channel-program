﻿using System.ComponentModel.DataAnnotations;

namespace SemarAutomation.Models
{
    public class SFMProposalType
    {
        [Key]
        public int Id { get; set; }
        public string? Name { get; set; }
    }
}
