﻿using System.ComponentModel.DataAnnotations;

namespace SemarAutomation.Models
{
    public class UserLoginLog
    {
        [Key]
        public int Id { get; set; }
        public string? IP { get; set; }
        public string? UserName { get; set; }
        public DateTime TimeLogin { get; set; }
        public string? Detail { get; set; }
    }
}
