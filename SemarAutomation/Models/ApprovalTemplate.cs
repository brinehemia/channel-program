﻿using System.ComponentModel.DataAnnotations;

namespace SemarAutomation.Models
{
    public class ApprovalTemplate
    {
        [Key]
        public string Id { get; set; }
        public string? Title { get; set; }
        public string? AllowedRoles { get; set; }
        public bool IsEveryoneAllowed { get; set; }
        public ApprovalTemplate()
        {
            Guid g = Guid.NewGuid();
            Id = g.ToString();
        }
    }
}
