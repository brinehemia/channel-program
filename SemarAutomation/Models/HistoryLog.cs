﻿using System.ComponentModel.DataAnnotations;

namespace SemarAutomation.Models
{
    public class HistoryLog
    {
        [Key]
        public int Id { get; set; }
        public string? ApprovalTaskId { get; set; }
        public string? UserId { get; set; }
        public string? Action { get; set; }
        public string? OldData { get; set; }
        public string? NewData { get; set; }
        public string? Comment { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
