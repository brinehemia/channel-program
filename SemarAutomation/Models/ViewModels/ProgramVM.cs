﻿namespace SemarAutomation.Models.ViewModels
{
    public class SFMProgramDTO
    {
        public int Id { get; set; }
        public string ProgramName { get; set; }
        public string PPR { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime DefaultExpDate { get; set; }
        public string IdDistributor { get; set; }
    }

    public class EditProgram
    {
        public int Id { get; set; }
        public string IdProgram { get; set; }
        public string IdDistributor { get; set; }
        public string ProgramName { get; set; }
        public string PPR { get; set; }
        public string TypeClaim { get; set; }
        public DateTime ExpiredDate { get; set; }
        public DateTime RangeStartDate { get; set; }
        public DateTime RangeEndDate { get; set; }
        public string StartDate { get; set; }
        public string DefaultExpDate { get; set; }
        public string ProgramPeriode { get; set; }
    }

}
