﻿namespace SemarAutomation.Models.ViewModels
{
    public class ActionViewModel
    {
        public string controller { get; set; }
        public string function { get; set; }
        public Parameter parameter { get; set; }
    }

    public class Parameter
    {
        public string approvalId { get; set; }
        public string UserId { get; set; }
        public string userName { get; set; }
        public string title { get; set; }
        public string sendToRole { get; set; }
        public string SendFromRole { get; set; }
    }

    public class DecisionViewModel
    {
        public string Condition { get; set; }
    }
}
