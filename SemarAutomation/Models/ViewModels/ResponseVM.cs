﻿namespace SemarAutomation.Models.ViewModels
{
    public class ResponseSoldTo
    {
        public List<string> data { get; set; }
    }

    public class GlobalResponse
    {
        public object? Data { get; set; }
        public string Message { get; set; }   
        public int StatusCode { get; set; }
        public bool Success { get; set; }
    }

    public class ResponseClaims
    {
        public string TipeAktivitas { get; set; }
        public int Total { get; set; }
    }
}
