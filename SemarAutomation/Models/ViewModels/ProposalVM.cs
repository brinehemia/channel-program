﻿using System.ComponentModel.DataAnnotations;

namespace SemarAutomation.Models.ViewModels
{
    public class ProposalDTO
    {
        public int Id { get; set; }
        public int DraftId { get; set; }
        public string Kategori { get; set; }
        public string ApprovalTaskId { get; set; }
        public int ProposalNumber { get; set; }
        public string AccountNumber { get; set; }
        public string TipeAktivitas { get; set; }
        public string NamaAktivitas { get; set; }
        public string SegmentPasar { get; set; }
        public string SubSegmentPasar { get; set; }
        public DateTime TanggalAktivitas { get; set; }
        public string SumberBudget { get; set; }
        public string Opportunity { get; set; }
        public int Opportunityint { get; set; }
        public string KPI { get; set; }
        public long Nominal { get; set; }
        public IFormFileCollection UploadedFile { get; set; }
        public string Status { get; set; }
        public string ProposalAktivitas { get; set; }
        public string QuotationVendor { get; set; }
        public string ListFilename { get; set; }
    }

    public class ProposalList
    {
        [Key]
        public int Id { get; set; }
        public int ProposalNumber { get; set; }
        public string ApprovalTaskId { get; set; }
        public string AccountNumber { get; set; }
        public string DistributorName { get; set; }
        public string Kategori { get; set; }
        public string TipeAktivitas { get; set; }
        public string NamaAktivitas { get; set; }
        public string SegmentPasar { get; set; }
        public DateTime TanggalAktivitas { get; set; }
        public string SumberBudget { get; set; }
        public string ProgramName { get; set; }
        public string Opportunity { get; set; }
        public string KPI { get; set; }
        public long Nominal { get; set; }
        public long NominalBalance { get; set; }
        public long ActualAmount { get; set; }
        public string Status { get; set; }
        public string StatusApproval { get; set; }
        public string Style { get; set; }
        public string Color { get; set; }
        public string TextButton { get; set; }
        public string Roles { get; set; }
        public DateTime ExpiredDate { get; set; }
        public string ExpiredDateView { get; set; }
        public string isExpired { get; set; }
        public DateTime CreatedAt { get; set; }
    }

    public class ListProposalFile 
    {
        public string FullName { get; set; }
        public string FieldName { get; set; }
        public List<ProposalFile> ProposalFile { get; set; }

    }

    public class ProposalFile
    {
        public int Id { get; set; }
        public string Fieldname { get; set; }
        public string Filename { get; set; }
        public string Url { get; set; }
    }

    public class UploadedFilename
    {
        public string fieldname { get; set; }
        public List<string> listFilename { get; set; }
    }

    public class ProposalKPI
    {
        public string Id { get; set; }
        public int Order { get; set; }
        public string Name { get; set; }
    }
}