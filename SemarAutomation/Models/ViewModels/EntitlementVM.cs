﻿using System.ComponentModel.DataAnnotations;

namespace SemarAutomation.Models.ViewModels
{
    public class SFMEntitlementDTO
    {
        [Key]
        public int Id { get; set; }
        public string Account { get; set; }
        public string DistributorName { get; set; }
        public string ProgramName { get; set; }
        public DateTime ExpiredDate { get; set; }
        public string ExpiredDateView { get; set; }
        public long EntitlementValue { get; set; }
        public long EntitlementBalance { get; set; }
        public string PPR { get; set; }
        public string Remark { get; set; }
        public bool IsRejected { get; set; }
        public int LastClaimNumber { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string Style { get; set; }
    }

    public class DetailSFMEntitlement
    {
        [Key]
        public int Id { get; set; }
        public string DistributorAccount { get; set; }
        public string ProgramName { get; set; }

        public int EntitlementId { get; set; }

        public string Type { get; set; }
        public long Value { get; set; }
        public long ValueTotal { get; set; }
        public string Remark { get; set; }
        public int EntitlementRemarkId { get; set; }
        public string ApprovalId { get; set; }
        public bool IsOld { get; set; }
        public DateTime Date { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }

    public class EditEntitlementViewModel
    {
        public int Id { get; set; }
        public long Entitlement { get; set; }
        public long Balance { get; set; }
        public DateTime ExpiredDate { get; set; }
        public DateTime RangeStart { get; set; }
        public DateTime RangeEnd { get; set; }
    }
}
