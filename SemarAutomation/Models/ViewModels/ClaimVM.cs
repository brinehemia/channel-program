﻿namespace SemarAutomation.Models.ViewModels
{
    public class SFMFormClaimViewModel
    {
        public ApprovalTask ApprovalTask { get; set; }
        public StepBase CurrentStep { get; set; }
        public ApplicationUser User { get; set; }
        public List<ApprovalCommentVM> ApprovalComment { get; set; }
        public SFMClaimDTO SFMClaimData { get; set; }
        public SFMProposalDetail SFMClaimDTO { get; set; }
        public Proposal Proposal { get; set; }
        public int ClaimNumber { get; set; }
        public long BalanceTemp { get; set; }
        public bool isReadDocument { get; set; }
        public string DistributorName { get; set; }
        public string OpportunityValue { get; set; }
        public string ActualAmount { get; set; }
        public int[] ValueSumberBudget { get; set; }
        public List<ProposalKPI> ValueKPI { get; set; }
        public List<ListProposalFile> UploadedFile { get; set; }
        public List<ProposalFile> ProposalFile { get; set; }
        public List<OpportunityData> OpportunityList { get; set; }
        public List<TypeFileData> TypeFileData { get; set; }
        public DescriptionFile DescriptionFile { get; set; }
        public List<string> SegmenList { get; set; }
        public List<string> SubSegmenList { get; set; }
        public int DraftId { get; set; }

        public bool IsRebateBooster { get; set; }
    }

    public class ClaimDTO
    {
        public string Id { get; set; }
        public string ApprovalTaskId { get; set; }
        public int ClaimNumber { get; set; }
        public int ProposalNumber { get; set; }
        public string Hasil { get; set; }
        public long Nominal { get; set; }
        public string AccountNumber { get; set; }
        public string Invoice { get; set; }
        public DateTime InvoiceDate { get; set; }
        public IFormFileCollection UploadedFile { get; set; }
        public string Status { get; set; }
        public string InvoicePeserta { get; set; }
        public string InvoiceVendor { get; set; }
        public string LaporanAktivitas { get; set; }
        public string ListFilename { get; set; }
    }

    public class ClaimList
    {
        public string AccountNumber { get; set; }
        public string DistributorName { get; set; }
        public int ClaimNumber { get; set; }
        public DateTime ClaimDate { get; set; }
        public string NamaAktivitas { get; set; }
        public long NominalClaim { get; set; }
        public DateTime LastUpdate { get; set; }
        public string Data { get; set; }
        public string StatusApproval { get; set; }
        public string Status { get; set; }
        public string Style { get; set; }
        public string Color { get; set; }
        public string TextButton { get; set; }
        public string Roles { get; set; }
    }

    public class ClaimCounter
    {
        public string ApprovalId { get; set; }
        public string TipeAktivitas { get; set; }
        public DateTime ClaimDate { get; set; }
    }

}
