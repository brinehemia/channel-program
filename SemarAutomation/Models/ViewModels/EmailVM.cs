﻿namespace SemarAutomation.Models.ViewModels
{
    public class SendEmailViewModel
    {
        //sendto
        public ApplicationUser User { get; set; }
        //content
        public ApplicationUser User2 { get; set; }
        public string Subject { get; set; }
        public string Link { get; set; }
        public string LinkAttachment { get; set; }
        public string AttachmentName { get; set; }
        public string RootLogo { get; set; }
        public string Email { get; set; }
        public string EmailBantuan { get; set; }
        public long EntitlementValue { get; set; }
        public string NamaProgram { get; set; }
        public DateTime ExpDateClaim { get; set; }
        public DateTime RangeStartDate { get; set; }
        public DateTime RangeEndDate { get; set; }
        public int NomorClaim { get; set; }
        public string TypeClaim { get; set; }
        public string DistributorName { get; set; }
        public string Channel { get; set; }
        public string CurrentPIC { get; set; }
        public string CurrentPICRole { get; set; }
        public string Title { get; set; }
        public string Territory { get; set; }
        public string CCApproverSendback { get; set; }
        public string ApprovalTaskId { get; set; }
        public string CatatanSendback { get; set; }
        public string CcSendback { get; set; }
        public string CurrentRole { get; set; }
    }

    public class EmailSettings
    {
        public string Email { get; set; }
        public string Pass { get; set; }
    }

    public class UploadSendEmailViewModel
    {
        public int Id { get; set; }
        public string Account { get; set; }
        public ApplicationUser User { get; set; }
        public string ProgramName { get; set; }
        public DateTime ExpiredDate { get; set; }
        public DateTime RangeStartDate { get; set; }
        public DateTime RangeEndDate { get; set; }
        public long EntitlementValue { get; set; }
        public long EntitlementBalance { get; set; }
        public string PPR { get; set; }
        public string Remark { get; set; }
        public bool IsOld { get; set; }
        public int LastClaimNumber { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public SendEmailViewModel SendEmail { get; set; }
    }
}
