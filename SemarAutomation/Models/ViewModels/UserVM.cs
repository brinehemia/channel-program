﻿namespace SemarAutomation.Models.ViewModels
{
    public class UserSFM
    {
        public int id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public DateTime email_verified_at { get; set; }
        public string role { get; set; }
        public string platform { get; set; }
        public string phone_number { get; set; }
        public string distributor_company { get; set; }
        public List<string> sold_to { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }

    public class ReponseDetailUser
    {
        public UserSFM user { get; set; }
    }

    public class UserSFMList
    {
        public int id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public DateTime email_verified_at { get; set; }
        public string role { get; set; }
        public string platform { get; set; }
        public string phone_number { get; set; }
        public string sold_to { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }

    public class ReponseListUser
    {
        public List<UserSFMList> data { get; set; }
    }

    public class ResponseLoginSFM
    {
        public string success { get; set; }
        public string message { get; set; }
        public TokenSFM data { get; set; }
    }

    public class TokenSFM
    {
        public string Token { get; set; }
    }
}
