﻿namespace SemarAutomation.Models.ViewModels
{
    public class DocumentUploadTemplates
    {
        public Dictionary<int, string> UploadEntitlementSFM { get; set; }

        public DocumentUploadTemplates()
        {
            UploadEntitlementSFM = new Dictionary<int, string>()
            {
                { 1, "no distributor account" },
                { 2, "program name" },
                { 3, "expired date" },
                { 4, "entitlement" },
                { 5, "ppr" },
                { 6, "remark" }
            };
        }
    }
}
