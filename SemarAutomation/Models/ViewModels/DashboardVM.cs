﻿namespace SemarAutomation.Models.ViewModels
{
    public class DashBoardSFMViewModel
    {
        public string OnprocessProposal { get; set; }
        public int OnprocessProposalCount { get; set; }

        public string OnprocessClaim { get; set; }
        public int OnprocessClaimCount { get; set; }

        public string CloseClaim { get; set; }
        public int CloseClaimCount { get; set; }

        public string CloseProposal { get; set; }
        public int CloseProposalCount { get; set; }

        public string CancelProposal { get; set; }
        public int CancelProposalCount { get; set; }

        public string ExpDateEntitlement { get; set; }
        public int ExpDateEntitlementCount { get; set; }
    }

    public class ChartSFMViewModel
    {
        public string Roles { get; set; }
        public long Value { get; set; }
        public int Count { get; set; }
    }
}
