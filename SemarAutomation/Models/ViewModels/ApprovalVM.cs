﻿namespace SemarAutomation.Models.ViewModels
{
    public class SFMClaimDTO
    {
        public int ClaimNumber { get; set; }
        public string PRNumber { get; set; }
        public string Invoice { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string NamaAktivitas { get; set; }
        public string UserId { get; set; }
        public List<HasilValue> Hasil { get; set; }
        public string ApprovalTaskId { get; set; }
        public int ProposalId { get; set; }
        public long Entitlement { get; set; }
        public long NominalClaim { get; set; }
        public DateTime ExpiryDate { get; set; }
        public DateTime ClaimDate { get; set; }
        public DateTime LastUpdate { get; set; }
        public long AmountFinance { get; set; }
        public string BillingDocNumber { get; set; }
        public long VATAmount { get; set; }
        public long WHTAmount { get; set; }
        public DateTime BillingDate { get; set; }
        public long BalanceFinal { get; set; }
        public string PPRNumber { get; set; }

        public string Note { get; set; }

        public string InvoicePeserta { get; set; }
        public string InvoiceVendor { get; set; }
        public string LaporanAktivitas { get; set; }


        public bool DocumentReceivedASM { get; set; }
        public DateTime DocumentReceivedDateASM { get; set; }

        public bool DocumentReceivedMarketing { get; set; }
        public DateTime DocumentReceivedDateMarketing { get; set; }

        public bool DocumentReceivedChannelLead { get; set; }
        public DateTime DocumentReceivedDateChannelLead { get; set; }

        public bool DocumentReceivedFinance { get; set; }
        public DateTime DocumentReceivedDateFinance { get; set; }

        public bool DocumentReceivedLegal { get; set; }
        public DateTime DocumentReceivedDateLegal { get; set; }

        public List<ProposalFile> UploadedFile { get; set; }

        public bool UploadedProcessed { get; set; }

        public bool NeedLegalApproval { get; set; }
        public bool IsNeedLegal { get; set; }
    }

    public class SFMProposalDetail
    {
        public int ProposalNumber { get; set; }
        public string Program { get; set; }
        public string UserId { get; set; }
        public string ApprovalTaskId { get; set; }
        public string EntitlementId { get; set; }
        public long Nominal { get; set; }
        public DateTime ExpiryDate { get; set; }
        public DateTime ProposalDate { get; set; }
        public string Note { get; set; }

        public List<DetailBalanceTaken> DetailBalanceTaken { get; set; }

        public bool DocumentReceivedASM { get; set; }
        public DateTime DocumentReceivedDateASM { get; set; }

        public bool DocumentReceivedMarketing { get; set; }
        public DateTime DocumentReceivedDateMarketing { get; set; }

        public bool DocumentReceivedLegal { get; set; }
        public DateTime DocumentReceivedDateLegal { get; set; }

        public bool NeedLegalApproval { get; set; }
        public bool IsNeedLegal { get; set; }
    }

    public class DetailBalanceTaken
    {
        public int EntitlementId { get; set; }
        public int Order { get; set; }
        public long Nominal { get; set; }
    }

    public class HasilValue
    {
        public string KPI { get; set; }
        public long Target { get; set; }
        public long Aktual { get; set; }
    }

    public class SFMDataEmail
    {
        public string SendToRole { get; set; }
        public string SendFromRole { get; set; }
    }

    public class SFMHistoryClaim
    {
        public int ClaimNumber { get; set; }
        public DateTime ClaimDate { get; set; }
        public string NamaAktivitas { get; set; }
        public long Nominal { get; set; }
    }

    public class SFMListResponse
    {
        public List<string> data { get; set; }
    }

    public class OpportunityList
    {
        public List<OpportunityData> data { get; set; }
    }

    public class OpportunityData
    {
        public string name { get; set; }
        public string sfdc_id { get; set; }
        public long amount { get; set; }
        public string amountString { get; set; }
    }

    public class OpportunityDetailResponse
    {
        public List<OpportunityDetail> data { get; set; }
    }

    public class OpportunityDetail
    {
        public int id { get; set; }
        public string name { get; set; }
        public long amount { get; set; }
        public string amountString { get; set; }
        public DateTime expected_order_date { get; set; }
        public string expected_order_date_DTO { get; set; }
        public DateTime requested_delivery_date { get; set; }
        public string requested_delivery_date_DTO { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public string city_name { get; set; }
        public string segment { get; set; }
        public string sub_segment { get; set; }
        public string created_by_name { get; set; }
        public DateTime last_modified { get; set; }
        public string last_modified_DTO { get; set; }
        public List<EkosistemOpportunity> ekosistem { get; set; }
    }

    public class EkosistemOpportunity
    {
        public int id { get; set; }
        public string company_name { get; set; }
        public string customer_contact_name { get; set; }
        public string customer_role { get; set; }
    }

    public class UpdateCloseClaim
    {
        public int claimNumber { get; set; }
        public string billingdokumen { get; set; }
        public string billingdate { get; set; }
        public string prnumber { get; set; }
    }

    public class ResultAjax
    {
        public string Status { get; set; }
        public string Content { get; set; }
    }
}
