﻿using System.ComponentModel.DataAnnotations;

namespace SemarAutomation.Models
{
    public class SFMEntitlement
    {
        [Key]
        public int Id { get; set; }
        public string? Account { get; set; }
        public string? ProgramName { get; set; }
        public DateTime ExpiredDate { get; set; }
        public long EntitlementValue { get; set; }
        public long EntitlementBalance { get; set; }
        public string? PPR { get; set; }
        public string? Remark { get; set; }
        public bool IsRejected { get; set; }
        public int LastClaimNumber { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public SFMEntitlement()
        {
            UpdatedAt = DateTime.Now;
        }
    }
}
