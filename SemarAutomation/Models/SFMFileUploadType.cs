﻿using System.ComponentModel.DataAnnotations;

namespace SemarAutomation.Models
{
    public class SFMFileUploadType
    {
        [Key]
        public int Id { get; set; }
        public string? ProposalType { get; set; }
        public string? ProposalSubType { get; set; }
        public string? TypeFileProposal { get; set; }
        public string? TypeFileClaim { get; set; }
        public string? DetailProposal { get; set; }
        public string? DetailClaim { get; set; }
    }

    public class TypeFileData
    {
        public string Filename { get; set; }
        public string IdName { get; set; }
        public List<string> TypeFile { get; set; }
    }

    public class DescriptionFile
    {
        public string DetailProposal { get; set; }
        public string DetailClaim { get; set; }
    }

}