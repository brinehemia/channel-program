﻿using System.ComponentModel.DataAnnotations;

namespace SemarAutomation.Models
{
    public class ApprovalTask
    {
        [Key]
        public string Id { get; set; }
        public string? Title { get; set; }
        public string? Description { get; set; }
        public string? ApprovalTemplateId { get; set; }
        public string? SubmittedById { get; set; }
        public string? CurrentPICId { get; set; }
        public string? Data { get; set; }
        public int? ClaimNumber { get; set; }
        public string? TypeData { get; set; }
        public int EntitlementId { get; set; }
        public string? CurrentStepId { get; set; }
        public DateTime DueDate { get; set; }
        public string? Status { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public ApprovalTask()
        {
            Guid g = Guid.NewGuid();
            Id = g.ToString();
            UpdatedAt = DateTime.UtcNow.AddHours(7);
        }
    }
}
