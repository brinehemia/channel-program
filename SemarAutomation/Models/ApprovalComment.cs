﻿using System.ComponentModel.DataAnnotations;

namespace SemarAutomation.Models
{
    public class ApprovalComment
    {
        [Key]
        public int Id { get; set; }
        public string? ApprovalTaskId { get; set; }
        public string? UserId { get; set; }
        public string? Comment { get; set; }
        public DateTime CreatedAt { get; set; }

        public ApprovalComment()
        {
            CreatedAt = DateTime.UtcNow.AddHours(7);
        }
    }

    public class ApprovalCommentVM
    {
        public int Id { get; set; }
        public string? ApprovalTaskId { get; set; }
        public string? UserId { get; set; }
        public string? Name { get; set; }
        public string? Role { get; set; }
        public string? Comment { get; set; }
        public DateTime CreatedAt { get; set; }

    }
}
