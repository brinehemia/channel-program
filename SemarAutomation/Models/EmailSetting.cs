﻿using System.ComponentModel.DataAnnotations;

namespace SemarAutomation.Models
{
    public class EmailSetting
    {
        [Key]
        public int Id { get; set; }
        public string? Email { get; set; }
        public string? Password { get; set; }
    }
}
