﻿using System.ComponentModel.DataAnnotations;

namespace SemarAutomation.Models
{
    public enum StepBaseType { Start, Action, Approval, Decision, Finish }
    public class StepBase
    {
        [Key]
        public string Id { get; set; }
        public string ApprovalTemplateId { get; set; }
        public string? PreviousStepId { get; set; }
        public string? OnApprovedId { get; set; }
        public string? OnRejectedId { get; set; }
        public string? OnSendbackId { get; set; }
        public string Title { get; set; }
        public int order { get; set; }
        public StepBaseType Type { get; set; }
        public string? Description { get; set; }
        public string? View { get; set; }
        public string? Roles { get; set; }

        public StepBase()
        {
            Guid g = Guid.NewGuid();
            Id = g.ToString();
        }
    }
}
