﻿using System.ComponentModel.DataAnnotations;

namespace SemarAutomation.Models
{
    public class ProgramSFMDistributor
    {
        [Key]
        public int Id { get; set; }
        public int DataProgramId { get; set; }
        public string? UserDistributorNumber { get; set; }
    }
}
