﻿using System.ComponentModel.DataAnnotations;

namespace SemarAutomation.Models
{
    public class Proposal
    {
        [Key]
        public int Id { get; set; }
        public int ProposalNumber { get; set; }
        public string? ApprovalTaskId { get; set; }
        public string? AccountNumber { get; set; } = string.Empty;
        public string? Kategori { get; set; }
        public string? TipeAktivitas { get; set; }
        public string? NamaAktivitas { get; set; }
        public string? SegmentPasar { get; set; }
        public string? SubSegmentPasar { get; set; }
        public DateTime TanggalAktivitas { get; set; }
        public string? SumberBudget { get; set; }
        public string? Opportunity { get; set; }
        public string? KPI { get; set; }
        public long Nominal { get; set; }
        public long NominalBalance { get; set; }
        public string? UploadedFile { get; set; }
        public string? Status { get; set; }
        public string? ProposalAktivitas { get; set; }
        public string? QuotationVendor { get; set; }
    }
}
