﻿namespace SemarAutomation.Models
{
    public class SendEmailLog
    {
        public string Id { get; set; }
        public string? Action { get; set; }
        public string? SendTo { get; set; }
        public string? Data { get; set; }
        public string? Status { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public SendEmailLog()
        {
            UpdatedAt = DateTime.UtcNow.AddHours(7);
        }
    }
}
