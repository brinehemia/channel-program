﻿using Microsoft.AspNetCore.Identity;

namespace SemarAutomation.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string? Name { get; set; }
        public string RoleName { get; set; }
        public string? Channel { get; set; }
        public string? AccountDistributor { get; set; }
        public string? OtherEmail { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public bool IsSFMUser { get; set; }
        public string? CompanyDistributor { get; set; }

        public ApplicationUser()
        {
            UpdatedAt = DateTime.UtcNow.AddHours(7);
        }
    }
}
