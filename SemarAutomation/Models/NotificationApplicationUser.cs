﻿using System.ComponentModel.DataAnnotations;

namespace SemarAutomation.Models
{
    public class NotificationApplicationUser
    {
        [Key]
        public int Id { get; set; }
        public string? UserId { get; set; }
        public string? Title { get; set; }
        public string? Message { get; set; }
        public string? Icon { get; set; }
        public string? Color { get; set; }
        public string? Url { get; set; }
        public DateTime Date { get; set; }
        public bool IsRead { get; set; } = false;
    }
}
